/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>

#include <asm/dma.h>
#include <asm/mach-types.h>

#include <mach/edma.h>
#include <mach/mux.h>

#include "davinci-pcm.h"
#include "davinci-i2s.h"
#include "davinci-mcasp.h"

#define AUDIO_FORMAT (SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_CBS_CFS \
		     | SND_SOC_DAIFMT_NB_IF)

static int indio_hw_params(struct snd_pcm_substream *substream,
			 struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	int ret = 0;
	unsigned sysclk;

	sysclk = 24576000;

	/* set codec DAI configuration */
	ret = snd_soc_dai_set_fmt(codec_dai, AUDIO_FORMAT);
	if (ret < 0)
		return ret;

	/* set cpu DAI configuration */
	ret = snd_soc_dai_set_fmt(cpu_dai, AUDIO_FORMAT);
	if (ret < 0)
		return ret;

	ret = snd_soc_dai_set_sysclk(cpu_dai, DAVINCI_MCASP_AHCLKX, sysclk, SND_SOC_CLOCK_IN);
	if(ret < 0)
		return ret;

	ret = snd_pcm_hw_constraint_minmax(substream->runtime,
					   SNDRV_PCM_HW_PARAM_RATE,
					   48000, 48000);
	if (ret < 0)
		return ret;

	/* we will use a constant 16 bit per channel (64 bits total) shift rate
	 * at 48000 Hz. 24576000 / (16*2) / 48000 = 16 
	 * But we are running I2S stereo, so we need to double the CLKX rate.
	 */
	ret = snd_soc_dai_set_clkdiv(cpu_dai, DAVINCI_MCASP_CLKDIV_AHCLKX, 0);
	if (ret < 0)
		return ret;

	ret = snd_soc_dai_set_clkdiv(cpu_dai, DAVINCI_MCASP_CLKDIV_ACLKX, 15);
	if (ret < 0)
		return ret;

	return 0;
}

static struct snd_soc_ops indio_ops = {
	.hw_params = indio_hw_params,
};

static const struct snd_soc_dapm_widget dsd1791_dapm_widgets[] = {
	SND_SOC_DAPM_LINE("Line Out", NULL),
};

static const struct snd_soc_dapm_route audio_map[] = {
	{"Line Out", NULL, "LLOUT"},
	{"Line Out", NULL, "RLOUT"},
};

static int indio_dsd1791_init(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_soc_codec *codec = rtd->codec;
	struct snd_soc_dapm_context *dapm = &codec->dapm;

	snd_soc_dapm_new_controls(dapm, dsd1791_dapm_widgets,
				   ARRAY_SIZE(dsd1791_dapm_widgets));

	snd_soc_dapm_add_routes(dapm, audio_map, ARRAY_SIZE(audio_map));

	snd_soc_dapm_enable_pin(dapm, "Line Out");

	snd_soc_dapm_sync(dapm);

	return 0;
}

static struct snd_soc_dai_link indio_dai = {
	.name = "dsd1791",
	.stream_name = "Playback",
	.cpu_dai_name = "davinci-mcasp.0",
	.codec_dai_name = "dsd1791",
	.codec_name = "spi1.2",
	.platform_name = "davinci-pcm-audio",
	.init = indio_dsd1791_init,
	.ops = &indio_ops,
};

static struct snd_soc_card indio_snd_soc_card = {
	.name = "MityDSP-L138 INDIO",
	.dai_link = &indio_dai,
	.num_links = 1,
};

static struct platform_device *indio_snd_device;

static int __init indio_init(void)
{
	struct snd_soc_card *indio_snd_dev_data;
	int index = 0;
	int ret;

	indio_snd_dev_data = &indio_snd_soc_card;

	indio_snd_device = platform_device_alloc("soc-audio", index);
	if (!indio_snd_device)
		return -ENOMEM;

	platform_set_drvdata(indio_snd_device, indio_snd_dev_data);
	ret = platform_device_add(indio_snd_device);
	if (ret)
		platform_device_put(indio_snd_device);

	return ret;
}

static void __exit indio_exit(void)
{
	platform_device_unregister(indio_snd_device);
}

module_init(indio_init);
module_exit(indio_exit);

MODULE_AUTHOR("Michael Williamson");
MODULE_DESCRIPTION("MityDSP-L138 INDIO ASoC driver");
MODULE_LICENSE("GPL");
