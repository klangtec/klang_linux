/*
 * ALSA SoC codec driver for Texas Instruments DSD1791.
 *
 * Author:      (C) Michael Williamson <michael.williamson@criticallink.com>
 * Copyright:   (C) 2011 Critical Link, LLC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/spi/spi.h>
#include <linux/platform_device.h>
#include <linux/regmap.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/initval.h>
#include <sound/tlv.h>

#define DSD1791_REG_DIGATT_L	16
#define DSD1791_REG_DIGATT_R	17
#define DSD1791_REG_AUDFMT	18
#define DSD1791_REG_SRST	20

#define DSD1791_FMT_16RJ	(0<<4)
#define DSD1791_FMT_20RJ	(1<<4)
#define DSD1791_FMT_24RJ	(2<<4)
#define DSD1791_FMT_24LJ	(3<<4)
#define DSD1791_FMT_16I2S	(4<<4)
#define DSD1791_FMT_24I2S	(5<<4)
#define DSD1791_FMT_MASK	0x70
#define DSD1791_MAX_REG		23

/* DSD1791 register cache (16 through 23 are used) */
struct reg_default dsd1791_reg[] = {
	{16, 0xFF},
	{17, 0xFF},
	{18, 0x50},
	{19, 0x00},
	{20, 0x00},
	{21, 0x01},
	{22, 0x00},
	{23, 0x00},
};

static bool dsd1791_readable_reg(struct device *dev, unsigned int reg)
{
	if ( (reg >= 16) && (reg <= 23) )
		return true;

	return false;
}

/*
 * The DSD1791 DOUT pin is multiplexed with LZERO detection and in
 * many cases will not be available for readout.  In this case
 * mark all registers as non-volatile and just use the cache.
 */
static bool dsd1791_volatile_reg(struct device *dev, unsigned int reg)
{
	return false;
}

struct dsd1791 {
	struct regmap *regmap;
	int dai_fmt;
	int pcm_fmt;
};

static int dsd1791_set_format_word(struct dsd1791 *dsd1791,
				   struct snd_soc_codec *codec)
{
	u8 fmt = 0;
	int ret;

	switch (dsd1791->dai_fmt & SND_SOC_DAIFMT_FORMAT_MASK) {

	case SND_SOC_DAIFMT_I2S:
		switch (dsd1791->pcm_fmt) {
		case SNDRV_PCM_FORMAT_S16_LE:
			fmt = DSD1791_FMT_16I2S;
			break;
		case SNDRV_PCM_FORMAT_S24_LE:
			fmt = DSD1791_FMT_24I2S;
			break;
		default:
			return -EINVAL;
		}
		break;

	case SND_SOC_DAIFMT_RIGHT_J:
		switch (dsd1791->pcm_fmt) {
		case SNDRV_PCM_FORMAT_S16_LE:
			fmt = DSD1791_FMT_16RJ;
			break;
		case SNDRV_PCM_FORMAT_S24_LE:
			fmt = DSD1791_FMT_24RJ;
			break;
		default:
			return -EINVAL;
		}
		break;

	case SND_SOC_DAIFMT_LEFT_J:
		switch (dsd1791->pcm_fmt) {
		case SNDRV_PCM_FORMAT_S24_LE:
			fmt = DSD1791_FMT_24LJ;
		default:
			return -EINVAL;
		}
		break;
	default:
		return -EINVAL;
	}
	ret = snd_soc_update_bits(codec, DSD1791_REG_AUDFMT,
					DSD1791_FMT_MASK, fmt);
	if (ret < 0)
		return ret;

	return 0;
}

static int dsd1791_mute(struct snd_soc_dai *dai, int mute)
{
	struct snd_soc_codec *codec = dai->codec;
	int ret;

	ret = snd_soc_update_bits(codec, DSD1791_REG_AUDFMT, 1, mute ? 1 : 0);
	if (ret < 0)
		return ret;

	return 0;
}

static int dsd1791_hw_params(struct snd_pcm_substream *substream,
			     struct snd_pcm_hw_params *params,
			     struct snd_soc_dai *dai)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_codec *codec = rtd->codec;
	struct dsd1791 *dsd1791 = snd_soc_codec_get_drvdata(codec);

	dsd1791->pcm_fmt = params_format(params);

	return dsd1791_set_format_word(dsd1791, codec);
}

static int dsd1791_set_fmt(struct snd_soc_dai *codec_dai,
			     unsigned int fmt)
{
	struct snd_soc_codec *codec = codec_dai->codec;
	struct dsd1791 *dsd1791 = snd_soc_codec_get_drvdata(codec);

	dsd1791->dai_fmt = fmt;

	return dsd1791_set_format_word(dsd1791, codec);
}

#define DSD1791_RATES	SNDRV_PCM_RATE_8000_192000
#define DSD1791_FORMATS	(SNDRV_PCM_FMTBIT_S16_LE |\
			 SNDRV_PCM_FMTBIT_S24_LE)

static const struct snd_soc_dai_ops dsd1791_dai_ops = {
	.hw_params	= dsd1791_hw_params,
	.set_fmt	= dsd1791_set_fmt,
	.digital_mute	= dsd1791_mute,
};

static struct snd_soc_dai_driver dsd1791_dai = {
	.name = "dsd1791",
	.playback = {
		.stream_name = "Playback",
		.channels_min = 2,
		.channels_max = 2,
		.rates = DSD1791_RATES,
		.formats = DSD1791_FORMATS,
	},
	.ops = &dsd1791_dai_ops,
};

static const DECLARE_TLV_DB_SCALE(mvol_tlv, -12750, 50, 0);

static const struct snd_kcontrol_new dsd1791_snd_controls[] = {
	SOC_DOUBLE_R_TLV("Master Playback Volume", DSD1791_REG_DIGATT_L,
		DSD1791_REG_DIGATT_R, 0, 255, 0, mvol_tlv),
};

static int dsd1791_probe(struct snd_soc_codec *codec)
{
	int ret;
	struct dsd1791 *dsd1791 = snd_soc_codec_get_drvdata(codec);

	codec->control_data = dsd1791->regmap;
	ret = snd_soc_codec_set_cache_io(codec, 0, 0, SND_SOC_REGMAP);
	if (ret) {
		dev_err(codec->dev, "Failed to set Cache I/O: %d\n", ret);
		return ret;
	}

	ret = snd_soc_update_bits(codec, DSD1791_REG_SRST, 0x40, 0x40);
	if (ret < 0) {
		dev_err(codec->dev, "Unable to reset device: %d\n", ret);
		return ret;
	}

	/* default format after reset */
	dsd1791->dai_fmt = SND_SOC_DAIFMT_I2S;
	dsd1791->pcm_fmt = SNDRV_PCM_FORMAT_S24_LE;

	/*
	 * this bit must be set in order for the left/right attenuator
	 * settings to be applied by the codec when written via SPI.
	 */
	ret = snd_soc_update_bits(codec, DSD1791_REG_AUDFMT, 0x80, 0x80);

	if (ret < 0)
		return ret;

	return 0;
}

static struct snd_soc_codec_driver dsd1791_codec_driver = {
	.probe			= dsd1791_probe,
	.controls		= dsd1791_snd_controls,
	.num_controls		= ARRAY_SIZE(dsd1791_snd_controls),
};

static const struct regmap_config dsd1791_spi_regmap_config = {
	.val_bits		= 8,
	.reg_bits		= 8,
	.reg_defaults		= dsd1791_reg,
	.num_reg_defaults	= ARRAY_SIZE(dsd1791_reg),
	.max_register		= DSD1791_MAX_REG,
	.cache_type		= REGCACHE_RBTREE,
	.readable_reg		= dsd1791_readable_reg,
	.writeable_reg		= dsd1791_readable_reg,
	.volatile_reg		= dsd1791_volatile_reg,
};

static int __devinit dsd1791_spi_probe(struct spi_device *spi)
{
	struct dsd1791 *dsd1791;

	dsd1791 = devm_kzalloc(&spi->dev, sizeof *dsd1791, GFP_KERNEL);
	if (!dsd1791)
		return -ENOMEM;

	dsd1791->regmap = regmap_init_spi(spi, &dsd1791_spi_regmap_config);
	if (IS_ERR(dsd1791->regmap))
		return PTR_ERR(dsd1791->regmap);

	spi_set_drvdata(spi, dsd1791);

	return snd_soc_register_codec(&spi->dev,
		&dsd1791_codec_driver, &dsd1791_dai, 1);
};

static int __devexit dsd1791_spi_remove(struct spi_device *spi)
{
	struct dsd1791 *dsd1791 = spi_get_drvdata(spi);

	snd_soc_unregister_codec(&spi->dev);
	regmap_exit(dsd1791->regmap);
	return 0;
}

static struct spi_driver dsd1791_spi_driver = {
	.driver = {
		.name = "dsd1791",
		.owner = THIS_MODULE,
	},
	.probe = dsd1791_spi_probe,
	.remove = __devexit_p(dsd1791_spi_remove),
};

static int __init dsd1791_init(void)
{
	return spi_register_driver(&dsd1791_spi_driver);
}
module_init(dsd1791_init);

static void __exit dsd1791_exit(void)
{
	spi_unregister_driver(&dsd1791_spi_driver);
}
module_exit(dsd1791_exit);

MODULE_DESCRIPTION("ASoC DSD1791 codec driver");
MODULE_AUTHOR("Michael Williamson");
MODULE_LICENSE("GPL v2");
