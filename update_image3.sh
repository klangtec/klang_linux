#
# Compiles the Kernel, uploads image to NFS, flashes image on $ARM_HOST:ubi0 and installs kernel modules
#

set -e

ARM_HOST=192.168.188.113
MODULES_TEMP=../modules_temp
#NFS_PATH=~/klangnfs/fabian



SSH_HOST="root@${ARM_HOST}"
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=./.knownhosts"
SSH_PASS="sshpass -p GPsrDz99SbgcDe34IjFvm2qWFE7kq4u"

rm -f .knownhosts

. /usr/local/oecore-i686/environment-setup-armv5te-angstrom-linux-gnueabi

#make clean
#make mrproper
#exit 0

KERNEL_VER=`make ARCH=arm CROSS_COMPILE=arm-angstrom-linux-gnueabi- kernelversion`
KERNEL_SRC_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Kernel Version -$KERNEL_VER-"

#echo "Building uImage..."
#make -j3 ARCH=arm CROSS_COMPILE=arm-angstrom-linux-gnueabi- uImage

#echo "uImage complete! Uploading it to NFS..."
#cp arch/arm/boot/uImage $NFS_PATH/uImage



echo "Uploading it to $ARM_HOST:/tmp/uImage..."
#$SSH_PASS scp arch/arm/boot/uImage $SSH_HOST:/tmp/uImage
$SSH_PASS scp $SSH_OPTS arch/arm/boot/uImage $SSH_HOST:/tmp/uImage
echo "Upload finished."
echo "Flashing it to $ARM_HOST:/dev/ubi0_0"
$SSH_PASS ssh $SSH_OPTS $SSH_HOST "/usr/sbin/ubiupdatevol /dev/ubi0_0 /tmp/uImage"
echo "flashing complete!"

md5sum  arch/arm/boot/uImage
echo " = NFS "
#md5sum  $NFS_PATH/uImage
echo " = $ARM_HOST "
$SSH_PASS ssh $SSH_OPTS $SSH_HOST "md5sum /tmp/uImage"

#echo ""
#echo ""
#echo ""
#echo "Building modules (Ctrl+C to cancel)..."
#sleep 2
#make -j3 ARCH=arm CROSS_COMPILE=arm-angstrom-linux-gnueabi- modules > /dev/null

mkdir -p ${MODULES_TEMP}
rm -f -r ${MODULES_TEMP}/*

echo "Packaging modules..."
make -j3 ARCH=arm CROSS_COMPILE=arm-angstrom-linux-gnueabi- INSTALL_MOD_PATH=${MODULES_TEMP}/ modules_install
cd ${MODULES_TEMP}
tar cfvz kernel_modules.tar.gz lib > /dev/null

echo "Uploading module package ..."
$SSH_PASS scp $SSH_OPTS kernel_modules.tar.gz $SSH_HOST:/tmp/kernel_modules.tar.gz

echo "Extracting & Installing modules remotely ..."
$SSH_PASS ssh $SSH_OPTS $SSH_HOST "./remount-rootfs.sh; mkdir -p modules_temp; cd modules_temp; tar xfvz /tmp/kernel_modules.tar.gz > /dev/null; cp -r -f lib /; cd ..; rm -r modules_temp; rm  /tmp/kernel_modules.tar.gz"

echo "Installed modules:"
$SSH_PASS ssh $SSH_OPTS $SSH_HOST "ls /lib/modules"


#echo ""
#echo ""
#echo "U-Boot flash command:"
#echo "nfs 0xC0700000 192.168.188.42:/KLANGlinux/fabian/uImage; ubi part rootfs; ubi write C0700000 bootfs 0x280000"

