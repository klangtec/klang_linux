/*
 * fpga.h
 *
 *  Created on: Mar 5, 2010
 *      Author: mikew
 */

#ifndef FPGA_H_
#define FPGA_H_

#include <linux/types.h>
#include <linux/device.h>
#include "common/fpga/fpga.h"

#define FPGA_STATE_UNKNOWN      0 /* FPGA is not in a known state */
#define FPGA_STATE_RESET        1 /* FPGA has been reset, but we aren't programming */
#define FPGA_STATE_PROGRAMMING  2 /* FPGA is being programmed */
#define FPGA_STATE_PROGRAM_FAIL 3 /* FPGA failed programming */
#define FPGA_STATE_PROGRAMMED   4 /* FPGA has been programmed */

#define FPGA_CMD_RESET          1 /* Assert FPGA reset */
#define FPGA_CMD_PROGRAM        2 /* Initiate programming cycle */
#define FPGA_CMD_FINISHPROGRAM  3 /* Check if programming worked, then enumerate cores */

extern int read_core_version(void* baseaddr, struct coreversion* pdata);

/**
 * Generic FPGA device structure.  This represents a "core".
 */
struct fpga_device {
	struct device		dev;	     /* standard device data is stored here */
	struct coreversion	coreversion; /* the core version register information from FPGA */
	void*			baseaddr;    /* the base address of the "core" */
	int			coreindex;   /* the index of the core in the FPGA */
	struct fpga_driver*	drv;         /* device driver associated with this device */
};
#define to_fpga_device(d) container_of(d, struct fpga_device, dev)

/**
 * Generic FPGA "core" device driver structure.  All core drivers
 * for the linux side must create this structure and register it 
 * with register_fpga_driver().
 */
struct fpga_driver {
	unsigned short  id;                         /* The core ID this driver will support */
	char* version;                              /* driver version */
	struct driver_attribute version_attr;       /* version attribute seen in /sys */
	int (*IrqHandler)(struct fpga_device* dev); /* IRQ handler, called by bus structure */
	int (*probe)(struct device *dev);	    /* Probe method called by the bus */
	int (*remove)(struct device *dev);	    /* Remove method called by the bus */
	struct device_driver driver;                /* actual device driver implementation */
};
#define to_fpga_driver(d) container_of(d, struct fpga_driver, driver)

extern int register_fpga_driver(struct fpga_driver *);
extern int enable_irq_vec(int level, int vector, int enable);

#endif /* FPGA_H_ */
