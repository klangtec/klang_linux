/*
 * fpga_hdlc.c
 *
 *  Created on: Dec 1, 2011
 *      Author: Tim Iskander
 */

#include "fpga.h"
#include "common/fpga/fpga_hdlc.h"
#include "common/fpga/core_ids.h"

#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/ioctl.h>
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/clk.h>
#include <linux/cpufreq.h>
#include <linux/err.h>
#include <linux/wait.h>
#include <linux/sched.h>

#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/kfifo.h>
#include <linux/gpio.h>
#include <linux/list.h>
#include <linux/delay.h>

#include "hdlc_ioc.h"


/* 16 bit FCS lookup table per RFC1331 */
#define FCSINIT	0xFFFF

static unsigned short fcstab[256] = {
0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

#define HDLC_DRIVER_VERSION_STRING "HDLC Controller Driver 1.4.5"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Tim Iskander <iskander@criticallink.com>");
MODULE_DESCRIPTION("Driver for MityDSP-L138 FPGA Based HDLC Core");
MODULE_VERSION(HDLC_DRIVER_VERSION_STRING);

/* HDLC Frame separator flag [01111110] */
#define HDLC_FLAG 0x7E

/* Idle state bits - used to space frames */
#define IDLE_BITS 0xFF

#define HDLC_DEV_NUMS (1)
/* Size of hdlc core's transmit fifo */
#define HDLC_TFIFO_SIZE (4096)
/* TFIFO threshold - Set to half size of TFIFO */
#define HDLC_XPRL_THRESHOLD ((HDLC_TFIFO_SIZE/2)/32)
/* Size used for internal fifos. Must be power of 2 */
#define DRIVER_FIFO_SIZE (2048)
/* Max # of frames in the FIFO - 64 32-bit values... its a wag :) */
/* The frame_fifo is really just full of frame sizes */
#define FRAME_FIFO_MAX_COUNT 64
#define FRAME_FIFO_NUM_SIZE (sizeof(unsigned int))
#define FRAME_FIFO_SIZE (FRAME_FIFO_MAX_COUNT * FRAME_FIFO_NUM_SIZE)
/* FPGA interrupt line */
#define FPGA_INT    GPIO_TO_PIN(8, 12)

#define MODE_IS_TRANSPARENT(reg) (MDS_TRANSP_MODE == reg.bits.mds)

/**
 * write an unsigned int to a kfifo
 * \param pFifo pointer to kfifo to use
 * \param val value to put
 * \return 1 on success, < 0 on fail
 */
static inline unsigned int
kfifo_in_uint(struct kfifo *pFifo, unsigned int val)
{
	int rv = kfifo_in(pFifo, &val, sizeof(unsigned int));
	return (0 < rv) ? 1 : rv;
}


/**
 * read an unsigned int from a kfifo
 * \param pFifo pointer to kfifo to use
 * \param val pointer to value to get to
 * \return 1 on success, < 0 on fail
 */
static inline unsigned int
kfifo_out_uint(struct kfifo *pFifo, unsigned int *pVal)
{
	int rv = 0;
	if(kfifo_len(pFifo) < sizeof(unsigned int))
	{
		/* There is not a full int in the fifo yet
		 *(it is probably in the process of being added)
		 */
		return 0;
	}
	rv = kfifo_out(pFifo, pVal, sizeof(unsigned int));
	return (0 < rv) ? 1 : rv;
}


/**
 * Compute an HDLC compliant CRC for a buffer
 * \param buff pointer to data buffer
 * \param count number of btes in buff
 * \return crc value
 */
static inline unsigned short hdlc_crc(const unsigned char *buff, int count)
{
	unsigned short fcsval = FCSINIT;
	int ii;
	for (ii = 0; ii < count; ++ii)
		fcsval = (fcsval >> 8) ^ fcstab[(fcsval ^ buff[ii]) & 0xff];
	return fcsval^0xFFFF; /* return the 1's complement */
}

/* list of frames to xmit */
struct hdlc_frame_list{
	struct list_head list; /* kernel's list structure */
	ssize_t size;
	char *data;
};

/**
  * HDLC device statistics.
  * \note cleared in cdev_open
  */
struct hdlc_stats {
	uint32_t	frames_xmit;
	uint32_t	frames_recv;
	uint32_t	frames_filtered;
	uint32_t	bytes_xmit;
	uint32_t	bytes_recv;
	uint32_t	xmit_errs;
	uint32_t	recv_errs;
	uint32_t	avg_tx_fifo_size;
	uint32_t	avg_rx_fifo_size;
};

/**
 * HDLC device instance data
 */
struct hdlc_dev {
	/* Device in sysfs */
	struct device *device;

	/* fpga device info */
	struct fpga_device *fpgadev;

	/* Sysfs class */
	struct class *class;

	/* Character device */
	struct cdev cdev;

	/* Contains the registered number for this device */
	dev_t devnum;

	/* FIFO used to store incoming data. Fed by intr and read by cdev_read */
	struct kfifo read_fifo;
	struct kfifo frame_fifo;
	struct hdlc_frame_list xmit_frames;

	/* frameFlag is used by cdev_read so it hands the app a frame at a time */
	bool frameFlag;

	/* io_mode is used by cdev_write to decide if it should que writes or not */
	bool io_mode;

	/* Queue used to hold those waiting for data to read */
	wait_queue_head_t read_queue;

	/* Queue used when waiting for interrupt. cdev_write */
	wait_queue_head_t interrupt_queue;

	/* Semaphore to control access to io */
	struct semaphore ioSem;

	/* Only allow one reader and one writer device */
	/* Must grab ioSem before checking/setting */
	bool opened_read;
	bool opened_write;

	/* Debug level for controlling print stmts */
	uint16_t attrDbgLevel;
	/* Address to filter (optional) 0xFF means don't filter */
	uint8_t  recvAddr;
	/* statistics */
	struct hdlc_stats stats;

	uint32_t  lastmsgsize;
	char lastmsg[4096];
};

/**
 * Transmit a byte over HDLC
 * \param dev - pointer to HDLC device instance data
 * \param apFifo - Pointer to TX fifo
 * \param anVal - byte value to transmit
 * \param abFrame - True if this is a frame delimiter (and should not be
 * subject to bit stuffing in the FPGA.
 * \return 1
 */
static inline unsigned int xmit_byte(struct hdlc_dev *dev,
				 volatile unsigned short *apFifo,
				 const unsigned char anVal, bool abFrame)
{
	tuTxFifoReg tx_fifo;
	tuStatusTXFIFOReg STXR;
	volatile unsigned short	*lpBaseReg =
			(unsigned short*)dev->fpgadev->baseaddr;

	//Check FIFO
	STXR.mnWord   = lpBaseReg[gnSTATUS_TXFIFO_OFFSET];
	if(STXR.msBits.tx_fifo_full)
	{
		if (dev->attrDbgLevel)
			dev_dbg(dev->device, "xmit_byte: TX FIFO is FULL\n");

		msleep(1);
		STXR.mnWord   = lpBaseReg[gnSTATUS_TXFIFO_OFFSET];
		if(STXR.msBits.tx_fifo_full)
		{
			/* TX FIFO is still full after 1 ms */
			/* (enough time for 20 bytes) -- ERROR */
			dev_warn(dev->device,"xmit_byte: TX FIFO STILL FULL");
			return 0;
		} else {
			if (dev->attrDbgLevel)
				dev_dbg(dev->device,
					"xmit_byte: TX FIFO No Longer Full\n");
		}
	}


	//FIFO is not Full - Transmit a Byte
	tx_fifo.mnWord = 0;
	tx_fifo.msBits.tx_byte = anVal;
	tx_fifo.msBits.frame_delim_v = abFrame ? 1 : 0;
	*apFifo = tx_fifo.mnWord;
	if (1 < dev->attrDbgLevel)
		dev_dbg(dev->device, "xmit_byte: %04x\n", tx_fifo.mnWord);

	return 1;
}

/**
 * enable/disable the FPGA HDLC core
 * \param dev hdlc device struct
 * \param enable set to true to enable, false to disable :
 * \param reset - If set, will reset the core
 */
static void hdlc_enable_core(struct hdlc_dev* dev, bool enable, bool reset )
{
	tuCtrlReg	CTLR;
	tuIerReg	IER;
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;

	if(reset) {
		CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];
		CTLR.msBits.srst = 1;
		lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
		mdelay(1); /* delay 1ms for the fifos to clear */
	}
	CTLR.mnWord		= lpBaseReg[gnCTRL_OFFSET];
	CTLR.msBits.enable	= enable?1:0;
	CTLR.msBits.srst	= 0;
	lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;

	IER.mnWord		= lpBaseReg[gnIER_OFFSET];
	IER.msBits.int_frame_rx	= enable?1:0;
	lpBaseReg[gnIER_OFFSET] = IER.mnWord;
}


/**
 * enable/disable the RS422/232 Transceiver
 * \param dev hdlc device struct
 * \param enableTx Enable transmit (DEN pin)
 * \param enableRx Enable receive (_REN_ pin)
 */
static void hdlc_enable_xcvr(struct hdlc_dev* dev, bool enableTx, bool enableRx )
{
	tuCtrlReg	CTLR;
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;

	CTLR.mnWord		= lpBaseReg[gnCTRL_OFFSET];
	CTLR.msBits.hdlc_den	= enableTx?1:0;
	CTLR.msBits.hdlc_ren	= enableRx?1:0;
	lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;

}

/**
 * Initialize the FPGA core registers to sane default values
 * \param dev hdlc device struct
 */
static void hdlc_init_regs(struct hdlc_dev* dev)
{
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	CTLR.mnWord = 0;
	/* Set the transmit polarity */
	CTLR.msBits.tx_pol	= 0;   /* normal polarity */
	CTLR.msBits.rx_pol	= 0;   /* normal polarity */
	CTLR.msBits.txc_pol	= 0;   /* normal polarity */
	CTLR.msBits.rxc_pol	= 0;   /* normal polarity */
	CTLR.msBits.txc_rxc	= 0;   /* independent TX clock */
	CTLR.msBits.hdlc_den	= 0;   /* disable transmitter */
	CTLR.msBits.hdlc_ren	= 0;   /* disable receiver */

	lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
}

/**
 * Sets the GO bit in the transmitter. This tells the FPGA to
 * send the frames queued up by the driver.
 */
static void hdlc_send_frames(struct hdlc_dev* dev)
{
	volatile unsigned short  *lpBaseReg =
			(volatile unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;

	/* write the end of frame flag */
	volatile unsigned short  *tx_fifo_reg = &lpBaseReg[gnTX_FIFO_OFFSET];

	/* Tack a 0xFF on to the end of the fifo to get it clocked out */
	/* to gaurantee an inter-frame gap. */
	(void)xmit_byte(dev, tx_fifo_reg, IDLE_BITS, true);/* idle bits */

	/* Send command to start transmitting */
	CTLR.mnWord		= lpBaseReg[gnCTRL_OFFSET];
	CTLR.msBits.tx_go	= 1;
	lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;

}


/*///////////////////////////////////////////////////////////////////////////// */
/*// Device Attributes (sysfs) */
/*///////////////////////////////////////////////////////////////////////////// */


/*/////Specific settings */

/******** Version... RO *************/
ssize_t
attr_read_version(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	struct coreversion cv;
	int stat = 0;

	stat = read_core_version(lpBaseReg, &cv);
	if(0 == stat) {
		return snprintf(buf, PAGE_SIZE, "%s \n"
				"FPGA HDLC CORE Version: %02d.%02d  DATE(YY-MM-DD): %02d-%02d-%02d\n"
			,HDLC_DRIVER_VERSION_STRING
			,cv.ver1.bits.major
			,cv.ver1.bits.minor
			,cv.ver1.bits.year
			,cv.ver2.bits.month
			,cv.ver2.bits.day
			);
	} else {
		return snprintf(buf, PAGE_SIZE, "%s\n"
				"Unable to read FPGA core! (%d)\n"
			,HDLC_DRIVER_VERSION_STRING

			,stat
			);
	}
}
DEVICE_ATTR(version, S_IRUGO | S_IWUSR, attr_read_version, NULL);

/******** Lastmsg... RO *************/
ssize_t
attr_read_lastmsg(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);

	// TODO add Semaphore
	if(dev->lastmsgsize != 0)
	{
		memcpy(buf,&dev->lastmsg,dev->lastmsgsize);
	}

	return dev->lastmsgsize;
}
DEVICE_ATTR(lastmsg, S_IRUGO | S_IWUSR, attr_read_lastmsg, NULL);


/******** Statistics... RO *************/
ssize_t
attr_read_stats(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);

	return snprintf(buf, PAGE_SIZE, "HDLC Device Statistics:\n"
		"	frames_xmit     = %u\n"
		"	frames_recv     = %u\n"
		"	frames_filtered = %u\n"
		"	bytes_xmit      = %u\n"
		"	bytes_recv      = %u\n"
		"	xmit_errs       = %u\n"
		"	recv_errs       = %u\n"
		"	avg_tx_fifo_size= %u\n"
		"	avg_rx_fifo_size= %u\n"
		,dev->stats.frames_xmit
		,dev->stats.frames_recv
		,dev->stats.frames_filtered
		,dev->stats.bytes_xmit
		,dev->stats.bytes_recv
		,dev->stats.xmit_errs
		,dev->stats.recv_errs
		,dev->stats.avg_tx_fifo_size
		,dev->stats.avg_rx_fifo_size
		);
}
DEVICE_ATTR(stats, S_IRUGO | S_IWUSR, attr_read_stats, NULL);

/******** Status... RO *************/
ssize_t
attr_read_status(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuStatusReg SR;
	tuStatusTXFIFOReg TXFIFO;
	tuStatusRXFIFOReg RXFIFO;
	tuStatusErrorCountReg ERR;

	SR.mnWord	= lpBaseReg[gnSTATUS_OFFSET];
	ERR.mnWord	= lpBaseReg[gnSTATUS_ERROR_COUNT_OFFSET];
	TXFIFO.mnWord   = lpBaseReg[gnSTATUS_TXFIFO_OFFSET];
	RXFIFO.mnWord   = lpBaseReg[gnSTATUS_RXFIFO_OFFSET];

	return snprintf(buf, PAGE_SIZE, "HDLC Device Status:\n"
			"Status          Reg: = 0x%04x \n"
			"	0    - RX Clock Present: %d\n"
			"	1-15 - Unused\n"
			"Error Count	 Reg: = 0x%04x \n"
			"\n"
			"TX FIFO Status  Reg: = 0x%04x \n"
			"	0-11 - Available TX Bytes: %d\n"
			"	12 - Full	 : %d\n"
			"	13 - Almost Full : %d\n"
			"	14 - Almost Empty: %d\n"
			"	15 - Empty	 : %d\n"
			"RX FIFO Status  Reg: = 0x%04x \n"
			"	0-11 - Bytes to Read: %d\n"
			"	12 - Full	 : %d\n"
			"	13 - Almost Full : %d\n"
			"	14 - Almost Empty: %d\n"
			"	15 - Empty	 : %d\n"
		,SR.mnWord
		,SR.msBits.rx_clock_present
		,ERR.mnWord
		,TXFIFO.mnWord
		,TXFIFO.msBits.tx_fifo_available
		,TXFIFO.msBits.tx_fifo_full
		,TXFIFO.msBits.tx_fifo_almost_full
		,TXFIFO.msBits.tx_fifo_almost_empty
		,TXFIFO.msBits.tx_fifo_empty
		,RXFIFO.mnWord
		,RXFIFO.msBits.rx_fifo_available
		,RXFIFO.msBits.rx_fifo_full
		,RXFIFO.msBits.rx_fifo_almost_full
		,RXFIFO.msBits.rx_fifo_almost_empty
		,RXFIFO.msBits.rx_fifo_empty
		);
}
DEVICE_ATTR(status, S_IRUGO | S_IWUSR, attr_read_status, NULL);

/******** Mode... RW *************/
ssize_t
attr_read_mode(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;

	tuCtrlReg CTLR;
	tuIerReg IER;
	tuStatusReg SR;


	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];
	IER.mnWord  = lpBaseReg[gnIER_OFFSET];
	SR.mnWord   = lpBaseReg[gnSTATUS_OFFSET];

	return snprintf(buf, PAGE_SIZE,
			"HDLC MODE\n"
			"Control   Reg: = 0x%04x tcPol = %cC/%cD rx_pol = %cC/%cD"
			"    R=%s   X=%s%s\n"
			"Status    Reg: = 0x%04x \n"
			"Interrupt Reg: = 0x%04x rx int = %d\n",
			CTLR.mnWord,
			CTLR.msBits.txc_pol ? '+':'-',
			CTLR.msBits.tx_pol ? '+':'-',
			CTLR.msBits.rxc_pol ? '+':'-',
			CTLR.msBits.rx_pol ? '+':'-',
			CTLR.msBits.hdlc_ren ? "enabled":"disabled",
			CTLR.msBits.hdlc_den ? "enabled":"disabled",
			CTLR.msBits.txc_rxc ? "  TxC <-- RxC":"",
			SR.mnWord,
			IER.mnWord,
			IER.msBits.int_frame_rx	);


}

ssize_t attr_set_mode(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR, local_ctl;

	/* read current mode bits */
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	retval = sscanf(buf, "%hx", &local_ctl.mnWord);
	if(CTLR.msBits.tx_pol != local_ctl.msBits.tx_pol)
	{
		printk("HDLC Changing TX Polarity from %d to %d\n",
				CTLR.msBits.tx_pol, local_ctl.msBits.tx_pol);
		CTLR.msBits.tx_pol = local_ctl.msBits.tx_pol;
	}
	if(CTLR.msBits.rx_pol != local_ctl.msBits.rx_pol)
	{
		printk("HDLC Changing RX Polarity from %d to %d\n",
				CTLR.msBits.rx_pol, local_ctl.msBits.rx_pol);
		CTLR.msBits.rx_pol = local_ctl.msBits.rx_pol;
	}
	lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
	return count;
}
DEVICE_ATTR(mode, S_IRUGO | S_IWUSR, attr_read_mode, attr_set_mode);

/******** Filter address... RW *************/
ssize_t
attr_read_addr(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* fpga_hdlc = dev_get_drvdata(device);

	return snprintf(buf, PAGE_SIZE, "0x%02x\n",
			fpga_hdlc->recvAddr);
}

ssize_t attr_set_addr(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* fpga_hdlc = dev_get_drvdata(device);
	unsigned short val = 0;

	retval = sscanf(buf, "%hx", &val);
	val &= 0xFF; /* only 8 bits please */
	printk(KERN_INFO "NOT HDLC changing receiver Address from %d to %d\n",
			fpga_hdlc->recvAddr, val);
	/* fpga_hdlc->recvAddr = val; */
	return count;
}
DEVICE_ATTR(addr, S_IRUGO | S_IWUSR, attr_read_addr, attr_set_addr);

/******** Transmit Polarity... RW *************/
ssize_t
attr_read_xpol(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	return snprintf(buf, PAGE_SIZE, "Control Reg: = 0x%04x"
			" TX polarity = %c\n",
			CTLR.mnWord, CTLR.msBits.tx_pol ? '+':'-');
}

ssize_t attr_set_xpol(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	unsigned short val = 0;
	/* read current mode bits */
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	retval = sscanf(buf, "%hx", &val);
	val=val&1; /* only 1 bit please */
	if(CTLR.msBits.tx_pol != val)
	{
		printk("HDLC Changing TX polarity from %d to %d\n",
				CTLR.msBits.tx_pol, val);
		CTLR.msBits.tx_pol = val;
		lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
	}
	return count;
}
DEVICE_ATTR(tx_pol, S_IRUGO | S_IWUSR, attr_read_xpol, attr_set_xpol);

/******** Transmit Clock Polarity... RW *************/
ssize_t
attr_read_xcpol(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	return snprintf(buf, PAGE_SIZE, "Control Reg: = 0x%04x "
					"TX clock polarity = %c\n",
			CTLR.mnWord, CTLR.msBits.txc_pol ? '+':'-');
}

ssize_t attr_set_xcpol(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	unsigned short val = 0;
	/* read current mode bits */
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	retval = sscanf(buf, "%hx", &val);
	val=val&1; /* only 1 bit please */
	/*/ if(CTLR.msBits.txc_pol != val) */
	{
		printk("HDLC Changing TX clock polarity from %d to %d\n",
				CTLR.msBits.txc_pol, val);
		CTLR.msBits.txc_pol = val;
		lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
	}
	return count;
}
DEVICE_ATTR(txc_pol, S_IRUGO | S_IWUSR, attr_read_xcpol, attr_set_xcpol);

/******** Transceiver enable... RW *************/
ssize_t
attr_read_xcvr(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	return snprintf(buf, PAGE_SIZE, "Tranceiver xmit %s recv %s\n",
			CTLR.msBits.hdlc_den ? "enabled ":"disabled",
			CTLR.msBits.hdlc_ren ? "enabled ":"disabled");
}

ssize_t attr_set_xcvr(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	unsigned short val1 = 0;
	unsigned short val2 = 0;
	/* read current mode bits */

	retval = sscanf(buf, "%hd %hd", &val1, &val2);
	if(2 == retval)
	{
		hdlc_enable_xcvr(dev, val1?true:false, val2?true:false);

		CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];
		printk(KERN_INFO "Tranceiver xmit %s recv %s\n",
			CTLR.msBits.hdlc_den ? "enabled ":"disabled",
			CTLR.msBits.hdlc_ren ? "enabled ":"disabled");
		lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
	}
	else
		printk( KERN_WARNING "HDLC:xcvr format=xmit recv\n");

	return count;
}
DEVICE_ATTR(xcvr, S_IRUGO | S_IWUSR, attr_read_xcvr, attr_set_xcvr);

/******** Transmit clock source... RW *************/
ssize_t
attr_read_txc_rxc(struct device *device, struct device_attribute *attr,
		  char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	return snprintf(buf, PAGE_SIZE, "Control Reg: = 0x%04x "
					"TX clock source = %s\n",
					CTLR.mnWord, CTLR.msBits.txc_rxc
					? "RxC": "Int");
}

ssize_t attr_set_txc_rxc(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	unsigned short val = 0;
	/* read current mode bits */
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	retval = sscanf(buf, "%hx", &val);
	val&=1; /* only 1 bit please */
	if(CTLR.msBits.txc_rxc != val)
	{
		printk(KERN_INFO "HDLC Changing TX clock Follow RX clock "
				"from %d to %d\n",
				CTLR.msBits.txc_rxc, val);
		CTLR.msBits.txc_rxc = val;
		lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
	}
	return count;
}
DEVICE_ATTR(txc_rxc, S_IRUGO | S_IWUSR, attr_read_txc_rxc, attr_set_txc_rxc);

/******** Receive Polarity... RW *************/

ssize_t
attr_read_rpol(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	return snprintf(buf, PAGE_SIZE, "Control Reg: = 0x%04x"
			"RX polarity = %c\n",
			CTLR.mnWord, CTLR.msBits.rx_pol ? '+':'-');
}

ssize_t attr_set_rpol(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	unsigned short val = 0;
	/* read current mode bits */
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	retval = sscanf(buf, "%hx", &val);
	val=val&1; /* only 1 bit please */
	if(CTLR.msBits.tx_pol != val)
	{
		printk("HDLC Changing RX polarity from %d to %d\n",
				CTLR.msBits.rx_pol, val);
		CTLR.msBits.rx_pol = val;
		lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
	}
	return count;
}
DEVICE_ATTR(rx_pol, S_IRUGO | S_IWUSR, attr_read_rpol, attr_set_rpol);

/******** Receive clock Polarity... RW *************/

ssize_t
attr_read_rcpol(struct device *device, struct device_attribute *attr, char* buf)
{
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	return snprintf(buf, PAGE_SIZE, "Control Reg: = 0x%04x "
					"RX clock polarity = %c\n",
			CTLR.mnWord, CTLR.msBits.rxc_pol ? '+':'-');
}

ssize_t attr_set_rcpol(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);
	unsigned short  *lpBaseReg = (unsigned short*)dev->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	unsigned short val = 0;
	/* read current mode bits */
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	retval = sscanf(buf, "%hx", &val);
	val=val&1; /* only 1 bit please */
	if(CTLR.msBits.tx_pol != val)
	{
		printk("HDLC Changing RX clock polarity from %d to %d\n",
				CTLR.msBits.rxc_pol, val);
		CTLR.msBits.rxc_pol = val;
		lpBaseReg[gnCTRL_OFFSET] = CTLR.mnWord;
	}
	return count;
}
DEVICE_ATTR(rxc_pol, S_IRUGO | S_IWUSR, attr_read_rcpol, attr_set_rcpol);

/******** Debug level... RW *************/

ssize_t
attr_read_dbgLevel(struct device *device, struct device_attribute *attr,
				   char* buf)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);

	uint16_t data = dev->attrDbgLevel;


	retval = snprintf(buf, PAGE_SIZE, "Debug level =  %hu\n",data);

	return retval;
}

ssize_t attr_set_dbgLevel(struct device *device, struct device_attribute *attr,
		const char* buf, size_t count)
{
	ssize_t retval = 0;
	struct hdlc_dev* dev = dev_get_drvdata(device);

	unsigned short data;

	retval = sscanf(buf, "%hu", &data);
	if(1 == retval)
	{
		dev->attrDbgLevel = data;
	}
	retval = count;
	return retval;
}
DEVICE_ATTR(debug_level, S_IRUGO | S_IWUSR,
			attr_read_dbgLevel, attr_set_dbgLevel);

/* Add all device attributes so they can be added using create group call */
static struct attribute* hdlc_attributes[] = {
	&dev_attr_version.attr,
	&dev_attr_lastmsg.attr,
	&dev_attr_status.attr,
	&dev_attr_stats.attr,
	&dev_attr_mode.attr,
	&dev_attr_addr.attr,
	&dev_attr_tx_pol.attr,
	&dev_attr_txc_pol.attr,
	&dev_attr_txc_rxc.attr,
	&dev_attr_rx_pol.attr,
	&dev_attr_rxc_pol.attr,
	&dev_attr_xcvr.attr,
	&dev_attr_debug_level.attr,
	NULL,
};
static struct attribute_group hdlc_attr_group = {
	.attrs = hdlc_attributes,
};


static void hdlc_init_devdata( struct hdlc_dev* fpga_hdlc)
{
	/* reset the frameFlag */
	fpga_hdlc->frameFlag = false;
	/* clear stats */
	memset(&fpga_hdlc->stats, '\0', sizeof(struct hdlc_stats));

	/* Clear fifo */
	kfifo_reset(&fpga_hdlc->read_fifo);
	kfifo_reset(&fpga_hdlc->frame_fifo);

}

/*////////////////////////////////////////////////////////////////////////////*/
/*// Character Device (charDev) */
/*////////////////////////////////////////////////////////////////////////////*/

int cdev_open(struct inode* inode, struct file* filp)
{
	struct hdlc_dev* fpga_hdlc;
	int retval = 0;

	unsigned int accmode = (filp->f_flags & O_ACCMODE);

	fpga_hdlc = container_of(inode->i_cdev, struct hdlc_dev, cdev);
	/* Save dev in file pointer for easy access by cdev functions */
	filp->private_data = fpga_hdlc;

	if(down_interruptible(&fpga_hdlc->ioSem))
		return -ERESTARTSYS;

	/* Setup device for reading */
	if(accmode == O_RDONLY || accmode == O_RDWR)
	{
		/* Device may be opened only once */
		if(fpga_hdlc->opened_read)
		{
			dev_err(fpga_hdlc->device,
					"Device already opened by"
					"another process\n");
			retval = -EUSERS;
			goto out;
		}

		dev_dbg(fpga_hdlc->device, "cdev_open: read-mode\n");
		/* Initiate a receiver software reset */
		/* NOT IMPLEMENTED IN FPGA */

		hdlc_init_regs(fpga_hdlc);
		hdlc_init_devdata(fpga_hdlc);

		/* Enable core */
		hdlc_enable_core(fpga_hdlc, true, true);
		/* Enable receiver */
		hdlc_enable_xcvr(fpga_hdlc, true, true);
		/* Set device opened */
		fpga_hdlc->opened_read = true;
	}

	/* Setup device for writing */
	if(accmode == O_WRONLY || accmode == O_RDWR)
	{
		/* Device may be opened only once */
		if(fpga_hdlc->opened_write)
		{
			dev_err(fpga_hdlc->device,
					"Device already opened by"
					"another process\n");
			retval = -EUSERS;
			goto out;
		}
		hdlc_init_devdata(fpga_hdlc);

		dev_dbg(fpga_hdlc->device, "cdev_open: write-mode\n");

		/* Set device opened */
		fpga_hdlc->opened_write = true;
	}

	out:
	up(&fpga_hdlc->ioSem);
	return retval;
}

int cdev_release(struct inode* inode, struct file* filp)
{
	struct hdlc_dev* dev = filp->private_data;

	unsigned int accmode = (filp->f_flags & O_ACCMODE);
	struct hdlc_frame_list *pos;
	struct hdlc_frame_list *tpos;

	if(down_interruptible(&dev->ioSem))
		return -ERESTARTSYS;

	if(accmode == O_RDONLY || accmode == O_RDWR)
	{
		/* Disable receiver */
		hdlc_enable_core(dev, false, true);
		/* Disable receiver */
		hdlc_enable_xcvr(dev, false, false);
		dev_dbg(dev->device, "Receiver disabled\n");

		/* Clear device opened */
		dev->opened_read = false;
	}
	if(accmode == O_WRONLY || accmode == O_RDWR)
	{
		/* Clear device opened */
		dev->opened_write = false;
	}

	/* clean up the pending frame list. */
	list_for_each_entry_safe(pos, tpos, &dev->xmit_frames.list,list){
		list_del(&pos->list);
		kfree(pos->data);
		kfree(pos);
	}
	dev_dbg(dev->device, "Char device closed");

	up(&dev->ioSem);
	return 0;
}

ssize_t
cdev_read(struct file* filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct hdlc_dev* fpga_hdlc = filp->private_data;
	unsigned int frame_size = 0;
	ssize_t retval		= 0;
	int status		= 0;
	unsigned copied		= 0;
	int fifo_size		= 0;
	unsigned short crc	= 0;

	/* device frameFlag is used to make sure we return to the app a frame at a */
	/* time cdev_read will be called multiple times for an application read call */
	/* because they are (supposed to) asking to get a 4K block and our frames */
	/* are _much_ smaller than that. returning 0 from cdev_read tells the kernel */
	/* we are done. */
	if(fpga_hdlc->frameFlag)
	{
		int fifo_len = kfifo_len(&fpga_hdlc->frame_fifo);
		fpga_hdlc->frameFlag = false;

		if(0 != fifo_len) {
			wake_up_interruptible(&fpga_hdlc->read_queue);
		}
		if(fpga_hdlc->attrDbgLevel) {
			dev_dbg(fpga_hdlc->device, "cdev_read: Frame complete %d frames remain %d bytes\n", fifo_len, kfifo_len(&fpga_hdlc->read_fifo));
		}
		return 0;
	}
	if(kfifo_len(&fpga_hdlc->frame_fifo) < sizeof(unsigned int))
	{

		/* Nothing to report... */
		if(0 == kfifo_len(&fpga_hdlc->read_fifo))
			return 0;
		/* there is a message in the pipeline, but not there yet, so wait for it */
		if(filp->f_flags & O_NONBLOCK)
			return -EAGAIN;

		if(fpga_hdlc->attrDbgLevel)
			dev_dbg(fpga_hdlc->device, "Read: frame_fifo empty, sleeping\n");

		if(wait_event_interruptible(fpga_hdlc->read_queue,
				(kfifo_len(&fpga_hdlc->frame_fifo) >= sizeof(unsigned int))))
			return -ERESTARTSYS;
	}

	/* Get Size of Next frame in RX Fifo from Frame Size Fifo */
	status = kfifo_out_uint(&fpga_hdlc->frame_fifo,&frame_size);
	/* Get Size of RX Fifo */
	fifo_size = kfifo_len(&fpga_hdlc->read_fifo);

	/* Check to Make sure the Frame to be read is not bigger than    */
	/*    what we have in the RX FIFO                                */
	/* This check had better not _ever_ fail! if it does we totally  */
	/*      got it wrong and should quit. */
	if (frame_size > fifo_size)
	{
		printk(KERN_WARNING "HDLC Message FIFO sauced! clearing... failed... "
			   "frame_size[%u] > fifo_size[%d]\n", frame_size, fifo_size);
		status = 0;
		kfifo_reset(&fpga_hdlc->read_fifo);
		kfifo_reset(&fpga_hdlc->frame_fifo);
		retval = -EINVAL;
	}

	/* If we got a valid Size for the Next frame */
	if(1 == status)
	{
		/* if the user has asked for less than the frame we log an error and */
		/* toss the frame out. This should never happen though as the app should */
		/* be trying to read 4K... we dictate the actual bytes returned based */
		/*  on the frame size. */
		if(count <= frame_size)
		{
			printk(KERN_WARNING "read failed... count[%d] <= frame_size[%d]\n",
				   count, frame_size);
			retval = -EINVAL;
			status = 0;
			while(count--)
				status += kfifo_out(&fpga_hdlc->read_fifo,&frame_size,1);
		}
		else
		{
			retval = kfifo_to_user(&fpga_hdlc->read_fifo,
								   buf,
								   frame_size,
								   &copied);
			if(copied > 1)
				crc = hdlc_crc(buf, copied-2);
			else
				crc = 0xFFFF;

			if(crc != *(unsigned short *)&buf[copied -2]) {
				dev_warn(fpga_hdlc->device,
						"Read: CRC mismatch msg:%02x%02x calc:%04x\n",
						buf[copied-1],buf[copied-2],crc);
				++fpga_hdlc->stats.recv_errs;
				retval = -EIO;
			}
			if(fpga_hdlc->attrDbgLevel)
				dev_dbg(fpga_hdlc->device,
					"Read: read %u of %u bytes crc = %04x %02x %02x %02x %02x\n",
					copied, frame_size, crc,buf[0],buf[1],buf[2],buf[3]);

			/* Copy Last message read to Last Msg buff */
			if(copied < 4096)
			{
				memcpy(&fpga_hdlc->lastmsg,buf,copied);
				fpga_hdlc->lastmsgsize = copied;
			}



			if(0 <= retval) {
				/* set the frame flag so next read call returns 0 to frame the frame */
				fpga_hdlc->frameFlag = true;
			}

		}
	}

	/* if retval is error then return error otherwise return num copied bytes */
	if(0 == retval)
		retval = copied;
	return retval;
}

unsigned int cdev_poll(struct file* filp, poll_table* wait)
{
	unsigned int retval = 0;
	struct hdlc_dev* fpga_hdlc = filp->private_data;

	/* Register polling client to read_queue so they can be */
	/* notified when new data to read */
	poll_wait(filp, &fpga_hdlc->read_queue, wait);

	if(kfifo_len(&fpga_hdlc->frame_fifo) >= sizeof(unsigned int))
		retval |= POLLIN | POLLRDNORM; /* readable */

	retval |= POLLOUT | POLLWRNORM; /* writable */

	return retval;
}


static ssize_t hdlc_write_frame(struct hdlc_dev* fpga_hdlc, const char *buf,
								size_t count, bool send_frame_start)
{

	ssize_t                 retval		= 0;
	ssize_t                 bytes_written	= 0;
	unsigned short          crc             = 0;
	volatile unsigned short	*tx_fifo_reg;
	volatile unsigned short	*lpBaseReg =
			(unsigned short*)fpga_hdlc->fpgadev->baseaddr;

	tuStatusTXFIFOReg TXFIFO;
	

	if(1 < fpga_hdlc->attrDbgLevel)
		dev_dbg(fpga_hdlc->device, "hdlc_write_frame %u bytes from %p\n",
				count, buf);
	if(2 > count) { /* minimum HDLC message is 4 bytes */
		dev_warn(fpga_hdlc->device, "hdlc_write_frame SHORT MSG:"
				"%d bytes = %02x %02x %02x %02x\n",
				 count, buf[0], buf[1], buf[2], buf[3]);
		++fpga_hdlc->stats.xmit_errs;
		return -EINVAL;
	}
	
	tx_fifo_reg = &lpBaseReg[gnTX_FIFO_OFFSET];

	/* for now, we are assuming there is space in the tx fifo */


	crc = hdlc_crc(buf, count);

	/* Send a frame start flag if needed */
	if(send_frame_start)
	{
		   /* frame flag */
		if(!xmit_byte(fpga_hdlc, tx_fifo_reg,HDLC_FLAG, true))
		{
			/* xmit_byte returned 0 FIFO is full even after timeout */
			return -ENOSPC;
		}
		fpga_hdlc->stats.bytes_xmit++;
	}
	/* while bytes remaining, send the message bytes */
	while(bytes_written < count)
	{
		if(!xmit_byte(fpga_hdlc, tx_fifo_reg, buf[bytes_written], false))
		{
			/* xmit_byte returned 0 FIFO is full even after timeout */
			return -ENOSPC;
		}
		bytes_written++;
	}

	retval = bytes_written;
	/* write the CRC */
	if(!xmit_byte(fpga_hdlc, tx_fifo_reg,crc&0xFF, false))
	{
		/* xmit_byte returned 0 FIFO is full even after timeout */
		return -ENOSPC;
	}
	bytes_written++;

	if(!xmit_byte(fpga_hdlc, tx_fifo_reg,(crc>>8)&0xFF, false))
	{
		/* xmit_byte returned 0 FIFO is full even after timeout */
		return -ENOSPC;
	}
	bytes_written++;

	/* write the end of frame flag */
	if(!xmit_byte(fpga_hdlc, tx_fifo_reg,HDLC_FLAG, true))
	{
		/* xmit_byte returned 0 FIFO is full even after timeout */
		return -ENOSPC;
	}
	fpga_hdlc->stats.bytes_xmit++;

	++fpga_hdlc->stats.frames_xmit;

	TXFIFO.mnWord   = lpBaseReg[gnSTATUS_TXFIFO_OFFSET];

	// NewAvg = Avg + (newval - avg)/count
	fpga_hdlc->stats.avg_tx_fifo_size = fpga_hdlc->stats.avg_tx_fifo_size +
		( TXFIFO.msBits.tx_fifo_available -
		  fpga_hdlc->stats.avg_tx_fifo_size) /
		fpga_hdlc->stats.frames_xmit;

	fpga_hdlc->stats.bytes_xmit += retval;

	return retval;
}

ssize_t
cdev_write(struct file* filp, const char __user *pcUserData,
		   size_t count, loff_t *f_pos)
{
	struct hdlc_dev* fpga_hdlc = filp->private_data;
	ssize_t rv = 0;
	unsigned short  *lpBaseReg = (unsigned short*)fpga_hdlc->fpgadev->baseaddr;
	tuCtrlReg CTLR;
	tuStatusReg SR;


	/* read current mode bits */
	CTLR.mnWord = lpBaseReg[gnCTRL_OFFSET];

	/* Verify there is a receive clock if it is needed */
	if(CTLR.msBits.txc_rxc){
		/* We need clock */
		SR.mnWord   = lpBaseReg[gnSTATUS_OFFSET];
		if(!SR.msBits.rx_clock_present){
			return -ECOMM;
		}
	}

	/* fpga_hdlc->io_mode being set means to batch transfers up and send with ioctl */
	if(fpga_hdlc->io_mode)
	{
		struct hdlc_frame_list* frame =
				kmalloc(sizeof(struct hdlc_frame_list),
						GFP_KERNEL);
		if(!frame)
			return -ENOMEM;

		frame->size = count;
		frame->data = kmalloc(count, GFP_KERNEL);
		if(!frame->data) {
			kfree(frame);
			return -ENOMEM;
		}
		if (copy_from_user(frame->data, pcUserData, count) == 0) {
			/* dev_dbg(fpga_hdlc->device, "cdev_write %d bytes\n", count); */
			/* add the frame to the list */
			if(down_interruptible(&fpga_hdlc->ioSem))
			{
				kfree(frame->data);
				kfree(frame);
				return -ERESTARTSYS;
			}
			list_add_tail(&(frame->list),
				      &(fpga_hdlc->xmit_frames.list));
			up(&fpga_hdlc->ioSem);
			rv = count;
		}
		else
		{
			kfree(frame->data);
			kfree(frame);
			dev_err(fpga_hdlc->device, "Unable to allocate memory for packet.");
			rv = -ENOMEM;
		}
	}
	else
	{
		if(down_interruptible(&fpga_hdlc->ioSem))
			rv = -ERESTARTSYS;
		else
		{

			rv = hdlc_write_frame(fpga_hdlc, pcUserData, count, true);

			/* Now that we have sent the whole frame, need to send frame */
			/* complete command */
			hdlc_send_frames(fpga_hdlc);
			up(&fpga_hdlc->ioSem);
		}
	}
	return rv;
}


static long	hdlc_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int			err = 0;
	int			retval = 0;
	struct hdlc_dev* fpga_hdlc = filp->private_data;
	uint32_t	tmp;

	/* Check type and command number */
	if (_IOC_TYPE(cmd) != HDLC_IOC_MAGIC)
	{
		dev_err(fpga_hdlc->device, "IOCTL type incorrect %x, %d != %d\n",cmd,
				_IOC_TYPE(cmd) , HDLC_IOC_MAGIC);
		return -ENOTTY;
	}
	/* Check access direction once here; don't repeat below.
	 * IOC_DIR is from the user perspective, while access_ok is
	 * from the kernel perspective; so they look reversed.
	 */
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE,
						 (void __user *)arg, _IOC_SIZE(cmd));
	if (err == 0 && _IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ,
						 (void __user *)arg, _IOC_SIZE(cmd));
	if (err)
	{
		dev_err(fpga_hdlc->device, "IOCTL access error MCD= %x IOC_DIR=%d\n",
				cmd, _IOC_DIR(cmd));
		return -EFAULT;
	}

	/* use the buffer lock here for triple duty:
	 *  - prevent I/O (from us) so calling spi_setup() is safe;
	 *  - prevent concurrent SPI_IOC_WR_* from morphing
	 *    data fields while SPI_IOC_RD_* reads them;
	 *  - SPI_IOC_MESSAGE needs the buffer locked "normally".
	 */
	/* mutex_lock(&spidev->buf_lock); */

	switch (cmd) {
		/* read requests */
	case HDLC_IOC_RD_MODE:
		retval = __put_user(fpga_hdlc->io_mode,
							(__u8 __user *)arg);
		break;

		/* write requests */
	case HDLC_IOC_WR_MODE:
		retval = __get_user(tmp, (u8 __user *)arg);
		if (retval == 0) {
			fpga_hdlc->io_mode = tmp?true:false;
			dev_dbg(fpga_hdlc->device, "hdlc mode %02x\n", tmp);
		}
		break;
	case HDLC_IOC_SEND_FRAME:
		/* kick off a send */
		if(down_interruptible(&fpga_hdlc->ioSem))
			retval = -ERESTARTSYS;
		else
		{
			int num_msgs = 0;
			int write_return = 0;
			struct hdlc_frame_list *pos;
			struct hdlc_frame_list *tpos;
			list_for_each_entry_safe(pos, tpos, &fpga_hdlc->xmit_frames.list,list){
				if(write_return >= 0)
				{
					write_return = hdlc_write_frame(fpga_hdlc, pos->data, pos->size, (0==num_msgs));
				}
				list_del(&pos->list);
				kfree(pos->data);
				kfree(pos);
				++num_msgs;
			}
			/* Now that we have sent the whole frame, need to send frame */
			/* complete command */
			if(write_return >= 0)
				hdlc_send_frames(fpga_hdlc);
			up(&fpga_hdlc->ioSem);

			if(write_return >= 0)
			{
				if(fpga_hdlc->attrDbgLevel)
				{
					dev_dbg(fpga_hdlc->device,
					"HDLC_IOC_SEND_FRAME send %d frames\n",
					num_msgs);
				}
			} else {
				retval = write_return;
			}

		}
		break;

	default:
		dev_warn(fpga_hdlc->device, "HDLC_IOC unknown IOCTL 0x%x\n", cmd);

		break;
	}

	/* mutex_unlock(&spidev->buf_lock); */
	return retval;
}

struct file_operations hdlc_fops =
{
	.owner		= THIS_MODULE,
	.open		= cdev_open,
	.release	= cdev_release,
	.read		= cdev_read,
	.write		= cdev_write,
	.poll		= cdev_poll,
	.unlocked_ioctl = hdlc_ioctl,
};

/*//////////////////////////////////// */
/*// Device Init and Cleanup */
/*//////////////////////////////////// */

static int __init hdlc_setup_cdev(struct hdlc_dev* fpga_hdlc)
{
	int err;
	/* Init the char dev struct and register the file operation functions */
	cdev_init(&fpga_hdlc->cdev, &hdlc_fops);
	fpga_hdlc->cdev.owner = THIS_MODULE;
	/* Add char dev to the system */
	if((err = cdev_add(&fpga_hdlc->cdev, fpga_hdlc->devnum, 1)))
		dev_err(fpga_hdlc->device, "Error %d adding hdlc cdev\n", err);
	return err;
}




static void hdlc_cleanup(struct hdlc_dev* fpga_hdlc)
{

	if(kfifo_initialized(&fpga_hdlc->read_fifo))
		kfifo_free(&fpga_hdlc->read_fifo);

	if(kfifo_initialized(&fpga_hdlc->frame_fifo))
		kfifo_free(&fpga_hdlc->frame_fifo);

	if(fpga_hdlc->device != NULL && !IS_ERR(fpga_hdlc->device))
		sysfs_remove_group(&fpga_hdlc->device->kobj, &hdlc_attr_group);

	if(fpga_hdlc->device != NULL && !IS_ERR(fpga_hdlc->device) &&
	   fpga_hdlc->class != NULL && !IS_ERR(fpga_hdlc->class))
		device_destroy(fpga_hdlc->class, fpga_hdlc->devnum);

	if(fpga_hdlc->class != NULL && !IS_ERR(fpga_hdlc->class))
		class_destroy(fpga_hdlc->class);

	if(!IS_ERR(&fpga_hdlc->cdev))
		cdev_del(&fpga_hdlc->cdev);

	if(fpga_hdlc->devnum)
		unregister_chrdev_region(fpga_hdlc->devnum, HDLC_DEV_NUMS);
}


/**
 */
static int fpga_hdlc_IrqHandler(struct fpga_device* apDev)
{
	struct hdlc_dev *fpga_hdlc;
	volatile unsigned short  *lpBaseReg;
	tuIsrReg ISR;
	tuIerReg IER;
	tuFrameSizeReg FSR;
	tuStatusReg STR;
	tuStatusRXFIFOReg RXFIFO;
	uint16_t items_in_fifo  = 0;
	register int loop_count = 100; /* MAX # of iterations */
	int bytes_received = 0;
	int fifo_size = 0;
	int fifo_frame_size = 0;
	uint8_t buf[8]= { 0,0,0,0 ,0,0,0,0 };
	uint8_t foo[4] = { 0,0,0,0 };
	int init = 0;
	int ii=0;


	fpga_hdlc = dev_get_drvdata(&apDev->dev);
	/* dev_dbg(dev->device, "IRQ called\n"); */
	lpBaseReg = (volatile unsigned short*)fpga_hdlc->fpgadev->baseaddr;
	ISR.mnWord = lpBaseReg[gnISR_OFFSET];
	IER.mnWord = lpBaseReg[gnIER_OFFSET];
	STR.mnWord = lpBaseReg[gnSTATUS_OFFSET];
	/* If a frame rx interrupt */
	if(ISR.msBits.int_frame_rx) {

		tuRxFifoReg RXF;

		uint8_t byte=0;


		/* This register appears to be broken and always reporting 0 */
		/*		if(0 == lpBaseReg[gnFCSIZE_OFFSET]) { */
		/*			dev_warn(fpga_hdlc->device, "ISR called with empty FS FIFO\n"); */
		/*		} */
		/* Read receive buffer count */
		/* reading the rx count clears the interrupt */
		FSR.mnWord = lpBaseReg[gnFRAME_SIZE_OFFSET];

		/* Calculate amount of data in hdlc fifo */
		items_in_fifo = FSR.msBits.num_bytes;

		while(loop_count && items_in_fifo) {
			fifo_size = 0;
			fifo_frame_size = 0;

			ii=0;

			for( init = 0; init < 8; init++)
				buf[init]= 0;

			for( init = 0; init < 4; init++)
				foo[init] = 0;
			/* now that we know we got an interrupt */

			fifo_size = kfifo_len(&fpga_hdlc->read_fifo);
			fifo_frame_size = kfifo_len(&fpga_hdlc->frame_fifo);

			/* Check if Error Bit is set MSB (16 bit reg)*/
			//if( 0x8000 == (0x8000 & (items_in_fifo)) ) {
			if( FSR.msBits.frame_error ) {

				items_in_fifo = items_in_fifo & 0x7F;
				while(items_in_fifo > 0) {
					RXF.mnWord = lpBaseReg[gnRX_FIFO_OFFSET];
					items_in_fifo--;
					if(bytes_received < 4)
					{
						buf[bytes_received]= byte;
					}
					else if(items_in_fifo < 4)
					{
						buf[7-items_in_fifo]=byte;
					}


					++bytes_received;
				}
				dev_warn(fpga_hdlc->device, "IRQ ERROR MSG: "
					"ISR: Frame complete %u bytes - %02x %02x %02x %02x -- %02x %02x %02x %02x\n",
						bytes_received,
						buf[0],buf[1],buf[2],buf[3],
						buf[4],buf[5],buf[6],buf[7]
						);
				++fpga_hdlc->stats.recv_errs;

			/* Check FIFO Sizes */
			} else if( ((fifo_size + items_in_fifo) > DRIVER_FIFO_SIZE) ||
			    (fifo_frame_size == FRAME_FIFO_SIZE) )
			{
				while(items_in_fifo > 0) {
					RXF.mnWord = lpBaseReg[gnRX_FIFO_OFFSET];
					items_in_fifo--;
					++bytes_received;
				}
				dev_warn(fpga_hdlc->device, "IRQ FIFO FULL: "
					 "Received: %d  FIFO Size: %d  MAXFIFO: %d  FRAMEFIFO Size: %d FRAME MAX: %d\n",
					 bytes_received, fifo_size, DRIVER_FIFO_SIZE,
					 fifo_frame_size, FRAME_FIFO_SIZE);
			}
			/* minimum HDLC message is 4 bytes */
			else if(4 > items_in_fifo) {				

				while(items_in_fifo > 0) {

					RXF.mnWord = lpBaseReg[gnRX_FIFO_OFFSET];
					foo[ii++] = RXF.msBits.rx_byte;
					items_in_fifo--;
					++bytes_received;
				}
				dev_warn(fpga_hdlc->device, "IRQ SHORT MSG: "
					"%d bytes = %02x %02x %02x %02x\n",
					 ii, foo[0], foo[1], foo[2], foo[3]);
				++fpga_hdlc->stats.recv_errs;

			/* Everything checks out - Read it in */
			} else {
				/* Read bytes into fifo */
				while(items_in_fifo > 0) {

					RXF.mnWord = lpBaseReg[gnRX_FIFO_OFFSET];
					byte = RXF.msBits.rx_byte;
					kfifo_in(&fpga_hdlc->read_fifo, &byte, 1);
					items_in_fifo--;

					if(bytes_received < 4)
					{
						buf[bytes_received]= byte;
					}
					else if(items_in_fifo < 4)
					{
						buf[7-items_in_fifo]=byte;
					}


					++bytes_received;
				}

				/* Add the frame size to the frame fifo */
				/*          for cdev_read and cdev_poll */
				if(fpga_hdlc->attrDbgLevel)
					dev_dbg(fpga_hdlc->device,
						"ISR: Frame complete %u bytes - %02x %02x %02x %02x -- %02x %02x %02x %02x\n",
							bytes_received,
							buf[0],buf[1],buf[2],buf[3],
							buf[4],buf[5],buf[6],buf[7]
							);
				if(bytes_received) {

					/*  Add Size of frame to Frame Fifo */
					kfifo_in_uint(&fpga_hdlc->frame_fifo,
						      bytes_received);

					/* Reset bytes received for next frame */
					bytes_received = 0;
					wake_up_interruptible(&fpga_hdlc->read_queue);
				}
			}

			/* Increment Stats */
			++fpga_hdlc->stats.frames_recv;
			fpga_hdlc->stats.bytes_recv += bytes_received;

			/* Calculate the Average Size of the RX FIFO */
			RXFIFO.mnWord   = lpBaseReg[gnSTATUS_RXFIFO_OFFSET];
			// NewAvg = Avg + (newval - avg)/count
			fpga_hdlc->stats.avg_rx_fifo_size = fpga_hdlc->stats.avg_rx_fifo_size +
				( RXFIFO.msBits.rx_fifo_available -
				  fpga_hdlc->stats.avg_rx_fifo_size) /
				fpga_hdlc->stats.frames_recv;

			/* Calculate amount of data in hdlc fifo */
			FSR.mnWord = lpBaseReg[gnFRAME_SIZE_OFFSET];
			items_in_fifo = FSR.msBits.num_bytes;

			/* Decrement Loop Counter */
			--loop_count;
		}
		if(!loop_count)
			dev_warn(fpga_hdlc->device,
				 "Excessive iterations in ISR (100)\n");

		if(100 == loop_count)
		{
			++fpga_hdlc->stats.recv_errs;
			dev_dbg(fpga_hdlc->device,
				"ISR ERROR Called with no FRAME To read\n");
		}

		if(fpga_hdlc->attrDbgLevel &&(99 != loop_count))
			dev_dbg(fpga_hdlc->device,
					"Leaving ISR %d iterations\n",
					100-loop_count);

	}
	/* TODO probably should re-enable the interrupt in the FPGA */
	return 0;
}

/**
 * This routine is called when a device is removed from the FPGA bus.
 *
 * \param[in] dev pointer to the device being removed.
 */
static int fpga_hdlc_remove(struct device *dev)
{
	int rv = 0;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	struct hdlc_dev  *fpga_hdlc = dev_get_drvdata(dev);

	hdlc_cleanup(fpga_hdlc);
	/* Disable receiver */
	hdlc_enable_xcvr(fpga_hdlc, false, false);

	/* remove irq mapping */
	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
				   fpgadev->coreversion.ver0.bits.vector,
				   0);

	return rv;
}

static int fpga_hdlc_probe(struct device *dev);

/**
 * The driver object.  The information here must be common for
 * all of the potential drivers in the system.
 */
static struct fpga_driver fpga_hdlc_driver = {
	.id		= CORE_ID_HDLC, /** must match value in core_ids.h */
	.version	= HDLC_DRIVER_VERSION_STRING,
	.IrqHandler	= fpga_hdlc_IrqHandler,
	.probe		= fpga_hdlc_probe,
	.remove		= fpga_hdlc_remove,
	.driver 	= {
		.name 	= "fpga_hdlc",
		.owner 	= THIS_MODULE,
	},
};

/**
 */
static int fpga_hdlc_probe(struct device *dev)
{
	int rv = 0;
	struct hdlc_dev   *fpga_hdlc = NULL;
	unsigned short    *lpBaseReg = NULL;

	/* Base minor is the minor starting address for the charDev alloc */
	int baseminor = 0;

	struct fpga_device *fpgadev = to_fpga_device(dev);

	printk(KERN_NOTICE "fpga_hdlc_probe here..\n");

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	fpga_hdlc = (struct hdlc_dev *)kmalloc(sizeof(struct hdlc_dev),
							   GFP_KERNEL);
	if (fpga_hdlc == NULL)
	{
		rv = -ENOMEM;
		goto probe_bail;
	}
	fpga_hdlc->device  = dev;

	fpga_hdlc->fpgadev = fpgadev;

	dev_set_drvdata(dev, fpga_hdlc);

	dev_info(&fpgadev->dev, "FPGA HDLC Loaded\n");
	/* charDevice starts as unopened */
	fpga_hdlc->opened_read = false;
	fpga_hdlc->opened_write = false;

	hdlc_init_devdata(fpga_hdlc);
#ifdef DEBUG
	fpga_hdlc->attrDbgLevel   = 1;
#else
	fpga_hdlc->attrDbgLevel   = 0;
#endif

	fpga_hdlc->recvAddr       = 0xFF;

	printk(KERN_NOTICE "HDLC Module init - built "__DATE__":"__TIME__"\n");

	/* Get device number for this driver */
	rv = alloc_chrdev_region(&fpga_hdlc->devnum, baseminor,
				 HDLC_DEV_NUMS, "hdlc");
	if(rv < 0)
	{
		printk(KERN_WARNING "Hdlc failed to allocate device number %d\n",
			   MAJOR(fpga_hdlc->devnum));
		goto probe_bail;
	}

	/* Initialize the HDLC core registers to "sane" values */
	hdlc_init_regs(fpga_hdlc);

	/* Setup semaphore */
	sema_init(&fpga_hdlc->ioSem, 1 );

	/* Setup character device */
	rv = hdlc_setup_cdev(fpga_hdlc);
	if(rv < 0)
		goto probe_bail;

	/* Create class so device can be easily found under /sys/class/hdlc/hdlc */
	fpga_hdlc->class = class_create(THIS_MODULE, "hdlc");
	if(IS_ERR(fpga_hdlc->class))
	{
		printk(KERN_WARNING "Failed to create class\n");
		rv = PTR_ERR(fpga_hdlc->class);
		goto probe_bail;
	}

	/* Create device so that udev can autocreate /dev/hdlc */
	fpga_hdlc->device = device_create(fpga_hdlc->class, NULL,
					  fpga_hdlc->devnum, fpga_hdlc,
					  "hdlc_device");
	if(IS_ERR(fpga_hdlc->device))
	{
		printk(KERN_WARNING "Failed to create device\n");
		rv = PTR_ERR(fpga_hdlc->device);
		goto probe_bail;
	}
	/* Create all the sysfs device parameters in one go */
	if((rv = sysfs_create_group(&fpga_hdlc->device->kobj, &hdlc_attr_group)))
	{
		dev_err(fpga_hdlc->device, "Failed to add attributes group\n");
		goto probe_bail;
	}

	/* Allocate memory for fifos */
	if((rv = kfifo_alloc(&fpga_hdlc->read_fifo, DRIVER_FIFO_SIZE, GFP_KERNEL)))
	{
		dev_err(fpga_hdlc->device, "Failed to allocate read_fifo\n");
		goto probe_bail;
	}

	/* Allocate memory for frame fifo */
	if((rv = kfifo_alloc(&fpga_hdlc->frame_fifo, FRAME_FIFO_SIZE, GFP_KERNEL)))
	{
		dev_err(fpga_hdlc->device, "Failed to allocate frame_fifo\n");
		goto probe_bail;
	}

	/* Readies wait queue. Used to wake up clients select()ed on the char device. */
	init_waitqueue_head(&fpga_hdlc->read_queue);
	init_waitqueue_head(&fpga_hdlc->interrupt_queue);

	/* Disable receiver */
	hdlc_enable_xcvr(fpga_hdlc, false, false);
	/* reset the core */
	hdlc_enable_core(fpga_hdlc, false, true);
	INIT_LIST_HEAD(&fpga_hdlc->xmit_frames.list);

	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
				   fpgadev->coreversion.ver0.bits.vector,
				   1);

	dev_dbg(fpga_hdlc->device, "Hdlc driver successfully loaded\n");

	return rv;

	probe_bail:
	hdlc_cleanup(fpga_hdlc);
	return rv;





}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init fpga_hdlc_init(void)
{
	int ret;

	printk(KERN_NOTICE "fpga_hdlc_init here..\n");
	ret = register_fpga_driver(&fpga_hdlc_driver);
	if (ret)
	{
		printk(KERN_ALERT "Unable to register fpga_hdlc_driver\n");
		goto exit_bail;
	}

	return 0;

	exit_bail: /* Uh-Oh */
		return -1;
	}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void fpga_hdlc_exit(void)
{
	printk(KERN_NOTICE "fpga_hdlc_exit here..\n");
	driver_unregister(&fpga_hdlc_driver.driver);
}

module_init(fpga_hdlc_init);
module_exit(fpga_hdlc_exit);
