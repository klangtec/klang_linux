/*
 * fpga_pwm.c
 *
 *  Created on: December, 2012
 *      Author: Dominic Giambo
 */

#include "fpga.h"
#include "common/fpga/fpga_pwm.h"
#include "common/fpga/core_ids.h"

#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/semaphore.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/version.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <asm/io.h>
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dominic Giambo <dominic@criticallink.com");
MODULE_DESCRIPTION("Driver for MityDSP-L138 FPGA Based PWMInterface");

/**
 * This first version of the PWM core is quite limited in scope.
 * The following restrictions apply
 * - Only a single core is supported
 * - Only coninuous mode is supported
 * - Only Pulse 1 is used
 * - Delay is set to 0
 * - No interrupts are used
 */
struct fpga_pwmctlr {
	/* Device in sysfs */
	struct device *device;
	
	struct fpga_device*	fpgadev;
	
	/* Contains the registered number for this device */
	dev_t devnum;
	
	/* Sysfs class */
	struct class *class;
	
	/* Period in microseconds */
	unsigned int period_us;
	/* Pulse length in microseconds */
	unsigned int pulse_us;
	/* Pulse enable */
	int on;
};

static int pwm_set(struct fpga_pwmctlr* fpga_pwm_ctlr, struct device *dev, 
				   int period_us, int pulse_us, int on)
{
	unsigned short *lpBaseReg = (unsigned short*)fpga_pwm_ctlr->fpgadev->baseaddr;
	tuPWMCSR	CSR;
	tuPWMCD1	CD1;
	unsigned int divisor;
	unsigned int divisor_hz;
	unsigned int desired_div_clk;
	unsigned int hz;
	unsigned int PRD;
	tsPWMVals Vals;
	unsigned int start;
	unsigned int end;
	int emif_rate = 100000000; /* 100MHz = 10ns period*/
	
	// Configure the Repitition rate. Configure clock divisor and PRD
	
	/* Attempt to retrieve EMIF clock rate, otherwise use default */
	struct clk *emif_clk;
	emif_clk = clk_get(NULL, "aemif");
	if (!IS_ERR(emif_clk))
		emif_rate = clk_get_rate(emif_clk);
	
	//printk("emif rate %d\r\n", emif_rate);
	
	// Fixed point math FTW!!
	hz = ((1000000<<8)/(period_us)); // 24.8 format
	
	if (hz == 0)
	{
		dev_warn(dev,"Period out of valid range!");
		return -1;	
	}
	//printk("hz %d.%d\r\n", (hz & 0xFFFFFF00) >> 8, ((hz & 0x0FF)*100)/255);
	desired_div_clk = (hz * 65535); // 24.8 * 32 = 24.8
	//printk("desired_div_clk %d.%d\r\n", (desired_div_clk & 0xFFFFFF00) >> 8, (((desired_div_clk & 0x0FF)*100)/255));
	divisor = (emif_rate<<4)/(desired_div_clk>>4); // 28.4 / 28.4 = 32 bit format
	//printk("divisor %d\r\n", divisor);
	if (divisor < 17) 
	{
		divisor = 17;
		//dev_warn(dev,"divisor corrected to minimum of %d\r\n", divisor);
	}
	
	if (divisor > 1<<24)
	{
		dev_warn(dev,"Bad Repetition Rate requested (%d hz)\r\n", hz);
		return -1;
	}

	divisor_hz = (emif_rate<<4) / (divisor<<4); // 28.4 / 28.4 = 32
	//printk("divisor_hz %d\r\n", divisor_hz);

	PRD = ((divisor_hz<<8) / hz); // 24.8 / 24.8 = 32
	// Clip to max size of unsigned short
    if (PRD > 0xFFFF)
		PRD = 0xFFFF;
		
    //printk("PRD %d\r\n", PRD);
    
	divisor -= 1;
	PRD -= 1;
    
    // program the divisor
	CSR.word = ioread16(lpBaseReg+gnCSR_OFFSET);
	CSR.bits.clk_en = (on==1)?1:0;
	
	CD1.word = ioread16(lpBaseReg+gnCD1_OFFSET);
	CSR.bits.clk_div_low = divisor&0x0FFF;
	CD1.bits.clk_div_high = divisor>>12;
	iowrite16(CSR.word, lpBaseReg+gnCSR_OFFSET);
	iowrite16(CD1.word, lpBaseReg+gnCD1_OFFSET);
	
	// program the PRD
	iowrite16(PRD, lpBaseReg+gnPRD_OFFSET);
	
	// Pulse Config
	start = (PRD * (((period_us-pulse_us)<<8)/period_us))>>8; // 24.8 / 32.0 = 24.8
	end = PRD;

	//printk("Start Count %d\r\n", start);
	//printk("End Count %d\r\n", end);

	Vals.cnta = start;
	Vals.cntb = end;
	Vals.tuFlags.word = 0;
	Vals.tuFlags.bits.zero_action = 0;
	Vals.tuFlags.bits.force_zero = 0;
	Vals.tuFlags.bits.force_one = 0;
	Vals.tuFlags.bits.a_action = PWM_ACTION_SET_ONE;
	Vals.tuFlags.bits.b_action = PWM_ACTION_SET_ZERO;

	iowrite16(0, lpBaseReg+gnADDR_OFFSET);
	iowrite16(Vals.cnta, lpBaseReg+gnVALS_OFFSET);

	iowrite16(1, lpBaseReg+gnADDR_OFFSET);
	iowrite16(Vals.cntb, lpBaseReg+gnVALS_OFFSET);
	
	iowrite16(2, lpBaseReg+gnADDR_OFFSET);
	iowrite16(Vals.tuFlags.word, lpBaseReg+gnVALS_OFFSET);

	fpga_pwm_ctlr->period_us = period_us;
	fpga_pwm_ctlr->pulse_us = pulse_us;
	fpga_pwm_ctlr->on = on;

	return 0;
}


static ssize_t fpga_pwmctlr_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_pwmctlr* fpga_pwmctlr = (struct fpga_pwmctlr*)dev_get_drvdata(dev);
	return sprintf(buf, "Period (us): %d\n Pulse Width (us): %d\n Enabled: %d\n",fpga_pwmctlr->period_us,
				   fpga_pwmctlr->pulse_us, fpga_pwmctlr->on);
}

static ssize_t fpga_pwmctlr_set(struct device *dev, 
		struct device_attribute *attr, const char *buf, size_t count)
{
	int period_us,pulse_us, on;
	struct fpga_pwmctlr* fpga_pwmctlr = (struct fpga_pwmctlr*)dev_get_drvdata(dev);

	if(3 == sscanf(buf,"%d,%d,%d", &period_us,&pulse_us, &on)) {
		if(0 != pwm_set(fpga_pwmctlr, dev, period_us, pulse_us, on))
			dev_warn(dev, "pwm_set failed\n");
	} else {
			dev_warn(dev, "invalid format, must be <per_us>,<on_us>,<on>\n");
	}

	return count;
}
static DEVICE_ATTR(pwmctlr, S_IRUGO | S_IWUSR, fpga_pwmctlr_show, fpga_pwmctlr_set);

static struct attribute *fpga_pwmctlr_attributes[] = {
		&dev_attr_pwmctlr.attr,
		NULL,
};

static struct attribute_group fpga_pwmctlr_attr_group = {
		.attrs = fpga_pwmctlr_attributes,
};


/**
 * This routine is called when a device is removed from the FPGA bus.
 *
 * \param[in] dev pointer to the device being removed.
 */
static int fpga_pwmctlr_remove(struct device *dev)
{
	int rv = 0;
	struct fpga_pwmctlr *fpga_pwmctlr = dev_get_drvdata(dev);

	sysfs_remove_group(&dev->kobj, &fpga_pwmctlr_attr_group);

	if(fpga_pwmctlr->class != NULL && !IS_ERR(fpga_pwmctlr->class))
		class_destroy(fpga_pwmctlr->class);
		
	kfree(fpga_pwmctlr);

	return rv;
}

static int fpga_pwmctlr_probe(struct device *dev);

/**
 * The driver object.  The information here must be common for
 * all of the potential drivers in the system.
 */
static struct fpga_driver fpga_pwmctlr_driver = {
	.id		= CORE_ID_PWM, /** must match value in core_ids.h */
	.version	= "PWM Controller Driver 1.1",
	.probe		= fpga_pwmctlr_probe,
	.remove		= fpga_pwmctlr_remove,
	.driver 	= {
		.name 	= "fpga_pwmctlr",
		.owner 	= THIS_MODULE,
	},
};

/**
 */
static int fpga_pwmctlr_probe(struct device *dev)
{
	int rv = 0;
	struct fpga_pwmctlr   *fpga_pwmctlr = NULL;
	unsigned short    *lpBaseReg = NULL;
	struct fpga_device *fpgadev = to_fpga_device(dev);

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	fpga_pwmctlr = (struct fpga_pwmctlr *)kmalloc(sizeof(struct fpga_pwmctlr),
							GFP_KERNEL);
							
	if (fpga_pwmctlr == NULL)
	{
		rv = -ENOMEM;
		goto probe_bail;
	}
	
	/* Get device number for this driver */
	rv = alloc_chrdev_region(&fpga_pwmctlr->devnum, 1, 1, "pwm");
							
	fpga_pwmctlr->class = class_create(THIS_MODULE, "pwm");
	if(IS_ERR(fpga_pwmctlr->class))
	{
		printk(KERN_WARNING "Failed to create class\n");
		rv = PTR_ERR(fpga_pwmctlr->class);
		goto probe_bail;
	}
	
	fpga_pwmctlr->device = device_create(fpga_pwmctlr->class, NULL,
					  fpga_pwmctlr->devnum, fpga_pwmctlr,
					  "pwmctrl");

	rv = sysfs_create_group(&fpga_pwmctlr->device->kobj, &fpga_pwmctlr_attr_group);
	if (rv)
	{
		dev_err(dev, "%s failed to add attributes group - %d\n", __func__, rv);
		goto probe_bail;
	}

	fpga_pwmctlr->fpgadev = fpgadev;
	fpga_pwmctlr->period_us = 0;
	fpga_pwmctlr->pulse_us = 0;
	fpga_pwmctlr->on = 0;
	
	dev_set_drvdata(dev, fpga_pwmctlr);

	dev_info(&fpgadev->dev, "FPGA PWMCTRL Loaded\n");

	return rv;

probe_bail:
	return rv;
}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init fpga_pwmctlr_init(void)
{
	int ret;

	ret = register_fpga_driver(&fpga_pwmctlr_driver);
	if (ret)
	{
		printk(KERN_ALERT "Unable to register fpga_pwmctlr_driver\n");
		goto exit_bail;
	}

	return 0;

exit_bail: /* Uh-Oh */
	return -1;
}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void fpga_pwmctlr_exit(void)
{
	driver_unregister(&fpga_pwmctlr_driver.driver);
}

module_init(fpga_pwmctlr_init);
module_exit(fpga_pwmctlr_exit);
