/*
 * ads7843.c
 *
 *  Created on: Apr 20, 2010
 *      Author: Mike Williamson
 */

#include "common/fpga/ads7843.h"
#include "common/fpga/core_ids.h"
#include "fpga.h"
#include <linux/input.h>
#include <linux/timer.h>
#include <linux/slab.h>
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Williamson <michael.williamson@criticallink.com");
MODULE_DESCRIPTION("Driver for MityDSP-L138 FPGA Based ADS7843 Touchscreen Interface");

typedef enum {
	eeNoEvent = 0,
	eePenDown = 1,
	eeDataChange = 2,
} tsEvent;

/**
 *  this is the device driver specific parameters tied to each ads7843 device in the system
 *  (the object data)
 */
struct ads7843 {
	struct input_dev *input;
	char              phys[32];
	char              name[32];
	unsigned int      InterruptCount;
	tsEvent           lastEvent;
	struct timer_list timer;
	unsigned int      max_x;
	unsigned int      max_y;
	unsigned int      flip_x;
	unsigned int      flip_y;
	unsigned int      swap_xy;
};

static void ads7843_get_pos(struct device* dev, unsigned int* x, unsigned int* y)
{
	unsigned int local_x, local_y;
	tuPositionReg lsPos;
	struct ads7843* driver_data;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	unsigned int     *lpBaseReg = NULL;

	driver_data = (struct ads7843*)dev_get_drvdata(dev);
	lpBaseReg = (unsigned int*)fpgadev->baseaddr;

	/** enable the device and the interrupts */
	lsPos.mnLword = lpBaseReg[gnPOSITION_OFFSET];

	local_x = lsPos.msBits.mnXCount * driver_data->max_x / 4096;
	local_y = lsPos.msBits.mnYCount * driver_data->max_y / 4096;

	if (driver_data->flip_x)
	{
		local_x = driver_data->max_x - local_x;
	}

	if (driver_data->flip_y)
	{
		local_y = driver_data->max_y - local_y;
	}

	if (driver_data->swap_xy)
	{
		*x = local_y;
		*y = local_x;
	}
	else
	{
		*y = local_y;
		*x = local_x;
	}

}

static ssize_t ads7843_raw_pos_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	int x,y;
	ads7843_get_pos(dev, &x, &y);
	return sprintf(buf, "%5d %5d\n", x, y);
}
static DEVICE_ATTR(raw_pos, S_IRUGO, ads7843_raw_pos_show, NULL);

static int ads7843_is_pendown(struct device* dev)
{
	tuPositionReg lsPos;
	struct ads7843* driver_data;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	unsigned int     *lpBaseReg = NULL;

	driver_data = (struct ads7843*)dev_get_drvdata(dev);
	lpBaseReg = (unsigned int*)fpgadev->baseaddr;

	/** enable the device and the interrupts */
	lsPos.mnLword = lpBaseReg[gnPOSITION_OFFSET];

	return lsPos.msBits.mbPenDown;
}

static ssize_t ads7843_pen_down_show(struct device *dev,
            struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", ads7843_is_pendown(dev));
}
static DEVICE_ATTR(pen_down, S_IRUGO, ads7843_pen_down_show, NULL);

static ssize_t ads7843_max_store(struct device *dev, struct device_attribute *attr,
			                       const char *buf, size_t count)
{
	struct ads7843* driver_data;
	driver_data = (struct ads7843*)dev_get_drvdata(dev);

	sscanf(buf,"%u %u",&driver_data->max_x, &driver_data->max_y);

	return count;
}

static ssize_t ads7843_max_show(struct device *dev,
            struct device_attribute *attr, char *buf)
{
	struct ads7843* driver_data;
	driver_data = (struct ads7843*)dev_get_drvdata(dev);

	return sprintf(buf, "%u %u\n", driver_data->max_x, driver_data->max_y);
}
static DEVICE_ATTR(max, S_IRUGO | S_IWUSR, ads7843_max_show, ads7843_max_store);

static ssize_t ads7843_flip_store(struct device *dev, struct device_attribute *attr,
			                       const char *buf, size_t count)
{
	struct ads7843* driver_data;
	driver_data = (struct ads7843*)dev_get_drvdata(dev);

	sscanf(buf,"%u %u",&driver_data->flip_x, &driver_data->flip_y);

	return count;
}

static ssize_t ads7843_flip_show(struct device *dev,
            struct device_attribute *attr, char *buf)
{
	struct ads7843* driver_data;
	driver_data = (struct ads7843*)dev_get_drvdata(dev);

	return sprintf(buf, "%u %u\n", driver_data->flip_x, driver_data->flip_y);
}
static DEVICE_ATTR(flip, S_IRUGO | S_IWUSR, ads7843_flip_show, ads7843_flip_store);

static ssize_t ads7843_swap_store(struct device *dev, struct device_attribute *attr,
			                       const char *buf, size_t count)
{
	struct ads7843* driver_data;
	driver_data = (struct ads7843*)dev_get_drvdata(dev);

	sscanf(buf,"%u",&driver_data->swap_xy);

	return count;
}

static ssize_t ads7843_swap_show(struct device *dev,
            struct device_attribute *attr, char *buf)
{
	struct ads7843* driver_data;
	driver_data = (struct ads7843*)dev_get_drvdata(dev);

	return sprintf(buf, "%u\n", driver_data->swap_xy);
}
static DEVICE_ATTR(swap, S_IRUGO | S_IWUSR, ads7843_swap_show, ads7843_swap_store);


static struct attribute *ads7843_attributes[] = {
		&dev_attr_pen_down.attr,
		&dev_attr_raw_pos.attr,
		&dev_attr_swap.attr,
		&dev_attr_flip.attr,
		&dev_attr_max.attr,
		NULL,
};

static struct attribute_group ads7843_attr_group = {
		.attrs = ads7843_attributes,
};

/**
 * This method is invoked via a timer when the pen is pushed
 * and held down.  It is initialially invoked by the ISR
 * when the pen is pushed down.  It then reschedules itself
 * until the pen is lifted off of the touchscreen.  This routine
 * is what actually generates the input information for processing
 * touch and stroke events.
 *
 * \param[in] arg pointer to the ads7843 device associated with event
 */
static void ads7843_timer(unsigned long arg)
{
	tuPositionReg lsPos;
	unsigned int x,y;
	unsigned int     *lpBaseReg = NULL;
	struct fpga_device* fpgadev = (struct fpga_device*)arg;
	struct ads7843* driver_data = (struct ads7843*)dev_get_drvdata(&fpgadev->dev);

	lpBaseReg = (unsigned int*)fpgadev->baseaddr;

	/* if the pen is down */
	if (ads7843_is_pendown(&fpgadev->dev))
	{
		if (driver_data->lastEvent != eePenDown)
		{
			input_report_key(driver_data->input, BTN_TOUCH, 1);
		}
		ads7843_get_pos(&fpgadev->dev, &x, &y);
		input_report_abs(driver_data->input, ABS_X, x);
		input_report_abs(driver_data->input, ABS_Y, y);
		input_report_abs(driver_data->input, ABS_PRESSURE, 255);
		input_sync(driver_data->input);
		dev_dbg(&fpgadev->dev, "%d/%d/%d/%d\n", x, y, driver_data->lastEvent, 1);
		driver_data->lastEvent = eePenDown;
	}
	/* else pen is not down */
	else
	{
		if (driver_data->lastEvent == eePenDown)
		{
			input_report_key(driver_data->input, BTN_TOUCH, 0);
			ads7843_get_pos(&fpgadev->dev, &x, &y);
			input_report_abs(driver_data->input, ABS_X, x);
			input_report_abs(driver_data->input, ABS_Y, y);
			input_report_abs(driver_data->input, ABS_PRESSURE, 0);
			input_sync(driver_data->input);
			dev_dbg(&fpgadev->dev, "%d/%d/%d/%d\n", x, y, driver_data->lastEvent, 0);
		}
		driver_data->lastEvent = eeNoEvent;
	}

	/* if the pen is now up, then re-enable interrupts for the next pen down state */
	if (driver_data->lastEvent == eeNoEvent)
	{
		lsPos.mnLword = lpBaseReg[gnPOSITION_OFFSET];
		lsPos.msBits.mbIntEnable = 1;
		lsPos.msBits.mbIntAck = 0;
		lpBaseReg[gnPOSITION_OFFSET] = lsPos.mnLword;
	}
	/* else restart the timer */
	else
	{
		mod_timer(&driver_data->timer, jiffies+TS_POLL_TIME);
	}
}

/**
 * IRQ handler called when an ADS7843 core is asserting an interrupt
 * condition.  This method is called within the context of an ISR, and
 * must be atomic.
 *
 * \param[in] dev the device issuing the interrupt.
 * \return 0
 */
static int ads7843_IrqHandler(struct fpga_device* dev)
{
	tuPositionReg  lsPos;
	struct ads7843 *driver_data;
	unsigned int   *lpBaseReg;

	driver_data = dev_get_drvdata(&dev->dev);
	lpBaseReg = (unsigned int*)dev->baseaddr;

	/** enable the device and the interrupts */
	lsPos.mnLword = lpBaseReg[gnPOSITION_OFFSET];

	/* if a touch interrupt (pen-down) */
	if (lsPos.msBits.mbPenDown && lsPos.msBits.mbIntEnable)
	{
		mod_timer(&driver_data->timer, jiffies+TS_POLL_TIME);
		lsPos.msBits.mbIntEnable = 0;
	}

	/* if command is done */
	if (lsPos.msBits.mbCMDDone && lsPos.msBits.mbDispIrqEn)
	{
		lsPos.msBits.mbDispIrqEn = 0;
	}

	lsPos.msBits.mbIntAck = 1;
	lpBaseReg[gnPOSITION_OFFSET] = lsPos.mnLword;

	return 0;
}

static int ads7843_probe(struct device *dev);
static int ads7843_remove(struct device *dev);

/**
 * The driver object.  The information here must be common for
 * all of the potential drivers in the system.
 */
static struct fpga_driver ads7843_driver = {
	.id		= CORE_ID_ADS7843, /** must match value in core_ids.h */
	.version	= "ADS7843 Driver 1.0",
	.IrqHandler	= ads7843_IrqHandler,
	.probe		= ads7843_probe,
	.remove		= ads7843_remove,
	.driver		= {
		.name 	= "ads7843",
		.owner 	= THIS_MODULE,
	},
};

/**
 * The ads7843_probe routine is called after the ads7843 driver is successfully
 * matched to an FPGA core with the same core ID.
 *
 * \param[in] dev device within an fpga_device structure.
 * return 0 on successful probe / initialization.
 */
static int ads7843_probe(struct device *dev)
{
	int rv = 0;
	struct ads7843   *driver_data = NULL;
	struct input_dev *input_dev = NULL; /* input device interface*/
	unsigned int     *lpBaseReg = NULL;

	struct fpga_device* fpgadev = to_fpga_device(dev);

	lpBaseReg = (unsigned int*)fpgadev->baseaddr;

	dev_dbg(dev, "ads7843_probe() entered\n");

	/* create an input device - plugs into standard linux input framework */
	input_dev = input_allocate_device();
	driver_data = kzalloc(sizeof(struct ads7843), GFP_KERNEL);
	if (!input_dev || !driver_data)
	{
		rv = -ENOMEM;
		goto probe_free_mem;
	}

	snprintf(driver_data->phys, sizeof(driver_data->phys),"%s/input0", dev_name(dev));  /* TODO */
	snprintf(driver_data->name, sizeof(driver_data->name),"ADS7843 Touchscreen");
	driver_data->input = input_dev;
	input_dev->name = driver_data->name;
	input_dev->phys = driver_data->phys;
	input_dev->dev.parent = dev;
	input_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS);
	input_dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	driver_data->max_x = 4095;
	driver_data->max_y = 4095;

	input_set_abs_params(input_dev, ABS_X, 0, 4095, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, 4095, 0, 0);
	input_set_abs_params(input_dev, ABS_PRESSURE, 0, 255, 0, 0);

	rv = input_register_device(input_dev);
	if (rv)
	{
		dev_err(dev, "ads7843_probe() failed to register input device - %d\n", rv);
		goto probe_free_mem;
	}

	rv = sysfs_create_group(&dev->kobj, &ads7843_attr_group);
	if (rv)
	{
		dev_err(dev, "ads7843_probe() failed to add attributes group - %d\n", rv);
		goto probe_remove_input_dev;
	}

	dev_set_drvdata(dev, driver_data);

	init_timer(&driver_data->timer);
	driver_data->timer.function = ads7843_timer;
	driver_data->timer.data = (unsigned long)fpgadev;

	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
			       fpgadev->coreversion.ver0.bits.vector,
			       1);

	ads7843_timer((unsigned long)fpgadev);

	return rv;

probe_remove_input_dev:
	input_unregister_device(input_dev);

probe_free_mem:
	kfree(input_dev);
	kfree(driver_data);

	return rv;
}

/**
 * This routine is called when a device is removed from the FPGA bus.
 *
 * \param[in] dev pointer to the device being removed.
 */
static int ads7843_remove(struct device *dev)
{
	int rv = 0;
	struct ads7843 *driver_data;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	driver_data = (struct ads7843*)dev_get_drvdata(dev);

	dev_dbg(dev, "ads7843_remove entered\n");

	del_timer(&driver_data->timer);

	input_unregister_device(driver_data->input);

	sysfs_remove_group(&dev->kobj, &ads7843_attr_group);

	/* make sure IRQ is disabled */
	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
				   fpgadev->coreversion.ver0.bits.vector,
				   0);

	kfree(driver_data);

	dev_dbg(dev, "ads7843_removed completed\n");
	return rv;
}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init ads7843_init(void)
{
	int ret;

	ret = register_fpga_driver(&ads7843_driver);
	if (ret)
	{
		printk(KERN_ALERT "Unable to register ads7843_driver\n");
		goto exit_bail;
	}

	return 0;

exit_bail: /* Uh-Oh */
	return -1;
}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void ads7843_exit(void)
{
	driver_unregister(&ads7843_driver.driver);
}

module_init(ads7843_init);
module_exit(ads7843_exit);
