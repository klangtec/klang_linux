/*
 * Critical Link MityOMAP-L138 SoM Baseboard initializtaion file
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/mtd/mtd.h>
#include <linux/usb/musb.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/setup.h>
#include <mach/mux.h>
#include <mach/da8xx.h>
#include <linux/can/platform/mcp251x.h>

#define BASEBOARD_NAME "CIS10XX"

#define MMCSD_CD_PIN		GPIO_TO_PIN(4, 0)
#define MMCSD_WP_PIN		GPIO_TO_PIN(4, 1)

static int baseboard_mmc_get_ro(int index)
{
	return gpio_get_value(MMCSD_WP_PIN);
}

static int baseboard_mmc_get_cd(int index)
{
	return !gpio_get_value(MMCSD_CD_PIN);
}

static struct davinci_mmc_config da850_mmc_config = {
	.get_ro		= baseboard_mmc_get_ro,
	.get_cd		= baseboard_mmc_get_cd,
	.wires		= 4,
	.max_freq	= 50000000,
	.caps		= MMC_CAP_MMC_HIGHSPEED | MMC_CAP_SD_HIGHSPEED,
	.version	= MMC_CTLR_VERSION_2,
};

static const short da850_mmcsd0_pins[] __initconst = {
	DA850_MMCSD0_DAT_0, DA850_MMCSD0_DAT_1, DA850_MMCSD0_DAT_2,
	DA850_MMCSD0_DAT_3, DA850_MMCSD0_CLK, DA850_MMCSD0_CMD,
	DA850_GPIO4_0, DA850_GPIO4_1,
	-1
};

static __init void baseboard_setup_mmc(void)
{
	int ret;

	ret = davinci_cfg_reg_list(da850_mmcsd0_pins);
	if (ret)
		pr_warning("%s: mmcsd0 mux setup failed: %d\n", __func__, ret);

	ret = gpio_request(MMCSD_CD_PIN, "MMC CD\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_CD_PIN);
	gpio_direction_input(MMCSD_CD_PIN);

	ret = gpio_request(MMCSD_WP_PIN, "MMC WP\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_WP_PIN);
	gpio_direction_input(MMCSD_WP_PIN);

	ret = da8xx_register_mmcsd0(&da850_mmc_config);
	if (ret)
		pr_warning("%s: mmcsd0 registration failed: %d\n", __func__,
				ret);
}

/*
 * GPIO pins:
 * 	GP0[10] = TDO_3V3
 * 	GP0[11] = TRSTB_3V3
 * 	GP0[15] = TDI_3V3
 * 	GP0[6] = TCK_3V3
 *	GP0[14] = TMS_3V3
 * 	GP0[12] = RESET_3V3
 *	GP0[5] = POWER_DOWN_3V3
 *	GP0[13] = DVDD_EN
 *	GP0[1] = AVDD_EN
 *	GP2[15] = FAN_ON
 */
static short baseboard_gpio_pins[] __initdata = {
	DA850_GPIO0_1, DA850_GPIO0_5, DA850_GPIO0_6, 
	DA850_GPIO0_8, DA850_GPIO0_10, DA850_GPIO0_11,
	DA850_GPIO0_12, DA850_GPIO0_13, DA850_GPIO0_14, DA850_GPIO0_15,
	DA850_GPIO2_15, -1,
};

/**
 * UART2 Pins. CTS and RTS are ignored as they are floating in hw.
 */
static short baseboard_uart2_pins[] __initdata = {
	/*DA850_NUART2_CTS,*/
	/*DA850_NUART2_RTS,*/
	DA850_UART2_RXD,
	DA850_UART2_TXD,
	-1,
};

/**
 * I2C0 pins. 
 */
static short baseboard_i2c0_pins[] __initdata = {
	DA850_I2C0_SDA, 
	DA850_I2C0_SCL,
	-1
};

static int __init baseboard_init(void)
{
	int ret = 0;

	pr_info("%s [%s]...\n", __func__, BASEBOARD_NAME);

	ret = davinci_cfg_reg_list(baseboard_gpio_pins);
	if (ret)
		pr_warning("%s: GPIO mux setup failed: %d\n", __func__, ret);

	baseboard_setup_mmc();

	mityomapl138_usb_init(MUSB_PERIPHERAL);

	/* Setup pin muxing for UART2 */
	ret = davinci_cfg_reg_list(baseboard_uart2_pins);
	if (ret)
		pr_warning("%s: UART2 mux setup failed: %d\n", __func__, ret);

	/* Setup pin muxing for I2C0 */
	ret = davinci_cfg_reg_list(baseboard_i2c0_pins);
	if (ret)
		pr_warning("%s: I2C0 mux setup failed: %d\n", __func__, ret);

	return 0;
}
arch_initcall_sync(baseboard_init);

