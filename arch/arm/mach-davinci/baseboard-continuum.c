/*
 * Critical Link MityOMAP-L138 SoM Baseboard initializtaion file
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/usb/musb.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/setup.h>
#include <mach/mux.h>
#include <mach/da8xx.h>

#define BASEBOARD_NAME "Continuum HSDAQ"

#define MMCSD_CD_PIN		GPIO_TO_PIN(4, 0)
#define MMCSD_WP_PIN		GPIO_TO_PIN(4, 1)

#define DAC8718_CS_N		GPIO_TO_PIN(0, 13)

static int baseboard_mmc_get_ro(int index)
{
	return gpio_get_value(MMCSD_WP_PIN);
}

static int baseboard_mmc_get_cd(int index)
{
	return !gpio_get_value(MMCSD_CD_PIN);
}

static struct davinci_mmc_config da850_mmc_config = {
	.get_ro		= baseboard_mmc_get_ro,
	.get_cd		= baseboard_mmc_get_cd,
	.wires		= 4,
	.max_freq	= 50000000,
	.caps		= MMC_CAP_MMC_HIGHSPEED | MMC_CAP_SD_HIGHSPEED,
	.version	= MMC_CTLR_VERSION_2,
};

static const short da850_mmcsd0_pins[] __initconst = {
	DA850_MMCSD0_DAT_0, DA850_MMCSD0_DAT_1, DA850_MMCSD0_DAT_2,
	DA850_MMCSD0_DAT_3, DA850_MMCSD0_CLK, DA850_MMCSD0_CMD,
	DA850_GPIO4_0, DA850_GPIO4_1,
	-1
};

static __init void baseboard_setup_mmc(void)
{
	int ret;

	ret = davinci_cfg_reg_list(da850_mmcsd0_pins);
	if (ret)
		pr_warning("%s: mmcsd0 mux setup failed: %d\n", __func__, ret);

	ret = gpio_request(MMCSD_CD_PIN, "MMC CD\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_CD_PIN);
	gpio_direction_input(MMCSD_CD_PIN);

	ret = gpio_request(MMCSD_WP_PIN, "MMC WP\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_WP_PIN);
	gpio_direction_input(MMCSD_WP_PIN);

	ret = da8xx_register_mmcsd0(&da850_mmc_config);
	if (ret)
		pr_warning("%s: mmcsd0 registration failed: %d\n", __func__,
				ret);
}

/*
 * GPIO pins, this is an exhaustive list which may be overridden by
 * other devices
 */
static short baseboard_gpio_pins[] __initdata = {
	DA850_GPIO0_0, DA850_GPIO0_1, DA850_GPIO0_2, DA850_GPIO0_3,
	DA850_GPIO0_4, DA850_GPIO0_5, DA850_GPIO0_6, DA850_GPIO0_7,
	DA850_GPIO0_8, DA850_GPIO0_9, DA850_GPIO0_10, DA850_GPIO0_11,
	DA850_GPIO0_12, DA850_GPIO0_13, DA850_GPIO0_14, DA850_GPIO0_15,
	DA850_GPIO2_12, DA850_GPIO2_15, -1,
};

static struct spi_board_info baseboard_spi1_info[] = {
	[0] = {		/* Onboard DAC8718 device */
		.modalias		= "spidev",
		.max_speed_hz		= 10000000,
		.bus_num		= 1,
		.chip_select		= 1,
	},
};

static u8 spi1_cs[] = {
	SPI_INTERN_CS,
	DAC8718_CS_N,
};

static void __init baseboard_setup_spi(void)
{
	int ret;

	ret = gpio_request(DAC8718_CS_N, "DAC8718 CS\n");
	if (ret)
		pr_warning("%s: can not open DAC8718 CS %d\n", __func__,
				DAC8718_CS_N);

	ret = spi_register_board_info(baseboard_spi1_info,
					ARRAY_SIZE(baseboard_spi1_info));
	if (ret)
		pr_warning("%s: Unable to register SPI1 Info: %d\n", __func__,
				ret);
}

static int __init baseboard_pre_init(void)
{
	pr_info("%s: Entered\n", __func__);
	da8xx_spi_pdata[1].chip_sel = spi1_cs;
	da8xx_spi_pdata[1].num_chipselect = ARRAY_SIZE(spi1_cs);

	davinci_soc_info.emac_pdata->phy_id = NULL;
	return 0;
}
postcore_initcall_sync(baseboard_pre_init);

static int __init baseboard_init(void)
{
	pr_info("%s [%s]...\n", __func__, BASEBOARD_NAME);

	davinci_cfg_reg_list(baseboard_gpio_pins);

	baseboard_setup_mmc();

	baseboard_setup_spi();

	mityomapl138_usb_init(MUSB_PERIPHERAL);

	return 0;
}
arch_initcall_sync(baseboard_init);

