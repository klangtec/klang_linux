/*
 * Critical Link MityOMAP-L138 SoM Baseboard initializtaion file
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/usb/musb.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/setup.h>
#include <mach/mux.h>
#include <mach/da8xx.h>

#define BASEBOARD_NAME "USM"

#define MMCSD_CD_PIN		GPIO_TO_PIN(4, 0)
#define WIFI_SD_SEL_PIN		GPIO_TO_PIN(4, 1)

static int baseboard_mmc_get_ro(int index)
{
	return 0;
}

static int baseboard_mmc_get_cd(int index)
{
	return !gpio_get_value(MMCSD_CD_PIN);
}

static struct davinci_mmc_config da850_mmc_config = {
	.get_ro		= baseboard_mmc_get_ro,
	.get_cd		= baseboard_mmc_get_cd,
	.wires		= 4,
	.max_freq	= 50000000,
	.caps		= MMC_CAP_MMC_HIGHSPEED | MMC_CAP_SD_HIGHSPEED,
	.version	= MMC_CTLR_VERSION_2,
};

static const short da850_mmcsd0_pins[] __initconst = {
	DA850_MMCSD0_DAT_0, DA850_MMCSD0_DAT_1, DA850_MMCSD0_DAT_2,
	DA850_MMCSD0_DAT_3, DA850_MMCSD0_CLK, DA850_MMCSD0_CMD,
	DA850_GPIO4_0, DA850_GPIO4_1,
	-1
};

static __init void baseboard_setup_mmc(void)
{
	int ret;

	ret = davinci_cfg_reg_list(da850_mmcsd0_pins);
	if (ret)
		pr_warning("%s: mmcsd0 mux setup failed: %d\n", __func__, ret);

	ret = gpio_request(MMCSD_CD_PIN, "MMC CD\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_CD_PIN);
	gpio_direction_input(MMCSD_CD_PIN);

	ret = gpio_request(WIFI_SD_SEL_PIN, "MMC WP\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				WIFI_SD_SEL_PIN);
	gpio_direction_output(WIFI_SD_SEL_PIN, 1);

	ret = da8xx_register_mmcsd0(&da850_mmc_config);
	if (ret)
		pr_warning("%s: mmcsd0 registration failed: %d\n", __func__,
				ret);
}

/*
 * GPIO pins, this is an exhaustive list which may be overridden by
 * other devices
 */
static short baseboard_gpio_pins[] __initdata = {
	DA850_GPIO0_0, DA850_GPIO0_1, DA850_GPIO0_2, DA850_GPIO0_3,
	DA850_GPIO0_4, DA850_GPIO0_5, DA850_GPIO0_6, DA850_GPIO0_7,
	DA850_GPIO0_8, DA850_GPIO0_9, DA850_GPIO0_10, DA850_GPIO0_11,
	DA850_GPIO0_12, DA850_GPIO0_13, DA850_GPIO0_14, DA850_GPIO0_15,
	-1,
};

/**
 * UART2 Pins. CTS ignored as it's floating on hw.
 */
static short baseboard_uart2_pins[] __initdata = {
/*	DA850_NUART2_CTS,
	DA850_NUART2_RTS, */
	DA850_UART2_RXD,
	DA850_UART2_TXD,
	-1,
};

/**
 * Info for SPI devices on various FPGA chip selects.
 */
static struct spi_board_info spifpga_info[] = {
	/* AD5421 */
	[0] = {
		.modalias		= "spidev",
		.max_speed_hz		= 1000000,
		.bus_num		= 3,
		.chip_select		= 0,
		.mode			= SPI_MODE_1,
	},
	/* LMP90100 */
	[1] = {
		.modalias		= "spidev",
		.max_speed_hz		= 10000000,
		.bus_num		= 3,
		.chip_select		= 1,
	},
	/* Ethernet switch */
	[2] = {
		.modalias		= "spidev",
		.max_speed_hz		= 10000000,
		.bus_num		= 3,
		.chip_select		= 2,
	},
	/* Temp Sensor */
	[3] = {
		.modalias		= "spidev",
		.max_speed_hz		= 10000000,
		.bus_num		= 3,
		.chip_select		= 3,
	},
};

struct gpio_table {
	int		pin;
	const char	*name;
};

static struct gpio_table gpio_out_table[] = {
	{ GPIO_TO_PIN(0, 13)	, "red" },
	{ GPIO_TO_PIN(0, 5)	, "yellow" },
	{ GPIO_TO_PIN(0, 12)	, "green" },
	{ GPIO_TO_PIN(0, 14)	, "blue" },
};

static int mode_pins[] = {
	GPIO_TO_PIN(0, 7),
	GPIO_TO_PIN(0, 10),
	GPIO_TO_PIN(0, 11),
	GPIO_TO_PIN(0, 15),
};

static int assy_pins[] = {
	GPIO_TO_PIN(0, 1),
	GPIO_TO_PIN(0, 4),
	GPIO_TO_PIN(0, 3),
};

static int welmec_pins[] = {
	GPIO_TO_PIN(0, 6),
};

/*
 * Reads back the gpio pins for the logic blocks specified by USM.
 */
static ssize_t input_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	int  val, len, invert, rv, i;
	int *table = NULL;

	val = len = invert = 0;

	if (!strcmp(attr->attr.name, "mode")) {
		table = mode_pins;
		len = ARRAY_SIZE(mode_pins);
		invert = 1;
	} else if (!strcmp(attr->attr.name, "assy")) {
		table = assy_pins;
		len = ARRAY_SIZE(assy_pins);
	} else if (!strcmp(attr->attr.name, "welmec")) {
		table = welmec_pins;
		len = ARRAY_SIZE(welmec_pins);
		invert = 1;
	}

	for (i = 0; i < len; i++) {
		rv = gpio_get_value(table[i]) ? 1 : 0;
		rv ^= invert;
		if (rv)
			val |= (1 << i);
	}

	if (!len)
		val = -1;

	return sprintf(buf, "%d\n", val);
}

/*
 * Sets the gpio outputs (in this case, the LEDs).
 */
static ssize_t output_store(struct kobject *kobj, struct kobj_attribute *attr,
			 const char *buf, size_t count)
{
	int  val, rv, i, cur;
	rv = sscanf(buf, "%d", &val);

	/* check we got valid results */
	if ((val < 0) || (val > 2))
		return count;

	for (i = 0; i < ARRAY_SIZE(gpio_out_table); i++) {
		if (!strcmp(gpio_out_table[i].name, attr->attr.name)) {
			if (val <= 1)
				gpio_set_value(gpio_out_table[i].pin, val);
			else {
				cur = gpio_get_value(gpio_out_table[i].pin);
				gpio_set_value(gpio_out_table[i].pin,
						cur ? 0 : 1);
			}
			break;
		}
	}

	return count;
}

static struct kobj_attribute kobj_attrs[] = {
	__ATTR(green, 0666, NULL, output_store),
	__ATTR(yellow, 0666, NULL, output_store),
	__ATTR(red, 0666, NULL, output_store),
	__ATTR(blue, 0666, NULL, output_store),
	__ATTR(welmec, 0444, input_show, NULL),
	__ATTR(mode, 0444, input_show, NULL),
	__ATTR(assy, 0444, input_show, NULL),
};

static struct attribute *attrs[] = {
	&kobj_attrs[0].attr,
	&kobj_attrs[1].attr,
	&kobj_attrs[2].attr,
	&kobj_attrs[3].attr,
	&kobj_attrs[4].attr,
	&kobj_attrs[5].attr,
	&kobj_attrs[6].attr,
	NULL
};

static struct attribute_group attr_group = {
	.attrs = attrs,
};

static struct kobject *gpio_kobj;

/*
 * Initializes the kobject used to provide a user-mode file
 * API for changing GPIO configuration.
 */
static int __init baseboard_sysfs_init(void)
{
	int retval, i;
	void __iomem *pupdena, *pupdsel;

	/* Configure pull up control */
	pupdena = IO_ADDRESS(0x01E2C00C);
	__raw_writel((__raw_readl(pupdena) | 0x3), pupdena);
	pupdsel = IO_ADDRESS(0x01E2C010);
	__raw_writel((__raw_readl(pupdsel) | 0x3), pupdsel);

	for (i = 0; i < ARRAY_SIZE(gpio_out_table); i++) {
		retval = gpio_request(gpio_out_table[i].pin,
					gpio_out_table[i].name);
		if (retval)
			pr_warning("%s: can not open GPIO %d\n", __func__,
					gpio_out_table[i].pin);
		gpio_direction_output(gpio_out_table[i].pin, 0);
	}

	for (i = 0; i < ARRAY_SIZE(mode_pins); i++) {
		retval = gpio_request(mode_pins[i], "mode");
		if (retval)
			pr_warning("%s: can not open GPIO %d\n", __func__,
					mode_pins[i]);
		gpio_direction_input(mode_pins[i]);
	}

	for (i = 0; i < ARRAY_SIZE(assy_pins); i++) {
		retval = gpio_request(assy_pins[i], "assy");
		if (retval)
			pr_warning("%s: can not open GPIO %d\n", __func__,
					assy_pins[i]);
		gpio_direction_input(assy_pins[i]);
	}

	for (i = 0; i < ARRAY_SIZE(welmec_pins); i++) {
		retval = gpio_request(welmec_pins[i], "assy");
		if (retval)
			pr_warning("%s: can not open GPIO %d\n", __func__,
					welmec_pins[i]);
		gpio_direction_input(welmec_pins[i]);
	}

	gpio_kobj = kobject_create_and_add("usm_io", kernel_kobj);
	if (!gpio_kobj)
		return -ENOMEM;

	retval = sysfs_create_group(gpio_kobj, &attr_group);
	if (retval)
		kobject_put(gpio_kobj);

	return retval;
}
module_init(baseboard_sysfs_init);

static void __exit baseboard_sysfs_exit(void)
{
	kobject_put(gpio_kobj);
}
module_exit(baseboard_sysfs_exit);

static int __init baseboard_pre_init(void)
{
	pr_info("%s: Entered\n", __func__);
	davinci_soc_info.emac_pdata->phy_id = "";
	return 0;
}
postcore_initcall_sync(baseboard_pre_init);

static int __init baseboard_init(void)
{
	int ret = 0;

	pr_info("%s [%s]...\n", __func__, BASEBOARD_NAME);

	/* Setup pin muxing for GPIOs */
	ret = davinci_cfg_reg_list(baseboard_gpio_pins);

	baseboard_setup_mmc();

	/* Register the FPGA spi devices for each chip select*/
	ret = spi_register_board_info(spifpga_info, ARRAY_SIZE(spifpga_info));
	if (ret)
		pr_warning("%s: Unable to register SPI1 Info: %d", __func__,
				ret);

	/* Setup pin muxing for UART2 (including RTS pin)*/
	ret = davinci_cfg_reg_list(baseboard_uart2_pins);

	mityomapl138_usb_init(MUSB_HOST);

	return 0;
}
arch_initcall_sync(baseboard_init);

