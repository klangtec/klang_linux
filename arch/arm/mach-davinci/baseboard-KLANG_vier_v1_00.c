/*
 * Critical Link MityOMAP-L138 SoM Baseboard initializtaion file
 *
 * 2016/07/18 KLANG:technologies GmbH [info@klang.com] - Created new baseboard file for KLANG:vier based on MityOMAP-file
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/mtd/mtd.h>
#include <linux/usb/musb.h>
#include <linux/i2c.h>
#include <linux/i2c/at24.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/setup.h>
#include <mach/mux.h>
#include <mach/da8xx.h>
#include <linux/can/platform/mcp251x.h>

//#include <linux/input/ft5x06.h>


//#include <linux/input/ft5x06_ts.h>
#define BASEBOARD_NAME "KLANG:technologies KLANG:vier - v1.00"

#define MSTPRI2_LCD_MASK  0x70000000
#define MSTPRI2_LCD_SHIFT 28
#define LCD_PRIORITY	  0 /* make video output highest priority */

#define MMCSD_CD_PIN		GPIO_TO_PIN(4, 0)
#define MMCSD_WP_PIN		GPIO_TO_PIN(4, 1)
#define ETHERNET_PHY_RESET	GPIO_TO_PIN(2, 15)


//#define FT5x06_I2C_SLAVEADDRESS (0x70 >> 1)
//#define OMAP_FT5x06_GPIO GPIO_TO_PIN(8, 12)
//#define OMAP_FT5x06_RESET_GPIO GPIO_TO_PIN(8, 15)


static struct davinci_i2c_platform_data mityomap_i2c_1_pdata = {
	.bus_freq	= 100,	/* kHz */
	.bus_delay	= 0,	/* usec */
};
/*
int ft5x06_dev_init(int resource)
{	
        if (resource){
		printk(KERN_INFO "Entering Pinconfig-function (ft5x06_dev_init)\n\n\n");
		davinci_cfg_reg(DA850_GPIO8_12);
		davinci_cfg_reg(DA850_GPIO8_15);
                //omap_mux_init_signal("cam_d0.gpio_99", OMAP_PIN_INPUT | OMAP_PIN_OFF_WAKEUPENABLE);
                //omap_mux_init_signal("gpmc_d10.gpio_46", OMAP_PIN_OUTPUT );
 
//                if (gpio_request(OMAP_FT5x06_RESET_GPIO, "ft5x06_reset") < 0){
//                        printk(KERN_ERR "can't get ft5x06 xreset GPIO\n");
//                        return -1;
//                }
 
 //               if (gpio_request(OMAP_FT5x06_GPIO, "ft5x06_touch") < 0) {
   //                     printk(KERN_ERR "can't get ft5x06 interrupt GPIO\n");
     //                   return -1;
       //         }
 
       //         gpio_direction_input(OMAP_FT5x06_GPIO);
	//	gpio_direction_output(OMAP_FT5x06_RESET_GPIO, 1);
//        } else {
//                gpio_free(OMAP_FT5x06_GPIO);
//                gpio_free(OMAP_FT5x06_RESET_GPIO);

//        }
 
        return 0;
}*/
/*
static struct ft5x06_platform_data ft5x06_platform_data = {
        .maxx = 800,
        .maxy = 480,
        .flags = 0,
        .reset_gpio = OMAP_FT5x06_RESET_GPIO,
        .use_st = FT_USE_ST,
        .use_mt = FT_USE_MT,
        .use_trk_id = 1, //FT_USE_TRACKING_ID,
        .use_sleep = FT_USE_SLEEP,
        .use_gestures = 1,
};*/


static struct i2c_board_info __initdata mityomap_I2C1_info[] = {
      
};


static int __init i2c1_init(void)
{
	return i2c_register_board_info(1, mityomap_I2C1_info,
					ARRAY_SIZE(mityomap_I2C1_info));

}

static int baseboard_mmc_get_ro(int index)
{
	return gpio_get_value(MMCSD_WP_PIN);
};

static int baseboard_mmc_get_cd(int index)
{
	return !gpio_get_value(MMCSD_CD_PIN);
};

static struct davinci_mmc_config da850_mmc_config = {
	.get_ro		= baseboard_mmc_get_ro,
	.get_cd		= baseboard_mmc_get_cd,
	.wires		= 4,
	.max_freq	= 50000000,
	.caps		= MMC_CAP_MMC_HIGHSPEED | MMC_CAP_SD_HIGHSPEED,
	.version	= MMC_CTLR_VERSION_2,
};

static const short da850_mmcsd0_pins[] __initconst = {
	DA850_MMCSD0_DAT_0, DA850_MMCSD0_DAT_1, DA850_MMCSD0_DAT_2,
	DA850_MMCSD0_DAT_3, DA850_MMCSD0_CLK, DA850_MMCSD0_CMD,
	DA850_GPIO4_0, DA850_GPIO4_1,
	-1
};

static const short da850_I2C1_pins[] __initconst = {
	DA850_I2C1_SCL,
	DA850_I2C1_SDA,
	-1
};


static __init void baseboard_setup_mmc(void)
{
	int ret;

	ret = davinci_cfg_reg_list(da850_mmcsd0_pins);
	if (ret)
		pr_warning("%s: mmcsd0 mux setup failed: %d\n", __func__, ret);

	ret = gpio_request(MMCSD_CD_PIN, "MMC CD\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_CD_PIN);
	gpio_direction_input(MMCSD_CD_PIN);

	ret = da8xx_register_mmcsd0(&da850_mmc_config);
	if (ret)
		pr_warning("%s: mmcsd0 registration failed: %d\n", __func__,
				ret);
}

/*
 * GPIO pins, this is an exhaustive list which may be overridden by
 * other devices
 */
static short baseboard_gpio_pins[] __initdata = {
	DA850_GPIO0_0, DA850_GPIO0_1, DA850_GPIO0_2, DA850_GPIO0_3,
	DA850_GPIO0_4, DA850_GPIO0_5, DA850_GPIO0_6, DA850_GPIO0_7,
	DA850_GPIO0_8, DA850_GPIO0_9, DA850_GPIO0_10, DA850_GPIO0_11,
	DA850_GPIO0_12, DA850_GPIO0_13, DA850_GPIO0_14, DA850_GPIO0_15,
	DA850_GPIO2_12, DA850_GPIO2_15, DA850_GPIO8_12, DA850_GPIO8_15, -1,
};


static const struct display_panel disp_panel = {
	QVGA,
	16,
	16,
	COLOR_ACTIVE,
};

static struct lcd_ctrl_config lcd_cfg = {
	&disp_panel,
	.ac_bias		= 255,
	.ac_bias_intrpt		= 0,
	.dma_burst_sz		= 16,
	.bpp			= 16,
	.fdd			= 255,
	.tft_alt_mode		= 0,
	.stn_565_mode		= 0,
	.mono_8bit_mode		= 0,
	.invert_line_clock	= 0,
	.invert_frm_clock	= 0,
	.sync_edge		= 0,
	.sync_ctrl		= 1,
	.raster_order		= 0,
};

static struct da8xx_lcdc_platform_data KLANG_Display_pdata = {
	.manu_name		= "KLANG",
	.controller_data	= &lcd_cfg,
	.type			= "KLANG_Display",
};


static __init void baseboard_setup_lcd(const char *panel)
{
	int ret;
	struct da8xx_lcdc_platform_data *pdata;

	u32 prio;

	/* set peripheral master priority up to 1 */
	prio = __raw_readl(DA8XX_SYSCFG0_VIRT(DA8XX_MSTPRI2_REG));
	prio &= ~MSTPRI2_LCD_MASK;
	prio |= LCD_PRIORITY<<MSTPRI2_LCD_SHIFT;
	__raw_writel(prio, DA8XX_SYSCFG0_VIRT(DA8XX_MSTPRI2_REG));

	if (!strcmp("KLANG_Display", panel))
		pdata = &KLANG_Display_pdata;
	else {
		pr_warning("%s: unknown LCD type : %s\n", __func__,
				panel);
		return;
	}

	ret = davinci_cfg_reg_list(da850_lcdcntl_pins);
	if (ret) {
		pr_warning("%s: lcd pinmux failed : %d\n", __func__,
				ret);
		return;
	}

	ret = da8xx_register_lcdc(pdata);
}

#ifdef CONFIG_MTD
#define OFFSET_LCDCONFIG 176
#define LCDCONFIG_LEN 33
static void baseboard_mtd_notify(struct mtd_info *mtd)
{
	int retlen;
	char buf[LCDCONFIG_LEN]; /* enable, manufacturer name */
	if (!strcmp(mtd->name, "periph-config")) {
		mtd_read(mtd, OFFSET_LCDCONFIG, LCDCONFIG_LEN, &retlen,
			  buf);
		if (retlen == LCDCONFIG_LEN) {
			if (buf[0]) {
				buf[LCDCONFIG_LEN-1] = 0;
				pr_info("Using LCD panel: %s\n", &buf[1]);
				baseboard_setup_lcd(&buf[1]);
			} else
				pr_info("No LCD configured\n");
		}
	}
}
static struct mtd_notifier baseboard_spi_notifier = {
	.add	= baseboard_mtd_notify,
};

static void baseboard_mtd_notify_add(void)
{
	register_mtd_user(&baseboard_spi_notifier);
}
#else
static void baseboard_mtd_notify_add(void) { }
#endif



static int __init baseboard_pre_init(void)
{
	pr_info("%s: Entered\n", __func__);
//	davinci_soc_info.emac_pdata->phy_id = "0:00";

	

	return 0;
}
postcore_initcall_sync(baseboard_pre_init);

#define LEERZEICHEN_18STK "                  "

static int __init baseboard_init(void)
{
	int ret;
	pr_info("\n\n\n\n********************************************************\n*%s%s%s*\n*%sKLANG:technologies%s*\n*%s%s%s*\n********************************************************\n\n\n\n", LEERZEICHEN_18STK, LEERZEICHEN_18STK, LEERZEICHEN_18STK, LEERZEICHEN_18STK, LEERZEICHEN_18STK, LEERZEICHEN_18STK, LEERZEICHEN_18STK, LEERZEICHEN_18STK);

	pr_info("%s [%s]...\n", __func__, BASEBOARD_NAME);

	davinci_cfg_reg_list(baseboard_gpio_pins);
// Added by KLANG:technologies:	
	ret = gpio_request(ETHERNET_PHY_RESET, "ETHERNET RESET");
	if (ret)
		pr_warning("%s: can't open GPIO %d\n", __func__, ETHERNET_PHY_RESET);
	else
		pr_info("Reset GPIO accessable.");
	gpio_direction_output(ETHERNET_PHY_RESET, 1);
	pr_info("Set Reset to output and value 1");

	ret = da8xx_register_i2c(1, &mityomap_i2c_1_pdata);
	if (ret)
		pr_warning("i2c1 registration failed: %d\n", ret);

	ret = i2c1_init();
	if (ret)
		pr_warning("I2C-1 init failed: %d\n", ret);
// END.



	baseboard_setup_mmc();

	davinci_cfg_reg_list(da850_I2C1_pins);

	baseboard_mtd_notify_add();

	mityomapl138_usb_init(MUSB_HOST);

	return 0;
}
arch_initcall_sync(baseboard_init);

