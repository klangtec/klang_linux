/*
 * Critical Link MityOMAP-L138 SoM Baseboard initializtaion file
 *
 *
 * 2014/07/16 KLANG:technologies GmbH [info@klang.com] - Fixed USB problem
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/mtd/mtd.h>
#include <linux/usb/musb.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/setup.h>
#include <mach/mux.h>
#include <mach/da8xx.h>
#include <linux/can/platform/mcp251x.h>

#define BASEBOARD_NAME "IndustrialIO"
#define MSTPRI2_LCD_MASK  0x70000000
#define MSTPRI2_LCD_SHIFT 28
#define LCD_PRIORITY	  0 /* make video output highest priority */

#define MMCSD_CD_PIN		GPIO_TO_PIN(4, 0)
#define MMCSD_WP_PIN		GPIO_TO_PIN(4, 1)
#define SYSTEM_RESET		GPIO_TO_PIN(5, 14)
#define CAN_CS_N		GPIO_TO_PIN(2, 15)
#define CAN_INT_PIN		GPIO_TO_PIN(2, 12)
#define DSD1791_CS_N		GPIO_TO_PIN(0, 8)

static int baseboard_mmc_get_ro(int index)
{
	return gpio_get_value(MMCSD_WP_PIN);
}

static int baseboard_mmc_get_cd(int index)
{
	return !gpio_get_value(MMCSD_CD_PIN);
}

static struct davinci_mmc_config da850_mmc_config = {
	.get_ro		= baseboard_mmc_get_ro,
	.get_cd		= baseboard_mmc_get_cd,
	.wires		= 4,
	.max_freq	= 50000000,
	.caps		= MMC_CAP_MMC_HIGHSPEED | MMC_CAP_SD_HIGHSPEED,
	.version	= MMC_CTLR_VERSION_2,
};

static const short da850_mmcsd0_pins[] __initconst = {
	DA850_MMCSD0_DAT_0, DA850_MMCSD0_DAT_1, DA850_MMCSD0_DAT_2,
	DA850_MMCSD0_DAT_3, DA850_MMCSD0_CLK, DA850_MMCSD0_CMD,
	DA850_GPIO4_0, DA850_GPIO4_1,
	-1
};

static __init void baseboard_setup_mmc(void)
{
	int ret;

	ret = davinci_cfg_reg_list(da850_mmcsd0_pins);
	if (ret)
		pr_warning("%s: mmcsd0 mux setup failed: %d\n", __func__, ret);

	ret = gpio_request(MMCSD_CD_PIN, "MMC CD\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_CD_PIN);
	gpio_direction_input(MMCSD_CD_PIN);

	ret = gpio_request(MMCSD_WP_PIN, "MMC WP\n");
	if (ret)
		pr_warning("%s: can not open GPIO %d\n", __func__,
				MMCSD_WP_PIN);
	gpio_direction_input(MMCSD_WP_PIN);

	ret = da8xx_register_mmcsd0(&da850_mmc_config);
	if (ret)
		pr_warning("%s: mmcsd0 registration failed: %d\n", __func__,
				ret);
}

/*
 * GPIO pins, this is an exhaustive list which may be overridden by
 * other devices
 */
static short baseboard_gpio_pins[] __initdata = {
	DA850_GPIO0_0, DA850_GPIO0_1, DA850_GPIO0_2, DA850_GPIO0_3,
	DA850_GPIO0_4, DA850_GPIO0_5, DA850_GPIO0_6, DA850_GPIO0_7,
	DA850_GPIO0_8, DA850_GPIO0_9, DA850_GPIO0_10, DA850_GPIO0_11,
	DA850_GPIO0_12, DA850_GPIO0_13, DA850_GPIO0_14, DA850_GPIO0_15,
	DA850_GPIO2_12, DA850_GPIO2_15, DA850_GPIO5_14, -1,
};

/* davinci da850 evm audio machine driver */
static u8 da850_iis_serializer_direction[] = {
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
};

static struct snd_platform_data baseboard_snd_data = {
	.tx_dma_offset	= 0x2000,
	.rx_dma_offset	= 0x2000,
	.op_mode	= DAVINCI_MCASP_IIS_MODE,
	.num_serializer	= ARRAY_SIZE(da850_iis_serializer_direction),
	.tdm_slots	= 0,
	.serial_dir	= da850_iis_serializer_direction,
	.asp_chan_q	= EVENTQ_1,
	.version	= MCASP_VERSION_2,
	.txnumevt	= 0,
	.rxnumevt	= 0,
};

static short baseboard_mcasp_pins[25] __initdata = {
	DA850_AHCLKX, DA850_ACLKX, DA850_AFSX,
	DA850_AHCLKR, DA850_ACLKR, DA850_AFSR,
	DA850_AMUTE, DA850_GPIO5_14,
	-1, -1, -1, -1,
	-1, -1, -1, -1,
	-1, -1, -1, -1,
	-1, -1, -1, -1,
	-1
};

static __init int baseboard_setup_mcasp(void)
{
	int ret;

	baseboard_mcasp_pins[8+0] = DA850_AXR_15;
	da850_iis_serializer_direction[15] = TX_MODE;

	ret = davinci_cfg_reg_list(baseboard_mcasp_pins);
	if (ret)
		pr_warning("%s: mcasp mux setup failed: %d\n", __func__, ret);


	baseboard_snd_data.tdm_slots = 2;
	baseboard_snd_data.txnumevt = 1;

	da8xx_register_mcasp(0, &baseboard_snd_data);

	return ret;
}

static const struct display_panel disp_panel = {
	QVGA,
	16,
	16,
	COLOR_ACTIVE,
};

static struct lcd_ctrl_config lcd_cfg = {
	&disp_panel,
	.ac_bias		= 255,
	.ac_bias_intrpt		= 0,
	.dma_burst_sz		= 16,
	.bpp			= 16,
	.fdd			= 255,
	.tft_alt_mode		= 0,
	.stn_565_mode		= 0,
	.mono_8bit_mode		= 0,
	.invert_line_clock	= 0,
	.invert_frm_clock	= 0,
	.sync_edge		= 0,
	.sync_ctrl		= 1,
	.raster_order		= 0,
};

static struct da8xx_lcdc_platform_data sharp_lq035q7dh06_pdata = {
	.manu_name		= "sharp",
	.controller_data	= &lcd_cfg,
	.type			= "Sharp_LQ035Q7DH06",
};

static struct da8xx_lcdc_platform_data chimei_p0430wqlb_pdata = {
	.manu_name		= "ChiMei",
	.controller_data	= &lcd_cfg,
	.type			= "ChiMei_P0430WQLB",
};

static struct da8xx_lcdc_platform_data vga_640x480_pdata = {
	.manu_name		= "VGA",
	.controller_data	= &lcd_cfg,
	.type			= "vga_640x480",
};

static struct da8xx_lcdc_platform_data wvga_800x480_pdata = {
	.manu_name		= "WVGA",
	.controller_data	= &lcd_cfg,
	.type			= "wvga_800x480",
};

static struct da8xx_lcdc_platform_data svga_800x600_pdata = {
	.manu_name		= "SVGA",
	.controller_data	= &lcd_cfg,
	.type			= "svga_800x600",
};

static struct da8xx_lcdc_platform_data nec_nl4827hc19_pdata = {
	.manu_name		= "NEC",
	.controller_data	= &lcd_cfg,
	.type			= "NEC_NL4827HC19-05B",
};

static __init void baseboard_setup_lcd(const char *panel)
{
	int ret;
	struct da8xx_lcdc_platform_data *pdata;

	u32 prio;

	/* set peripheral master priority up to 1 */
	prio = __raw_readl(DA8XX_SYSCFG0_VIRT(DA8XX_MSTPRI2_REG));
	prio &= ~MSTPRI2_LCD_MASK;
	prio |= LCD_PRIORITY<<MSTPRI2_LCD_SHIFT;
	__raw_writel(prio, DA8XX_SYSCFG0_VIRT(DA8XX_MSTPRI2_REG));

	if (!strcmp("Sharp_LQ035Q7DH06", panel))
		pdata = &sharp_lq035q7dh06_pdata;
	else if (!strcmp("ChiMei_P0430WQLB", panel))
		pdata = &chimei_p0430wqlb_pdata;
	else if (!strcmp("vga_640x480", panel))
		pdata = &vga_640x480_pdata;
	else if (!strcmp("wvga_800x480", panel))
		pdata = &wvga_800x480_pdata;
	else if (!strcmp("svga_800x600", panel))
		pdata = &svga_800x600_pdata;
	else if (!strcmp("NEC_NL4827HC19-05B", panel)) {
		pdata = &nec_nl4827hc19_pdata;
		lcd_cfg.invert_line_clock	= 1;
		lcd_cfg.invert_frm_clock	= 1;
	} else {
		pr_warning("%s: unknown LCD type : %s\n", __func__,
				panel);
		return;
	}

	ret = davinci_cfg_reg_list(da850_lcdcntl_pins);
	if (ret) {
		pr_warning("%s: lcd pinmux failed : %d\n", __func__,
				ret);
		return;
	}

	ret = da8xx_register_lcdc(pdata);
}

#ifdef CONFIG_MTD
#define OFFSET_LCDCONFIG 0260
#define LCDCONFIG_LEN 33
static void __init baseboard_mtd_notify(struct mtd_info *mtd)
{
	int retlen;
	char buf[LCDCONFIG_LEN]; /* enable, manufacturer name */
	if (!strcmp(mtd->name, "periph-config")) {
		mtd_read(mtd, OFFSET_LCDCONFIG, LCDCONFIG_LEN, &retlen,
			  buf);
		if (retlen == LCDCONFIG_LEN) {
			if (buf[0]) {
				buf[LCDCONFIG_LEN-1] = 0;
				pr_info("Using LCD panel: %s\n", &buf[1]);
				baseboard_setup_lcd(&buf[1]);
			} else
				pr_info("No LCD configured\n");
		}
	}
}

static struct mtd_notifier __initdata baseboard_spi_notifier = {
	.add	= baseboard_mtd_notify,
};

static void __init baseboard_mtd_notify_add(void)
{
	register_mtd_user(&baseboard_spi_notifier);
}
#else
static void __init baseboard_mtd_notify_add(void) { }
#endif

static struct mcp251x_platform_data mcp2515_pdata = {
	.oscillator_frequency	= 20000000,
};

static struct davinci_spi_config spi_mcp2515_config = {
	.io_type	= SPI_IO_TYPE_DMA,
	.c2tdelay	= 0,
	.t2cdelay	= 0,
};

static struct davinci_spi_config spi_dsd1791_config = {
	.io_type	= SPI_IO_TYPE_DMA,
	.c2tdelay	= 0,
	.t2cdelay	= 0,
};

static struct spi_board_info baseboard_spi1_info[] = {
	[0] = {
		.modalias		= "mcp2515",
		.platform_data		= &mcp2515_pdata,
		.controller_data	= &spi_mcp2515_config,
		.max_speed_hz		= 10000000,
		.bus_num		= 1,
		.chip_select		= 1,
	},
	[1] = {
		.modalias		= "dsd1791",
		.controller_data	= &spi_dsd1791_config,
		.max_speed_hz		= 1000000,
		.bus_num		= 1,
		.chip_select		= 2,
	},
};

static u8 spi1_cs[] = {
	SPI_INTERN_CS,
	CAN_CS_N,
	DSD1791_CS_N,
};

static void __init baseboard_setup_spi(void)
{
	int ret;

	ret = gpio_request(CAN_CS_N, "CAN CS\n");
	if (ret)
		pr_warning("%s: can not open CAN CS %d\n", __func__, CAN_CS_N);

	ret = gpio_request(CAN_INT_PIN, "CAN IRQ\n");
	if (ret) {
		pr_warning("%s: can not open CAN IRQ %d\n", __func__,
			CAN_INT_PIN);
	} else {
		gpio_direction_input(CAN_INT_PIN);
		baseboard_spi1_info[0].irq = gpio_to_irq(CAN_INT_PIN);
	}

	ret = gpio_request(DSD1791_CS_N, "DSD1791 CS\n");
	if (ret)
		pr_warning("%s: can not open DSD1791 CS %d\n", __func__,
			DSD1791_CS_N);

	// KLANG
	//baseboard_spi1_info[0].irq = gpio_to_irq(CAN_INT_PIN);

	ret = spi_register_board_info(baseboard_spi1_info,
					ARRAY_SIZE(baseboard_spi1_info));
	if (ret)
		pr_warning("%s: Unable to register SPI1 Info: %d\n", __func__,
				ret);
}

static int __init baseboard_pre_init(void)
{
	pr_info("%s: Entered\n", __func__);
	da8xx_spi_pdata[1].chip_sel = spi1_cs;
	da8xx_spi_pdata[1].num_chipselect = ARRAY_SIZE(spi1_cs);
	davinci_soc_info.emac_pdata->phy_id = "0:03";
	return 0;
}
postcore_initcall_sync(baseboard_pre_init);

/**
 * UART2 Pins for expansion port (RS-485 port).
 */
static short baseboard_uart2_pins[] __initdata = {
	DA850_UART2_RXD,
	DA850_UART2_TXD,
	-1,
};

static int __init baseboard_init(void)
{
	int ret;
	pr_info("%s [%s]...\n", __func__, BASEBOARD_NAME);

	davinci_cfg_reg_list(baseboard_gpio_pins);

	davinci_cfg_reg_list(baseboard_uart2_pins);

	baseboard_setup_mmc();

	/* SYSTEM_RESET PIN connected to CAN controller and AUDIO controller */
	ret = gpio_request(SYSTEM_RESET, "SYSTEM RESET\n");
	if (ret)
		pr_warning("%s: can't open GPIO %d\n", __func__, SYSTEM_RESET);
	msleep(20);
	gpio_direction_output(SYSTEM_RESET, 0);
	msleep(20);
	gpio_set_value(SYSTEM_RESET, 1);

	baseboard_setup_spi();

	baseboard_setup_mcasp();

	baseboard_mtd_notify_add();

//	mityomapl138_usb_init(MUSB_OTG);
	mityomapl138_usb_init(MUSB_HOST);

	return 0;
}
arch_initcall_sync(baseboard_init);

