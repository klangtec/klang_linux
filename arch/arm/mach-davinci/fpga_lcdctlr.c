/*
 * fpga_lcdctlr.c
 *
 *  Created on: Sep 12, 2011
 *      Author: Mike Williamson
 */

#include "fpga.h"
#include "common/fpga/fpga_lcdctlr.h"
#include "common/fpga/core_ids.h"

#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/clk.h>
#include <linux/cpufreq.h>
#include <linux/err.h>
#include <asm/io.h>
#include <linux/device.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Williamson <michael.williamson@criticallink.com");
MODULE_DESCRIPTION("Driver for MityDSP-L138 FPGA Based LCD Controller Core");

struct fpga_lcdctlr {
	struct fpga_device*	fpgadev;
};

static void modify_csr(struct fpga_lcdctlr *fpga_lcdctlr, int mask, int value)
{
	unsigned short *lpBaseReg = (unsigned short*)fpga_lcdctlr->fpgadev->baseaddr;
	uint16_t csr;

	csr = ioread16(lpBaseReg+gnCSR_OFFSET);
	csr &= ~mask;
	csr |= value;
	iowrite16(csr,lpBaseReg+gnCSR_OFFSET);
}

static void set_duty(struct fpga_lcdctlr *fpga_lcdctlr, int percent)
{
	uint16_t pwm_clocks, pwm_on;
	unsigned int temp;
	unsigned short *lpBaseReg = (unsigned short*)fpga_lcdctlr->fpgadev->baseaddr;
	if (percent < 0)
		percent = 0;

	if (percent > 100)
		percent = 100;

	pwm_clocks = ioread16(lpBaseReg+gnPWM_CLKS_OFFSET);	
	temp = pwm_clocks;
	temp *= percent;
	temp /= 100;
	pwm_on = temp;
	iowrite16(pwm_on, lpBaseReg+gnPWM_ON_OFFSET);
}

static int get_duty(struct fpga_lcdctlr *fpga_lcdctlr)
{
	uint16_t pwm_clocks, pwm_on;
	int temp;
	unsigned short *lpBaseReg = (unsigned short*)fpga_lcdctlr->fpgadev->baseaddr;

	pwm_clocks = ioread16(lpBaseReg+gnPWM_CLKS_OFFSET);	
	pwm_on = ioread16(lpBaseReg+gnPWM_ON_OFFSET);	

	temp = pwm_on*100;
	temp /= pwm_clocks;

	return temp;
}

static ssize_t fpga_lcdctlr_duty_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_lcdctlr* fpga_lcdctlr = (struct fpga_lcdctlr*)dev_get_drvdata(dev);
	int duty = get_duty(fpga_lcdctlr);
	return sprintf(buf, "%d\n",duty);
}

static ssize_t fpga_lcdctlr_duty_set(struct device *dev, 
		struct device_attribute *attr, const char *buf, size_t count)
{
	int percent;
	struct fpga_lcdctlr* fpga_lcdctlr = (struct fpga_lcdctlr*)dev_get_drvdata(dev);

	sscanf(buf,"%d", &percent);

	set_duty(fpga_lcdctlr, percent);

	return count;
}
static DEVICE_ATTR(duty, S_IRUGO | S_IWUSR, fpga_lcdctlr_duty_show, fpga_lcdctlr_duty_set);

static void set_freq(struct fpga_lcdctlr *fpga_lcdctlr, int freq)
{
	uint16_t pwm_clocks;
	int temp, duty;
	struct clk *emif_clk;
	unsigned short *lpBaseReg = (unsigned short*)fpga_lcdctlr->fpgadev->baseaddr;

	duty = get_duty(fpga_lcdctlr);

	emif_clk = clk_get(NULL, "aemif");
	if (IS_ERR(emif_clk))
		/* This should never happen, but default to 100 MHz */
		temp = 100000000;
	else
		temp = clk_get_rate(emif_clk);

	temp /= freq;
	temp -= 1;
	if (temp < 10)
		temp = 10;
	if (temp > 0x0FFFF)
		temp = 0x0FFFF;
	pwm_clocks = temp;
	iowrite16(pwm_clocks, lpBaseReg+gnPWM_CLKS_OFFSET);

	set_duty(fpga_lcdctlr, duty);
}

static int get_freq(struct fpga_lcdctlr *fpga_lcdctlr)
{
	uint16_t pwm_clocks;
	int temp;
	struct clk *emif_clk;
	unsigned short *lpBaseReg = (unsigned short*)fpga_lcdctlr->fpgadev->baseaddr;

	emif_clk = clk_get(NULL, "aemif");
	if (IS_ERR(emif_clk))
		/* This should never happen, but default to 100 MHz */
		temp = 100000000;
	else
		temp = clk_get_rate(emif_clk);
	pwm_clocks = ioread16(lpBaseReg+gnPWM_CLKS_OFFSET);	
	temp /= (pwm_clocks+1);

	return temp;
}

static ssize_t fpga_lcdctlr_freq_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_lcdctlr* fpga_lcdctlr = (struct fpga_lcdctlr*)dev_get_drvdata(dev);
	int freq = get_freq(fpga_lcdctlr);
	return sprintf(buf, "%d\n",freq);
}

static ssize_t fpga_lcdctlr_freq_set(struct device *dev, 
		struct device_attribute *attr, const char *buf, size_t count)
{
	int freq;
	struct fpga_lcdctlr* fpga_lcdctlr = (struct fpga_lcdctlr*)dev_get_drvdata(dev);

	sscanf(buf,"%d", &freq);

	set_freq(fpga_lcdctlr, freq);

	return count;
}
static DEVICE_ATTR(freq, S_IRUGO | S_IWUSR, fpga_lcdctlr_freq_show, fpga_lcdctlr_freq_set);

static ssize_t fpga_lcdctlr_csrbit_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	int val, shift;
	struct fpga_lcdctlr* fpga_lcdctlr = (struct fpga_lcdctlr*)dev_get_drvdata(dev);
	unsigned short *lpBaseReg = (unsigned short*)fpga_lcdctlr->fpgadev->baseaddr;

	if (!strcmp(attr->attr.name,"enable"))
		shift = 0;
	else if (!strcmp(attr->attr.name,"pwrctl"))
		shift = 1;
	else if (!strcmp(attr->attr.name,"backlight"))
		shift = 2;
	else if (!strcmp(attr->attr.name,"hrev"))
		shift = 3;
	else if (!strcmp(attr->attr.name,"vrev"))
		shift = 4;
	else if (!strcmp(attr->attr.name,"reset"))
		shift = 5;
	else
		return -EIO;

	val = ioread16(lpBaseReg+gnCSR_OFFSET);
	val >>= shift;
	val &= 1;

	return sprintf(buf, "%d\n",val);
}

static ssize_t fpga_lcdctlr_csrbit_set(struct device *dev, 
		struct device_attribute *attr, const char *buf, size_t count)
{
	int bit, shift;
	struct fpga_lcdctlr* fpga_lcdctlr = (struct fpga_lcdctlr*)dev_get_drvdata(dev);

	sscanf(buf,"%d", &bit);

	if (!strcmp(attr->attr.name,"enable"))
		shift = 0;
	else if (!strcmp(attr->attr.name,"pwrctl"))
		shift = 1;
	else if (!strcmp(attr->attr.name,"backlight"))
		shift = 2;
	else if (!strcmp(attr->attr.name,"hrev"))
		shift = 3;
	else if (!strcmp(attr->attr.name,"vrev"))
		shift = 4;
	else if (!strcmp(attr->attr.name,"reset"))
		shift = 5;
	else
		return -EIO;

	modify_csr(fpga_lcdctlr, 1<<shift, bit<<shift);

	return count;
}
static DEVICE_ATTR(enable, S_IRUGO | S_IWUSR, fpga_lcdctlr_csrbit_show, fpga_lcdctlr_csrbit_set);
static DEVICE_ATTR(pwrctl, S_IRUGO | S_IWUSR, fpga_lcdctlr_csrbit_show, fpga_lcdctlr_csrbit_set);
static DEVICE_ATTR(backlight, S_IRUGO | S_IWUSR, fpga_lcdctlr_csrbit_show, fpga_lcdctlr_csrbit_set);
static DEVICE_ATTR(hrev, S_IRUGO | S_IWUSR, fpga_lcdctlr_csrbit_show, fpga_lcdctlr_csrbit_set);
static DEVICE_ATTR(vrev, S_IRUGO | S_IWUSR, fpga_lcdctlr_csrbit_show, fpga_lcdctlr_csrbit_set);
static DEVICE_ATTR(reset, S_IRUGO | S_IWUSR, fpga_lcdctlr_csrbit_show, fpga_lcdctlr_csrbit_set);

static struct attribute *fpga_lcdctlr_attributes[] = {
		&dev_attr_freq.attr,
		&dev_attr_duty.attr,
		&dev_attr_enable.attr,
		&dev_attr_backlight.attr,
		&dev_attr_pwrctl.attr,
		&dev_attr_hrev.attr,
		&dev_attr_vrev.attr,
		&dev_attr_reset.attr,
		NULL,
};

static struct attribute_group fpga_lcdctlr_attr_group = {
		.attrs = fpga_lcdctlr_attributes,
};

/**
 */
static int fpga_lcdctlr_IrqHandler(struct fpga_device* dev)
{
	struct fpga_lcdctlr *fpga_lcdctlr;
	unsigned short  *lpBaseReg;

	fpga_lcdctlr = dev_get_drvdata(&dev->dev);
	lpBaseReg = (unsigned short*)dev->baseaddr;

	return 0;
}

/**
 * This routine is called when a device is removed from the FPGA bus.
 *
 * \param[in] dev pointer to the device being removed.
 */
static int fpga_lcdctlr_remove(struct device *dev)
{
	int rv = 0;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	struct fpga_lcdctlr *fpga_lcdctlr = dev_get_drvdata(dev);

	sysfs_remove_group(&dev->kobj, &fpga_lcdctlr_attr_group);

	/* make sure IRQ is disabled */
	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
			   fpgadev->coreversion.ver0.bits.vector,
			   0);

	kfree(fpga_lcdctlr);

	return rv;
}

static int fpga_lcdctlr_probe(struct device *dev);

/**
 * The driver object.  The information here must be common for
 * all of the potential drivers in the system.
 */
static struct fpga_driver fpga_lcdctlr_driver = {
	.id		= CORE_ID_LCDCTRL, /** must match value in core_ids.h */
	.version	= "LCD Controller Driver 1.0",
	.IrqHandler	= fpga_lcdctlr_IrqHandler,
	.probe		= fpga_lcdctlr_probe,
	.remove		= fpga_lcdctlr_remove,
	.driver 	= {
		.name 	= "fpga_lcdctlr",
		.owner 	= THIS_MODULE,
	},
};

/**
 */
static int fpga_lcdctlr_probe(struct device *dev)
{
	int rv = 0;
	struct fpga_lcdctlr   *fpga_lcdctlr = NULL;
	unsigned short    *lpBaseReg = NULL;
	struct fpga_device *fpgadev = to_fpga_device(dev);

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	rv = sysfs_create_group(&dev->kobj, &fpga_lcdctlr_attr_group);
	if (rv)
	{
		dev_err(dev, "%s failed to add attributes group - %d\n", __func__, rv);
		goto probe_bail;
	}

	fpga_lcdctlr = (struct fpga_lcdctlr *)kmalloc(sizeof(struct fpga_lcdctlr),
							GFP_KERNEL);
	if (fpga_lcdctlr == NULL)
	{
		rv = -ENOMEM;
		goto probe_bail;
	}
	fpga_lcdctlr->fpgadev = fpgadev;

	dev_set_drvdata(dev, fpga_lcdctlr);

	dev_info(&fpgadev->dev, "FPGA LCDCTLR Loaded\n");

	return rv;

probe_bail:
	return rv;
}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init fpga_lcdctlr_init(void)
{
	int ret;

	ret = register_fpga_driver(&fpga_lcdctlr_driver);
	if (ret)
	{
		printk(KERN_ALERT "Unable to register fpga_lcdctlr_driver\n");
		goto exit_bail;
	}

	return 0;

exit_bail: /* Uh-Oh */
	return -1;
}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void fpga_lcdctlr_exit(void)
{
	driver_unregister(&fpga_lcdctlr_driver.driver);
}

module_init(fpga_lcdctlr_init);
module_exit(fpga_lcdctlr_exit);
