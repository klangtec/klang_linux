#ifndef I2CFACTORYCONFIG_H
#define I2CFACTORYCONFIG_H

#include <stdint.h>

#define CONFIG_I2C_MAGIC_WORD 0x012C0138
#define CONFIG_I2C_VERSION    0x00010001

#define PARTNO_SPEED_OFFSET 5
#define PARTNO_FPGA_OFFSET	6
#define PARTNO_NOR_OFFSET   8
#define PARTNO_NAND_OFFSET	9
#define PARTNO_RAM_OFFSET	10
#define PARTNO_ROHS_OFFSET	12
#define PARNO_TEMP_OFFSET	13

struct SpeedLookUp {
	int code;
	int speed_mhz;
};

static struct SpeedLookUp SpeedLookUpTable[] = {
		{ 'A', 200 },
		{ 'B', 250 },
		{ 'C', 300 },
		{ 'D', 375 },
		{ 'E', 450 },
		{ 'F', 456 },
		{ 'G', 720 },
		{ 'H', 850 },
		{ 'I', 1000 },
		{ -1, 0 } /* Sentinal */
};

struct RAMLookUp {
	int code;
	int size_mb;
};

static struct RAMLookUp RAMLookUpTable[] = {
		{ '5', 128 },
		{ '6', 256 },
		{ -1, -1 } /* Sentinal */
};

static struct RAMLookUp NANDLookUpTable[] = {
		{ '2', 256 },
		{ '3', 512 },
		{ -1, -1 } /* Sentinal */
};

static struct RAMLookUp NORLookUpTable[] = {
		{ '2', 8 },
		{ '3', 16 },
		{ -1, -1 } /* Sentinal */
};

struct FPGALookUp {
	int code;
	const char* type;
};

static struct FPGALookUp FPGALookUpTable[] = {
		{ 'F', "6SLX9" },
		{ 'G', "6SLX16" },
		{ 'H', "6SLX25" },
		{ 'I', "6SLX45" },
		{ 'X', "None" },
		{ -1, 0 } /* Sentinal */
};

typedef struct I2CFactoryConfig 
{
	uint32_t          ConfigMagicWord;  /** CONFIG_I2C_MAGIC_WORD */
	uint32_t          ConfigVersion;    /** CONFIG_I2C_VERSION */
	uint8_t           MACADDR[6];       /** mac address assigned to part */
	uint32_t          NoUse;            /** fpga installed OBE (see board part number) */
	uint32_t          Spare;            /** Not Used */
	uint32_t          SerialNumber;     /** serial number assigned to part */
	char              PartNumber[32];   /** board part number, human readable text, NULL terminated */
} tsFactoryConfig;

#endif
