#ifdef MITYDSP_HEADER_BLOCK
/**************************************************************************/
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Workfile:: DspAdcRegMap.h                                         $  */
/*  $Revision:: 5                                                      $  */
/*  $Date:: 9/21/07 8:22a                                              $  */
/*  $Author:: Mikew                                                    $  */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Description::                                                     $  */
/*   Register definitions for the DSP high speed ADC base class.          */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*     o  0                                                               */
/*     | /       Copyright (c) 2006-2007                                  */
/*    (CL)---o   Critical Link, LLC                                       */
/*      \                                                                 */
/*       O                                                                */
/**************************************************************************/
#endif

#ifndef FPGA_ADC_H
#define FPGA_ADC_H

#define VER_OFFSET  0
#define CCR_OFFSET  1
#define BDR_OFFSET  2
#define BQR_OFFSET  3
#define FSR_OFFSET  4
#define IER_OFFSET  5
#define FDR_OFFSET  6
#define FZR_OFFSET  7


// CCR Register
typedef union
{
    struct 
    {
        unsigned int mbEnable         : 1;
        unsigned int mbMode           : 1;
        unsigned int mbExt            : 1;
        unsigned int mbBurstComplete  : 1;
        unsigned int mbUserData       : 1;
        unsigned int mbPack           : 1;
        unsigned int mbFlip           : 1;
        unsigned int mbSaturationMode : 1;
        unsigned int mbDifferential   : 1;
        unsigned int mnReserved       : 7;
        unsigned int mnChannelMask    : 8;
        unsigned int mnReserved2re    : 8;
    } msBits;
    unsigned int mnLword;
} tuAdcCcr;

// BDR Register
typedef union
{
    struct 
    {
        unsigned int mnDelay      : 16;
        unsigned int mnReserved   : 16;
    } msBits;
    unsigned int mnLword;
} tuAdcBdr;

// BQR Register
typedef union
{
    struct 
    {
        unsigned int mnQuantity   : 16;
        unsigned int mnReserved   : 16;
    } msBits;
    unsigned int mnLword;
} tuAdcBqr;

// FIFO Status Register
typedef union
{
    struct 
    {
        unsigned int mbFifoEmpty      :  1;
        unsigned int mbFifo1QFull     :  1;
        unsigned int mbFifoHalfFull   :  1;
        unsigned int mbFifo3QFull     :  1;
        unsigned int mbFifoFull       :  1;
        unsigned int mnReserved       : 11;
        unsigned int mnFifoLevel      : 16;
    } msBits;
    unsigned int mnLword;
} tuAdcFsr;

// Interrupt Enable Register
typedef union
{
    struct 
    {
        unsigned int mbInt1QFull        :  1;
        unsigned int mbIntHalfFull      :  1;
        unsigned int mbInt3QFull        :  1;
        unsigned int mbIntFull          :  1;
        unsigned int mbIntBurstComplete :  1;
        unsigned int mnReserved         : 27;
    } msBits;
    unsigned int mnLword;
} tuAdcIer;

// FZR Register
typedef union
{
    struct 
    {
        unsigned int mnSizeMinusOne : 16;
        unsigned int mnReserved     : 16;
    } msBits;
    unsigned int mnLword;
} tuAdcFzr;

#endif
