#ifndef FPGA_PWM_H
#define FPGA_PWM_H

/* Register Word (2 byte) Offsets */
static const int gnVER_OFFSET   = 0;
static const int gnCSR_OFFSET   = 1;
static const int gnCD1_OFFSET   = 2;
static const int gnRST_OFFSET   = 3;
static const int gnPRD_OFFSET   = 4;
static const int gnADDR_OFFSET  = 5;
static const int gnVALS_OFFSET  = 6;
static const int gnONS_OFFSET   = 7;

#define PWM_ACTION_IGNORE   0
#define PWM_ACTION_SET_ZERO 1
#define PWM_ACTION_SET_ONE  2
#define PWM_ACTION_TOGGLE   3

typedef struct 
{
    uint16_t cnta;
    uint16_t cntb;
    union {
        struct {
            unsigned int zero_action : 2;
            unsigned int a_action    : 2;
            unsigned int b_action    : 2;
            unsigned int force_zero  : 1;
            unsigned int force_one   : 1;
            unsigned int leave_one   : 1;
            unsigned int not_used    : 1;
            unsigned int one_shot_act: 2;
            unsigned int unused      : 4;
        } bits;
        uint16_t word;
    } tuFlags;
} tsPWMVals;

typedef union
{
    struct {
        unsigned int clk_en : 1;
        unsigned int active_ram : 1;
        unsigned int unused : 2;
        unsigned int clk_div_low : 12;
    } bits;
    uint16_t word;
} tuPWMCSR;

typedef union
{
    struct {
        unsigned int clk_div_high : 12;
        unsigned int unused       : 4;
    } bits;
    uint16_t word;
} tuPWMCD1;

typedef union
{
	struct {
		unsigned int clear_oneshot : 8;
		unsigned int oneshot_set   : 8;
	} bits;
	uint16_t word;
} tuPWMONS;

#endif
