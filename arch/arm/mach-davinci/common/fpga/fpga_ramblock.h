#ifdef MITYDSP_HEADER_BLOCK
/**************************************************************************/
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Workfile:: DspRamBlockRegMap.h                                    $  */
/*  $Revision:: 1                                                      $  */
/*  $Date:: 3/08/06 2:47p                                              $  */
/*  $Author:: Johnk                                                    $  */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Description::                                                     $  */
/*   Register definitions for the RAM Block firmware interface.           */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*     o  0                                                               */
/*     | /       Copyright (c) 2006                                       */
/*    (CL)---o   Critical Link, LLC                                       */
/*      \                                                                 */
/*       O                                                                */
/**************************************************************************/
#endif

#ifndef DSP_RAM_BLOCK_REGMAP_INCLUDED
#define DSP_RAM_BLOCK_REGMAP_INCLUDED
    
// Register Offsets
const int gnVER_OFFSET  =  0;
const int gnBCR_OFFSET  =  1;
const int gnBSR_OFFSET  =  2;
const int gnRBx_OFFSET  = 16;

// Register Definitions
typedef union
{
    struct 
    {
        unsigned int mbIntEnable  :  1;
        unsigned int mnBlockWidth :  2;
        unsigned int mnUnused     : 29;
    } msBits;
    unsigned int mnLword;
} tuRamBlockBcr;

typedef union
{
    struct 
    {
        unsigned int mbInDirty0   : 1;
        unsigned int mbInDirty1   : 1;
        unsigned int mbInDirty2   : 1;
        unsigned int mbInDirty3   : 1;
        unsigned int mbInDirty4   : 1;
        unsigned int mbInDirty5   : 1;
        unsigned int mbInDirty6   : 1;
        unsigned int mbInDirty7   : 1;
        unsigned int mbInDirty8   : 1;
        unsigned int mbInDirty9   : 1;
        unsigned int mbInDirty10  : 1;
        unsigned int mbInDirty11  : 1;
        unsigned int mbInDirty12  : 1;
        unsigned int mbInDirty13  : 1;
        unsigned int mbInDirty14  : 1;
        unsigned int mbInDirty15  : 1;
        unsigned int mbOutDirty0  : 1;
        unsigned int mbOutDirty1  : 1;
        unsigned int mbOutDirty2  : 1;
        unsigned int mbOutDirty3  : 1;
        unsigned int mbOutDirty4  : 1;
        unsigned int mbOutDirty5  : 1;
        unsigned int mbOutDirty6  : 1;
        unsigned int mbOutDirty7  : 1;
        unsigned int mbOutDirty8  : 1;
        unsigned int mbOutDirty9  : 1;
        unsigned int mbOutDirty10 : 1;
        unsigned int mbOutDirty11 : 1;
        unsigned int mbOutDirty12 : 1;
        unsigned int mbOutDirty13 : 1;
        unsigned int mbOutDirty14 : 1;
        unsigned int mbOutDirty15 : 1;
    } msBits;
    unsigned short mnInDirty;
    unsigned short mnOutDirty;
    unsigned int   mnLword;
} tuRamBlockBsr;

typedef union
{
    struct 
    {
        unsigned int mnData       :  8;
        unsigned int mnUnused     : 24;
    } ms8Bit;
    struct 
    {
        unsigned int mnData       : 16;
        unsigned int mnUnused     : 16;
    } ms16Bit;
    struct 
    {
        unsigned int mnData       : 24;
        unsigned int mnUnused     :  8;
    } ms24Bit;
    unsigned int mnLword;
} tuRamBlockData;

#endif

