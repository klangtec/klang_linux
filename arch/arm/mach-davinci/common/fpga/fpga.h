/*
 * fpga.h
 *
 *  Created on: Mar 5, 2010
 *      Author: mikew
 */

/* This doesn't work.  Kernel makefile is running -nostdinc --isystem (gcc -print-search-path=include)
#include <stdint.h>
*/
#if defined(_TMS320C6X)
#include <stdint.h>
#else
#include <linux/types.h>
#endif

#ifndef CMN_FPGA_H
#define CMN_FPGA_H

#define FPGA_BASE_ADDR 0x66000000 /* ARM side interface, CS5 */
#define FPGA_CORE_SIZE 0x80       /* Core address region */
#define FPGA_BASEMODULE_OFFSET  0
#define FPGA_BASEYEAR 2000

/**
 *  Core Version Register, FIFO_no = 0
 */
typedef union corever0 {
	struct bits0 {
		unsigned int core_id : 8; /* Core ID 0xF0-0xFF are reserved for customers */
		unsigned int vector  : 4; /* interrupt vector level */
		unsigned int level   : 2; /* interrupt level */
		unsigned int FIFO_no : 2; /* = 00 */
	} bits;
	uint16_t word;
} corever0;

/**
 *  Core Version Register, FIFO_no = 1
 */
typedef union corever1 {
	struct bits1 {
		unsigned int minor    : 4; /* minor revision */
		unsigned int major    : 4; /* major revision */
		unsigned int year     : 5; /* years since 2000 */
		unsigned int reserved : 1; /* not used */
		unsigned int FIFO_no  : 2; /* = 01 */
	} bits;
	uint16_t word;
} corever1;

/**
 *  Core Version Register, FIFO_no = 2
 */
typedef union corever2 {
	struct bits2 {
		unsigned int day       : 5; /* minor revision */
		unsigned int reserved  : 3; /* not used */
		unsigned int month     : 4; /* major revision */
		unsigned int reserved1 : 2; /* not used */
		unsigned int FIFO_no   : 2; /* = 10 */
	} bits;
	uint16_t word;
} corever2;

/**
 *  Core Version Register, FIFO_no = 3
 */
typedef union corever3 {
	struct bits3 {
		unsigned int reserved1 : 14; /* not used */
		unsigned int FIFO_no   : 2; /* = 10 */
	} bits;
	uint16_t word;
} corever3;

/**
 * This structure holds the FPGA core version information.
 */
struct coreversion {
	corever0 ver0;
	corever1 ver1;
	corever2 ver2;
	corever3 ver3;
};

/**
 * This structure describes the interrupt assignment register
 */
typedef union intass {
	struct thebits {
		unsigned int irq0_cpu : 1;  /* 0 = ARM, 1 = DSP */
		unsigned int irq1_cpu : 1;  /* 0 = ARM, 1 = DSP */
	} bits;
	uint16_t word;
} intass;

#define BM_CPU_ARM   0
#define BM_CPU_DSP   1

/** 
 *  Base Module Register definitions
 */
#define BM_VERSION_OFFSET           0
#define BM_MASKED_IRQS_INT0_OFFSET  1
#define BM_ENABLED_IRQS_INT0_OFFSET 2
#define BM_MASKED_IRQS_INT1_OFFSET  3
#define BM_ENABLED_IRQS_INT1_OFFSET 4
#define BM_INT_ASSIGNMENTS_OFFSET   5
#define BM_FPGA_VERSION_OFFSET      6
#define BM_DEVDNA_OFFSET_LSW        8
#define BM_DEVDNA_OFFSET_MSW        11
#define BM_SCRATCH_RAM_OFFSET       32
#define BM_SCRATCH_RAM_SIZE_BYTES   64

#endif
