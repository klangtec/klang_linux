/*
 * core_ids.h
 *
 *  Created on: Apr 20, 2010
 *      Author: mitydspl138
 */

#ifndef CORE_IDS_H_
#define CORE_IDS_H_

struct CoreDescription {
	unsigned char ID;
	const char*   Name;
};

#define CORE_ID_BASEMODULE    0
#define CORE_ID_TFP410        1
#define CORE_ID_LCDCTRL       2
#define CORE_ID_UART          3
#define CORE_ID_GPIO          4
#define CORE_ID_I2C           7
#define CORE_ID_PWM           8
#define CORE_ID_STEPPER3967   9
#define CORE_ID_SPI           14
#define CORE_ID_ADS7843       23
#define CORE_ID_DCMCTLR       24
#define CORE_ID_HDLC          25
#define CORE_ID_CAMLINK       27
#define CORE_ID_MIGIFACE      33

#ifdef FPGA_CTRL_C
struct CoreDescription KnownCores[] =
{
		{ CORE_ID_BASEMODULE,  "Base Module" },
		{ CORE_ID_ADS7843,     "ADS7843 Touch Screen" },
		{ CORE_ID_LCDCTRL,     "LCD Settings Controller" },
		{ CORE_ID_GPIO,        "GPIO" },
		{ CORE_ID_I2C,         "I2C Interface" },
		{ CORE_ID_SPI,         "SPI Interface" },
		{ CORE_ID_TFP410,      "TFP410 DVI Controller" },
		{ CORE_ID_PWM,         "PWM Interface" },
		{ CORE_ID_UART,        "UART Interface" },
		{ CORE_ID_DCMCTLR,     "DCM Control Interface" },
		{ CORE_ID_HDLC,        "HDLC Interface" },
		{ CORE_ID_MIGIFACE,    "Xilinx MIG Interface" },
		{ CORE_ID_STEPPER3967, "A3967 Stepper Controller" },
		{ CORE_ID_CAMLINK,     "Camera Link Interface" },
};
#else
extern struct CoreDescription KnownCores[];
#endif

#endif /* CORE_IDS_H_ */
