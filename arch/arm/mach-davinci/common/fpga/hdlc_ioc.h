/*
 * hdlc.h
 *
 * Copyright (C) 2011 Critical Link LLC
  */

#ifndef HDLC_H
#define HDLC_H

#include <linux/types.h>

/* User space defines etc for HDLC interface,
 * matching <linux/spi/spi.h>
 */


/*---------------------------------------------------------------------------*/

/* IOCTL commands */

#define HDLC_IOC_MAGIC			'h'


/* Read / Write of HDLC mode (HDLC_MODE_0..HDLC_MODE_1) */
#define HDLC_IOC_RD_MODE			_IOR(HDLC_IOC_MAGIC, 0, __u8)
#define HDLC_IOC_WR_MODE			_IOW(HDLC_IOC_MAGIC, 0, __u8)

/* Read / Write SPI bit justification */
#define HDLC_IOC_SEND_FRAME			_IOW(HDLC_IOC_MAGIC, 1, __u8)

#endif /* HDLC_H */
