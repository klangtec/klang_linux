/*
 * fpga_camlink.h
 *
 *  Created on: Mar 20, 2012
 *      Author: mikew
 */
#ifndef FPGA_CAMLINK_H
#define FPGA_CAMLINK_H

/* 2 byte (word) offsets */
static const int VSN_OFFSET = 0;
static const int CCR_OFFSET = 1;
static const int PCR_OFFSET = 2;  /* pixel count 24 bit number, read/write as 32 bit value */
static const int FTR_OFFSET = 4;  /* frame timer 24 bit number, read as 32 bit value */
static const int HSR_OFFSET = 6;
static const int HER_OFFSET = 7;
static const int VSR_OFFSET = 8;
static const int VER_OFFSET = 9;

/*  CCR Register */
typedef union
{
    struct 
    {
        unsigned int mbEnableCapture  : 1;   //!< set to 1 to enable camera capture
        unsigned int mbMaskFrameBits  : 1;   //!< set to 1 to mask Frame and Line bits to zero
		unsigned int mbResetFIFO      : 1;   //!< when 1, attached FIFO is cleared
		unsigned int mbXORDValid      : 1;   //!< when 1, invert DVALID signal
		unsigned int mbInvertLV       : 1;   //!< Invert Line Valid signal
		unsigned int mbInvertFV       : 1;   //!< Invert Frame Valid signal
	    unsigned int reserved         : 2;   //!< reserved
		unsigned int mnFrameDecimate  : 4;   //!< number of frames to skip between successive captures (0 to 15)
        unsigned int mnPXD            : 4;   //!< Bits per pixel
    } msBits;
    unsigned short mnWord;
} tuCamLinkCcr;

/**
 * HTR Register.
 * The Horizontal Data Timing Register allows users to discard pixels 
 * received in a horizontal line (when the data valid, line valid, and
 * frame valid strobes are asserted).  At the beginning of a line (rising
 * edge of line valid), the number of pixels specified by START_PIXEL will
 * be discarded and not clocked into the FIFO.  Once the START_PIXEL has
 * been reached, the core will then capture until END_PIXEL 
 * or until the line valid is de-asserted, at which point the
 * horizontal capture state machine will reset.
 */
typedef union
{
    struct 
    {
        unsigned int mnStart     : 12; //!< first position to capture
        unsigned int mnReserved1 :  4; //!< not used
    } msBits;
    unsigned short mnWord;
} tuCamLinkHsr;

typedef union
{
    struct 
    {
        unsigned int mnStop      : 12; //!< Pixel position to stop capturing
        unsigned int mnReserved  :  4; //!< not used
    } msBits;
    unsigned short mnWord;
} tuCamLinkHer;

/**
 * VTR Register.
 * The Vertical Data Timing Register allows users to discard lines received in
 * a frame (when the data valid, line valid, and frame valid strobes are asserted).
 * At the beginning of a frame (rising edge of frame valid), the number of lines
 * specified by START_LINE will be discarded and not clocked into the FIFO.  Once
 * the START_LINE has been reached, the core will then capture until END_LINE
 * is reached or until the frame valid is de-asserted, at which point
 * the line capture state machine will reset.
 */
typedef union
{
    struct 
    {
        unsigned int mnStart     : 12; //!< First line to capture
        unsigned int mnReserved1 :  4; //!< not used
    } msBits;
    unsigned short mnWord;
} tuCamLinkVsr;

typedef union
{
    struct 
    {
        unsigned int mnStop      : 12; //!< Line position to stop capturing
        unsigned int mnReserved  :  4; //!< not used
    } msBits;
    unsigned short mnWord;
} tuCamLinkVer;

#endif
