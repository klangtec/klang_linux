#ifdef MITYDSP_HEADER_BLOCK
/**************************************************************************/
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Workfile:: DspCounterRegMap.h                                     $  */
/*  $Revision:: 2                                                      $  */
/*  $Date:: 2/20/06 5:10p                                              $  */
/*  $Author:: Johnk                                                    $  */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Description::                                                     $  */
/*   Register definitions for the MityDSP counter core.                   */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*     o  0                                                               */
/*     | /       Copyright (c) 2006                                       */
/*    (CL)---o   Critical Link, LLC                                       */
/*      \                                                                 */
/*       O                                                                */
/**************************************************************************/
#endif

#ifndef FPGA_COUNTER_H
#define FPGA_COUNTER_H

// Register Offsets
const int gnVER_OFFSET  =  0;
const int gnISR_OFFSET  =  1;
const int gnCSR0_OFFSET = 16;

// CCR Register Masks
const unsigned int gnCSR_INT_ENABLE_MASK = 0x80000000;
const unsigned int gnCSR_INIT_VALID_MASK = 0x40000000;

typedef union
{
    struct 
    {
        unsigned int mnCount     : 30;
        unsigned int mbInitValid :  1;
        unsigned int mbIntEnable :  1;
    } msBits;
    unsigned int mnLword;
} tuCounterCsr;

#endif
