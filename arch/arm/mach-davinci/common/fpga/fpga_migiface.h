/*
 * fpga_migiface.h
 *
 *  Created on: Feb 2, 2012
 *      Author: mikew
 */

#ifndef FPGA_MIGIFACE_H_
#define FPGA_MIGIFACE_H_

#define RD_FIFO_SIZE	64
#define WR_FIFO_SIZE	64

/* Register Word (2 byte) Offsets */
static const int gnVER_OFFSET  = 0; // Standard Core Version Register
static const int gnCMD_OFFSET  = 1; // Command Register
static const int gnSTS_OFFSET  = 2; // Status Register
static const int gnWRL_OFFSET  = 3; // Write FIFO Low Word
static const int gnWRH_OFFSET  = 4; // Write FIFO High Word
static const int gnRDL_OFFSET  = 5; // Read FIFO Low Word
static const int gnRDH_OFFSET  = 6; // Read FIFO High Word
static const int gnADL_OFFSET  = 7; // Address Low Word
static const int gnADH_OFFSET  = 8; // Address High Word

typedef union
{
    struct
    {
    	unsigned int cmd_instr	: 3; /* MIG Command, see above */
    	unsigned int reserved	: 5; /* unused */
    	unsigned int cmd_bl	: 6; /* burst length (in 32 bit words) */
    	unsigned int unused	: 2; /* unused */
    } msBits;
    unsigned short mnWord;
} tuMIGCMD; // Command Register

typedef union
{
    struct
    {
        unsigned int calib_done		: 1;
        unsigned int cmd_empty		: 1;
        unsigned int cmd_full		: 1;
        unsigned int rd_empty		: 1;
        unsigned int rd_full		: 1;
        unsigned int rd_overflow	: 1;
	unsigned int rd_error		: 1;
	unsigned int wr_empty		: 1;
	unsigned int wr_full		: 1;
	unsigned int wr_underrun	: 1;
	unsigned int wr_error		: 1;
	unsigned int reserved		: 5;
    } msBits;
    unsigned short mnWord;
} tuMIGSTS; // Status Register

#endif /* FPGA_SPI_H_ */
