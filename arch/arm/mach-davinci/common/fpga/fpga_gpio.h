/*
 * gpio.h
 *
 *  Created on: Aug 7, 2010
 *      Author: Michael Williamson
 */

#ifndef FPGA_GPIO_H_
#define FPGA_GPIO_H_

/* Register Long Word (2 byte) Offsets */
static const int gnVER_OFFSET    = 0;
static const int gnCFG_OFFSET    = 1;
static const int gnBANK_OFFSET[4] = { 2, 7, 12, 17 };

/* Register offsets within a bank */
static const int gnIOVAL_OFFSET	= 0;
static const int gnIODIR_OFFSET	= 1;
static const int gnIRE_OFFSET		= 2;
static const int gnIFE_OFFSET		= 3;
static const int gnIP_OFFSET		= 4;

typedef union
{
    struct
    {
		unsigned int mnNumIOPerBank : 8;
		unsigned int mnNumBanks     : 4;
		unsigned int mnReserved     : 4;
    } msBits;
    unsigned short mnWord;
} tuConfigReg;

#endif /* FPGA_GPIO_H_ */
