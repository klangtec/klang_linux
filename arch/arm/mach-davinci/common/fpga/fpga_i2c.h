/*
 * fpga_i2c.h
 *
 *  Created on: May 3, 2010
 *      Author: mitydspl138
 */

#ifndef FPGA_I2C_H_
#define FPGA_I2C_H_

#define FPGA_I2C_TIMEOUT	(1*HZ)

/* Register Long Word (2 byte) Offsets */
static const int gnVER_OFFSET  = 0;
static const int gnIER_OFFSET  = 1;
static const int gnCSR_OFFSET  = 2;
static const int gnSAR_OFFSET  = 3;
static const int gnCNT_OFFSET  = 4;
static const int gnDIV_OFFSET  = 5;
static const int gnFDR_OFFSET  = 6;

#define FPGA_I2C_FIFO_DEPTH 32

typedef union
{
    struct
    {
    	unsigned int mbDoneIER        : 1;
    	unsigned int mbTxHalfEmptyIER : 1;
    	unsigned int mbRxHalfFullIER  : 1;
    	unsigned int mnReserved       : 13;
    } msBits;
    unsigned short mnWord;
} tuI2cIER;

typedef union
{
    struct
    {
    	unsigned int mbDone     : 1;
    	unsigned int mbGo       : 1;
    	unsigned int mbRnW      : 1;
    	unsigned int mb10Bit    : 1;
    	unsigned int mbStop     : 1;
    	unsigned int mbSDA      : 1; /* for debug */
    	unsigned int mbAckErr   : 1;
    	unsigned int mbFifoReset: 1;
    	unsigned int mnReserved : 8;
    } msBits;
    unsigned short mnWord;
} tuI2cCSR;

typedef union
{
    struct
    {
        unsigned int mnSlaveAddr : 10;
        unsigned int mnReserved  : 6;
    } msBits;
    unsigned short mnWord;
} tuI2cSAR;

typedef union
{
    struct
    {
        unsigned int mnData      : 8;
        unsigned int mnReserved  : 6;
    } msBits;
    unsigned short mnWord;
} tuI2cFDR;


#endif /* FPGA_I2C_H_ */
