/*
 * fpga_stepper3967.h
 *
 *      Author: mikew
 */

#ifndef FPGA_STEPPER3967_H
#define FPGA_STEPPER3967_H

// STEPPER register offsets (4 byte word offsets)
static const int gnVerOffset   = 0;     //!< Version Register Offset
static const int gnCsrOffset   = 1;     //!< Control and Status Register
static const int gnVirOffset   = 2;     //!< Velocity Increment Register
static const int gnVbrOffset   = 3;     //!< Velocity Bounds Register
static const int gnPdOffset    = 4;     //!< Position Divisor Register
static const int gnComOffset   = 5;     //!< Command Register
static const int gnScrOffset   = 6;     //!< Step Count Register
static const int gnOvrOffset   = 7;     //!< Overshoot register

// register definition area
#define CSR_RST_MASK     0x00000001
#define CSR_EN_MASK      0x00000002
#define CSR_SLP_MASK     0x00000004
#define CSR_GO_MASK      0x00000008
#define CSR_SSTOP_MASK   0x00000010
#define CSR_DIE_MASK     0x00000020
#define CSR_DONE_MASK    0x00000040
#define CSR_CONT_MASK    0x00000080
#define CSR_BUSY_MASK    0x00000100
#define CSR_MS2_MASK     0x00000200
#define CSR_MS1_MASK     0x00000400
#define CSR_MS_MASK      0x00000600
#define CSR_MS_SHIFT     9
#define CSR_MSV_MASK     0x00000800
#define CSR_TS1_MASK     0x00001000
#define CSR_TS2_MASK     0x00002000
#define CSR_TS3_MASK     0x00004000
#define CSR_TS4_MASK     0x00008000
#define CSR_TS5_MASK     0x00010000
#define CSR_TS6_MASK     0x00020000
#define CSR_TS7_MASK     0x00040000
#define CSR_ESTOP_MASK   0x00080000
#define CSR_TCOND_MASK   0x00100000
#define CSR_HC2_MASK	 0x00200000
#define CSR_HC1_MASK	 0x00400000
#define CSR_HC_MASK      0x00600000
#define CSR_HC_SHIFT     20
#define CSR_HCV_MASK     0x00800000
#define CSR_TS_CLEAR     0x000FF000

#define VBR_MAXPER_MASK  0xFFFF0000
#define VBR_MAXPER_SHIFT 16
#define VBR_MINPER_MASK  0x0000FFFF

#define VIR_VELINC_MASK  0x00FFFFFF

#define PDR_POSDIV_MASK  0x0000001F

#define COM_DIR_MASK     0x80000000
#define COM_DIR_SHIFT    31
#define COM_TSMASK_MASK  0x70000000
#define COM_TSMASK_SHIFT 28
#define COM_TVLCNT_MASK  0x0FFFFFFF

#define OVR_OVRSHT_MASK  0x00FFFFFF

#endif
