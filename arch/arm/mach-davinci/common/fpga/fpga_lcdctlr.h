#ifndef FPGA_LCDCTLR_H
#define FPGA_LCDCTLR_H

/* Register Word (2 byte) Offsets */
static const int gnVER_OFFSET		= 0;
static const int gnCSR_OFFSET		= 2;
static const int gnPWM_CLKS_OFFSET	= 3;
static const int gnPWM_ON_OFFSET	= 4;

typedef union
{
    struct {
        unsigned int lcd_en		: 1;
        unsigned int lcd_pwr		: 1;
        unsigned int lcd_backlit	: 1;
        unsigned int lcd_hrev		: 1;
        unsigned int lcd_vrev		: 1;
        unsigned int lcd_reset		: 1;
        unsigned int unused		: 10;
    } bits;
    uint16_t word;
} tuCSR;

#endif
