/*
 * fpga_hdlc.h
 *
 *  Created on: 11/29/11
 *      Author: Gregory Gluszek
 */

#ifndef _FPGA_HDLC_H_
#define _FPGA_HDLC_H_

// #include <stdint.h>

/* Register Word (2 byte) Offsets */
const int gnVER_OFFSET			= 0; //!< Version Register.
const int gnCTRL_OFFSET			= 1; //!< Control Register.
const int gnRX_FIFO_OFFSET		= 2; //!< Receive FIFO Register.
const int gnTX_FIFO_OFFSET		= 2; //!< Transfer FIFO Register.
const int gnFRAME_SIZE_OFFSET		= 3; //!< Num of bytes in current frame.
const int gnSTATUS_OFFSET		= 4; //!< Status Register.
const int gnIER_OFFSET			= 5; //!< Interrupt Enable Register.
const int gnISR_OFFSET			= 6; //!< Interrupt Status Register.
const int gnSTATUS_TXFIFO_OFFSET	= 7; //!< TX FIFO Status Register.
const int gnSTATUS_RXFIFO_OFFSET	= 8; //!< RX FIFO Status Register.
const int gnSTATUS_ERROR_COUNT_OFFSET   = 9; //!< FPGA ERROR Count Register.

typedef union
{
	struct
	{
		uint16_t tx_go      : 1; //!< Transmit 'go' bit. Starts transmit
					 //!< using data in TX FIFO.
		uint16_t tx_pol     : 1; //!< Transmit data polarity.
					 //!< 0 = active high.
		uint16_t txc_pol    : 1; //!< Transmit clock polarity.
					 //!< 0 = active high.
		uint16_t txc_rxc    : 1; //!< When set rx clock drives tx clock.
		uint16_t rx_pol     : 1; //!< Receive input polarity.
					 //!< 0 = active high.
		uint16_t rxc_pol    : 1; //!< Receive clock polarity.
					 //!< 0 = active high.
		uint16_t unused_1   : 1;
		uint16_t enable     : 1; //!< currently not implemented in FPGA (12/13/11)
		uint16_t hdlc_den   : 1; //!< Enable the xceiver output
		uint16_t hdlc_ren   : 1; //!< Enable the xceiver input

		uint16_t unused_2   : 4;

		uint16_t reserved   : 1; //!< Reserved
		uint16_t srst       : 1; //!< Synchronous reset of core.
	} msBits;
	uint16_t mnWord;
} tuCtrlReg;

typedef union
{
	struct
	{
		uint16_t rx_byte	: 8; //!< Receive data byte from FIFO.
		uint16_t unused_0	: 8;
	} msBits;
	uint16_t mnWord;
} tuRxFifoReg;

typedef union
{
	struct
	{
		uint16_t tx_byte	: 8; //!< Transmit data byte to FIFO.
		uint16_t frame_delim_v	: 1; //!< Set to 1 if data is a frame
				//!< delimiter and should not be zero stuffed.
		uint16_t unused_0	: 7;
	} msBits;
	uint16_t mnWord;
} tuTxFifoReg;

typedef union
{
	struct
	{
		uint16_t num_bytes : 15; //!< Number of bytes in the frame
		uint16_t frame_error : 1; //!< Number of bytes in the frame
			//!< corresponding to most recent interrupt.
	} msBits;
	uint16_t mnWord;
} tuFrameSizeReg;

typedef union
{
	struct
	{
		uint16_t rx_clock_present	:  1; //!< Receive Clock Present
		uint16_t unused_0		: 14; //!< Unused Bytes
	} msBits;
	uint16_t mnWord;
} tuStatusReg;

typedef union
{
	struct
	{
		uint16_t tx_fifo_available	: 12; //!< Available Tx Bytes
		uint16_t tx_fifo_full		:  1; //!< Tx FIFO Full
		uint16_t tx_fifo_almost_full	:  1; //!< Tx FIFO almost full
		uint16_t tx_fifo_almost_empty	:  1; //!< Tx FIFO almost empty
		uint16_t tx_fifo_empty		:  1; //!< Tx FIFO empty		
	} msBits;
	uint16_t mnWord;
} tuStatusTXFIFOReg;

typedef union
{
	struct
	{
		uint16_t rx_fifo_available	: 12; //!< Num Bytes To Read
		uint16_t rx_fifo_full		:  1; //!< Rx FIFO Full
		uint16_t rx_fifo_almost_full	:  1; //!< Rx FIFO almost full
		uint16_t rx_fifo_almost_empty	:  1; //!< Rx FIFO almost empty
		uint16_t rx_fifo_empty		:  1; //!< Rx FIFO empty		
	} msBits;
	uint16_t mnWord;
} tuStatusRXFIFOReg;

typedef union
{
	struct
	{
		uint16_t errors	: 16; //!< Num Bytes To Read
	} msBits;
	uint16_t mnWord;
} tuStatusErrorCountReg;

typedef union
{
	struct
	{
		uint16_t int_frame_rx	: 1; //!< Enable interrupt when a frame
						//!< delimiter is received.
		uint16_t unused_0	: 15;
	} msBits;
	uint16_t mnWord;
} tuIerReg;

typedef union
{
	struct
	{
		uint16_t int_frame_rx : 1; //!< Interrupt for when a frame
			//!< delimiter has been received.
		uint16_t unused_0 : 15;
	} msBits;
	uint16_t mnWord;
} tuIsrReg;

#endif /* _FPGA_HDLC_H_ */

