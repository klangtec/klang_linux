/*
 * ads7843.h
 *
 *  Created on: Apr 20, 2010
 *      Author: mitydspl138
 */

#ifndef ADS7843_H_
#define ADS7843_H_

#define TS_POLL_TIME  (20 * HZ / 1000)

// Register Long Word (4 byte) Offsets
static const int gnVER_OFFSET       =  0;
static const int gnPOSITION_OFFSET  =  1;
static const int gnDISP_CMD_OFFSET  =  2;
static const int gnDISP_RESP_OFFSET =  3;
static const int gnGPIO_OUT_OFFSET  =  4;
static const int gnGPIO_IN_OFFSET   =  5;

/**
 *   ADS7843 Core Command and Status Register definition.
 */
typedef union
{
    struct
    {
        unsigned int mnXCount     : 12;  //!< ADC X register count
        unsigned int mbXYInRange  :  1;  //!< when 1 XY values are in legal ADC range
        unsigned int mbGPIOEnable :  1;  //!< when 1, enable GPIO polling
        unsigned int mbCMDDone    :  1;  //!< when 1, a DISP command cycle has completed, clear by reading DISP_RESP_OFFSET
        unsigned int mbDispIrqEn  :  1;  //!< write 1 to this to enable CMDDone interrupts
        unsigned int mnYCount     : 12;  //!< ADC Y register count
        unsigned int mbBusy       :  1;  //!< raw "busy" bit from ADS7843, not used
        unsigned int mbPenDown    :  1;  //!< when true (1) pen is down
        unsigned int mbIntAck     :  1;  //!< write 1 to this to clear new coordinate state
        unsigned int mbIntEnable  :  1;  //!< write 1 to this to enable coordinate/pen interrupts
    } msBits;
    unsigned int mnLword;
} tuPositionReg;

/**
 *   ADS7843 GPIO Output and Input and DISP Command/Response Register definitions.
 */
typedef union
{
    struct
    {
        unsigned int mnData      : 24;  //!< the data payload
        unsigned int mnUnused    :  8;  //!< not used
    } msBits;
    unsigned int mnLword;
} tuDataIOReg;


#endif /* ADS7843_H_ */
