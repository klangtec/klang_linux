#ifndef FPGA_DCMCTLR_H
#define FPGA_DCMCTLR_H

/* Register Word (2 byte) Offsets */
static const int gnVER_OFFSET		= 0;
static const int gnPSC_OFFSET		= 1;
static const int gnDFS_OFFSET		= 2;
static const int gnDFC_OFFSET		= 3;

typedef union
{
	struct {
		unsigned int done	: 1; //!< write 1 to clear
		unsigned int overflow 	: 1; //!< high when ps is at edge of limit
		unsigned int go         : 1; //!< write 1 to increment cycle
		unsigned int psincdec	: 1; //!< 1 is up, 0 is down
		unsigned int unused	: 12;
	} bits;
	uint16_t word;
} tuPSC;

typedef union
{
	struct {
		unsigned int D      : 8; //!< desired divisor setting
		unsigned int M      : 8; //!< desired multiplier setting
	} bits;
	uint16_t word;
} tuDFS;

typedef union
{
	struct {
		unsigned int go     : 1; //!< write 1 to start increment cycle
		unsigned int done   : 1; //!< write 1 to clear
		unsigned int locked : 1; //!< 1 when locked
		unsigned int unused : 13; //!< unused
	} bits;
	uint16_t word;
} tuDFC;

#endif
