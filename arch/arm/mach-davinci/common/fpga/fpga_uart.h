/*
 * fpga_uart.h
 *
 *  Created on: Jan 1, 2011
 *      Author: Michael Williamson
 */

#ifndef FPGA_UART_H_
#define FPGA_UART_H_

// #include <stdint.h>

/* Register (byte-wise) Offsets */
static const uint32_t gnVER_OFFSET		= 0; //!< Version register.
static const uint32_t gnRHR_OFFSET		= 2; //!< Receive Holding Register.
static const uint32_t gnTHR_OFFSET		= 2; //!< Transmitter Holding Register. 
static const uint32_t gnMSR_OFFSET		= 4; //!< Modem Status Register. 
static const uint32_t gnLCR_OFFSET		= 5; //!< Line Control Register. 
static const uint32_t gnDLL_OFFSET		= 8; //!< Divisor LSB Latch. 
static const uint32_t gnDLH_OFFSET		= 9; //!< Divisor HSB Latch. 
static const uint32_t gnEFR_OFFSET		= 10; //!< Enhanced Feature Register. 
static const uint32_t gnIER_OFFSET		= 11; //!< Interrupt Enable Register. 
static const uint32_t gnMCR_OFFSET		= 12; //!< Modem Control Register. 
static const uint32_t gnLSR_OFFSET		= 13; //!< Line Status Register.

/// Maximum depth of the UART internal TX FIFO
static const uint32_t UART_TX_FIFO_DEPTH = 64;

typedef union
{
	struct
	{
		uint8_t rhr_ready_ie	: 1; //!< Receive data available.
		uint8_t tx_empty_ie	: 1; //!< Transmit empty.
		uint8_t ms_ie		: 1; //!< Modem Status. 
		uint8_t unused		: 5;
	} msBits;
	uint8_t mnWord;
} tuIERReg;

typedef union
{
	struct
	{
		uint8_t unused	: 6;
		uint8_t rts_ena	: 1; //!< Assert RTS. 
		uint8_t cts_ena	: 1; //!< Honor CTS.  
	} msBits;
	uint8_t mnWord;
} tuEFRReg;

typedef union
{
	struct
	{
		uint8_t cts_changed	: 1; //!< 
		uint8_t dsr_changed	: 1; //!< 
		uint8_t ri_changed	: 1; //!< 
		uint8_t dvd_changed	: 1; //!< 
		uint8_t cts_raw		: 1; //!< CTS
		uint8_t dsr_raw		: 1; //!< DSR
		uint8_t ri_raw		: 1; //!< RI
		uint8_t dvd_raw		: 1; //!< CD
	} msBits;
	uint8_t mnWord;
} tuMSRReg;

typedef union
{
	struct
	{
		uint8_t parity_mode	: 2; //!< Parity (5, 6, 7, 8).
		uint8_t stopbits	: 1; //!< Num Stopbits (1 or 2).
		uint8_t parity_enable	: 1; //!< Parity enable.
		uint8_t parity_odd 	: 2; //!< 0 = odd, 1 = even, 					//!< 2 = odd stick, 3 = even stick
		uint8_t reserved	: 2;
	} msBits;
	uint8_t mnWord;
} tuLCRReg;

typedef union
{
	struct
	{
		uint8_t dtr		: 1; //!< DTR
		uint8_t ctsi		: 1; //!< RTS override.
		uint8_t unused_1	: 2; 
		uint8_t loop_back	: 1; //!< loopback
		uint8_t unused_2	: 3;
	} msBits;
	uint8_t mnWord;
} tuMCRReg;

typedef union
{
	struct
	{
		uint8_t rhr_ready	: 1; //!< Receive data available.
		uint8_t txa_full	: 1; //!< Transmit queue full.
		uint8_t unused_1	: 3; 
		uint8_t txa_empty	: 1; //!< Transmit queue empty.
		uint8_t txa_empty2	: 1; //!< Transmit queue empty. (who 
			//!< knows why there are two...)
		uint8_t unused_2	: 1; 
	} msBits;
	uint8_t mnWord;
} tuLSRReg;

#endif /* FPGA_UART_H_ */
