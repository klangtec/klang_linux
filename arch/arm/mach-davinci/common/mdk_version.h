/**
 * \file version.h
 *
 * \brief Header file for the MDK version global variables.
 *
 * These variables should be available in the MityDSP namespace in 
 * the DSP Core library and the ARM / linux libdsp library for application
 * access.
 *
 *     o  0
 *     | /       Copyright (c) 2011
 *    (CL)---o   Critical Link, LLC
 *      \
 *       O
 */
#ifndef CORE_VERSION_H
#define CORE_VERSION_H


namespace MityDSP
{

//------------------------------------------------------
// Version info for the MityDSP MDK release
//------------------------------------------------------
#define MDKMAJOR 1  /* this is used to assign mdk_major in the compiled library */
#define MDKMINOR 3  /* this is used to assign mdk_major in the compiled library */
#define MDKBUILD 0  /* this is used to assign mdk_major in the compiled library */
extern int         mdk_major;        /**< MityDSP SDK major version number (i.e. x in x.y.z). */
extern int         mdk_minor;        /**< MityDSP SDK minor version number (i.e. y in x.y.z). */
extern int         mdk_build;        /**< MityDSP SDK build version number (i.e. z in x.y.z). */
extern const char *mdk_str;          /**< MityDSP SDK version as a string.  */
extern const char *mdk_date;         /**< MityDSP SDK build date.           */
extern const char *mdk_time;         /**< MityDSP SDK build time.           */
extern int         mdk_cctools;      /**< Code Generation Tools revision used to build libraries */

} // end of MityDSP namespace


#endif  // CORE_VERSION_H
