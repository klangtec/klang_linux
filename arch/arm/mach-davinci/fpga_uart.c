/*
 * fpga_uart.c: Serial driver for FPGA based UART
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2.  This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 */

#include "fpga.h"
#include "common/fpga/fpga_uart.h"
#include "common/fpga/core_ids.h"

#include <linux/module.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/tty.h>
#include <linux/delay.h>
#include <linux/clk.h>
#include <linux/cpufreq.h>
#include <asm/io.h>

#define FPGA_UART_NAME		"ttyFPGA"
#define FPGA_UART_MAJOR		203
#define FPGA_UART_MINOR		187
#define FPGA_UART_NR_UARTS	8

struct fpga_uart_port {
	struct uart_port	port;
	struct clk		*clk;
	unsigned long		baudrate;
	void			(*pm)(struct uart_port *port,
					unsigned int state,
					unsigned int old);
#ifdef CONFIG_CPU_FREQ
	struct notifier_block	freq_transition;
#endif
};

static struct fpga_uart_port fpga_uart_ports[FPGA_UART_NR_UARTS];

/* ---------------------------------------------------------------------
 * Core UART driver operations
 */
static int fpga_uart_receive(struct uart_port *port)
{
	struct tty_struct *tty = port->state->port.tty;
	unsigned char ch = 0;
	char flag = TTY_NORMAL;
	tuLSRReg LSR;
	LSR.mnWord = readb(port->membase + gnLSR_OFFSET);

	if (!LSR.msBits.rhr_ready)
		return 0;

	/* stats */
	port->icount.rx++;
	ch = readb(port->membase + gnRHR_OFFSET);

#if 0 /* TODO */
	if (stat & ULITE_STATUS_PARITY)
		port->icount.parity++;

	if (stat & ULITE_STATUS_OVERRUN)
		port->icount.overrun++;

	if (stat & ULITE_STATUS_FRAME)
		port->icount.frame++;

	/* drop byte with parity error if IGNPAR specificed */
	if (stat & port->ignore_status_mask & ULITE_STATUS_PARITY)
		stat &= ~ULITE_STATUS_RXVALID;

	stat &= port->read_status_mask;

	if (stat & ULITE_STATUS_PARITY)
		flag = TTY_PARITY;

	stat &= ~port->ignore_status_mask;

	if (stat & ULITE_STATUS_RXVALID)
#endif
		tty_insert_flip_char(tty, ch, flag);

#if 0 /* TODO */
	if (stat & ULITE_STATUS_FRAME)
		tty_insert_flip_char(tty, 0, TTY_FRAME);

	if (stat & ULITE_STATUS_OVERRUN)
		tty_insert_flip_char(tty, 0, TTY_OVERRUN);

#endif
	return 1;
}

static int fpga_uart_transmit(struct uart_port *port)
{
	struct circ_buf *xmit  = &port->state->xmit;
	tuLSRReg LSR;
	tuIERReg IER;

	LSR.mnWord = readb(port->membase + gnLSR_OFFSET);

	if (LSR.msBits.txa_full)
		return 0;

	if (port->x_char) {
		writeb(port->x_char, port->membase + gnTHR_OFFSET);
		port->icount.tx++;
		port->x_char = 0;
		return 1;
	}

	/* If we're out of data to send, we need to disable the tx empty int */
	if (uart_circ_empty(xmit) || uart_tx_stopped(port))
	{
		IER.mnWord = readb(port->membase + gnIER_OFFSET);
		IER.msBits.tx_empty_ie = 0;
		writeb(IER.mnWord, port->membase + gnIER_OFFSET);
		return 0;
	}

	writeb(xmit->buf[xmit->tail], port->membase + gnTHR_OFFSET);
	xmit->tail = (xmit->tail + 1) & (UART_XMIT_SIZE-1);
	port->icount.tx++;

	/* wake up */
	if (uart_circ_chars_pending(xmit) < WAKEUP_CHARS)
		uart_write_wakeup(port);

	return 1;
}

/**
 * Serial core request to check if the UART TX FIFO is empty.
 */
static unsigned int fpga_uart_tx_empty(struct uart_port *port)
{
	unsigned long flags;
	tuLSRReg LSR;
	spin_lock_irqsave(&port->lock, flags);
	LSR.mnWord = readb(port->membase + gnLSR_OFFSET);
	spin_unlock_irqrestore(&port->lock, flags);

	return LSR.msBits.txa_empty ? TIOCSER_TEMT : 0;
}

/**
 * Get the current MCR UART register value.
 */
static unsigned int fpga_uart_get_mctrl(struct uart_port *port)
{
	int rv = 0;
	tuMSRReg MSR;

	MSR.mnWord = readb(port->membase + gnMSR_OFFSET);

	if (MSR.msBits.cts_raw)
		rv |= TIOCM_CTS;
	if (MSR.msBits.dsr_raw)
		rv |= TIOCM_DSR;
	if (MSR.msBits.ri_raw)
		rv |= TIOCM_RI;
	if (MSR.msBits.dvd_raw)
		rv |= TIOCM_CAR;

	return rv;
}

/**
 * Sets a new value for the MCR UART register.
 */
static void fpga_uart_set_mctrl(struct uart_port *port, unsigned int mctrl)
{
	tuMCRReg MCR;

	MCR.mnWord = readb(port->membase + gnMCR_OFFSET);

	if (mctrl & TIOCM_RTS) {
	}

	if (mctrl & TIOCM_DTR)
		MCR.msBits.dtr = 1;
	else
		MCR.msBits.dtr = 0;


	if (mctrl & TIOCM_OUT1) {
	}

	if (mctrl & TIOCM_OUT2) {
	}

	if (mctrl & TIOCM_LOOP)
		MCR.msBits.loop_back = 1;
	else
		MCR.msBits.loop_back = 0;

	writeb(MCR.mnWord, port->membase + gnMCR_OFFSET);
}

/* Stop transmitting characters as soon as possible */
static void fpga_uart_stop_tx(struct uart_port *port)
{
	/* N/A */
}

/**
 * Starts the port sending data.
 */
static void fpga_uart_start_tx(struct uart_port *port)
{
	tuIERReg IER;

	/* Enable tx empty interrupt, so that start xmiting */
	IER.mnWord = readb(port->membase + gnIER_OFFSET);
	IER.msBits.tx_empty_ie = 1;
	writeb(IER.mnWord, port->membase + gnIER_OFFSET);
}

/**
 * Serial core request to stop RX.
 */
static void fpga_uart_stop_rx(struct uart_port *port)
{
	/* N/A */
}

/**
 * Serial core request to enable modem status interrupt reporting.
 */
static void fpga_uart_enable_ms(struct uart_port *port)
{
	tuIERReg IER;

	IER.mnWord = readb(port->membase + gnIER_OFFSET);
	IER.msBits.ms_ie = 1;
	writeb(IER.mnWord, port->membase + gnIER_OFFSET);
}

static void fpga_uart_break_ctl(struct uart_port *port, int ctl)
{
	/* N/A */
}

/**
 * Called once each time open() is called. Initialization goes here.
 */
static int fpga_uart_startup(struct uart_port *port)
{
	tuIERReg IER;

	/* Enable Tx and Rx interrupts */
	IER.mnWord = 0;
	IER.msBits.rhr_ready_ie = 1;
	writeb(IER.mnWord, port->membase + gnIER_OFFSET);
	
	return 0;
}

/**
 * Called once when close() is called. Cleanup goes here.
 */
static void fpga_uart_shutdown(struct uart_port *port)
{
	tuIERReg IER;
	IER.mnWord = 0;
	writeb(IER.mnWord, port->membase + gnIER_OFFSET);
}

static void fpga_uart_set_divisor(struct fpga_uart_port *fpgaport)
{
	struct clk *emif_clk;
	unsigned long clk_rate;
	unsigned long brgn;

	emif_clk = clk_get(NULL, "aemif");
	if (IS_ERR(emif_clk))
		/* This should never happen, but default to 100 MHz */
		clk_rate = 100000000;
	else
		clk_rate = clk_get_rate(emif_clk);

	brgn = clk_rate / fpgaport->baudrate;

	/* Set the baud counter hi and low registers */
	writeb(brgn & 0xff, fpgaport->port.membase + gnDLL_OFFSET);
	writeb((brgn >> 8) & 0xff, fpgaport->port.membase + gnDLH_OFFSET);
}

static void fpga_uart_set_termios(struct uart_port *port, 
	struct ktermios *termios, struct ktermios *old)
{
	unsigned long flags;
/*	unsigned int quot;*/
	tuLCRReg luLCRReg;
	tuEFRReg luEFRReg;
	struct fpga_uart_port *fpgaport;

	spin_lock_irqsave(&port->lock, flags);

	/* Zero out LCD reg data */
	luLCRReg.mnWord = 0;
	/* Zero out EFR reg data */
	luEFRReg.mnWord = 0;

	switch(termios->c_cflag & CSIZE) {
	case CS5:
		luLCRReg.msBits.parity_mode = 0;
	case CS6:
		luLCRReg.msBits.parity_mode = 1;
	case CS7:
		luLCRReg.msBits.parity_mode = 2;
	case CS8:
		luLCRReg.msBits.parity_mode = 3;
	default:
		break;
	}

	if ((termios->c_cflag & CRTSCTS) || (!(termios->c_cflag & CLOCAL))) {
		luEFRReg.msBits.rts_ena = 1;
		luEFRReg.msBits.cts_ena = 1;
	}

	if (termios->c_cflag & CSTOPB) {
		luLCRReg.msBits.stopbits = 1;
	}

	if (termios->c_cflag & PARENB) {
		luLCRReg.msBits.parity_enable = 1;

		if (termios->c_cflag & PARODD) {
			/* Check parity stick*/
			if (termios->c_cflag & CMSPAR) {
				luLCRReg.msBits.parity_odd = 2;
			} else {
				luLCRReg.msBits.parity_odd = 0;
			}
		} else {
			/* Check parity stick*/
			if (termios->c_cflag & CMSPAR) {
				luLCRReg.msBits.parity_odd = 3;
			} else {
				luLCRReg.msBits.parity_odd = 1;
			}
		}
	}

	/* Set LCR Reg data */
	writeb(luLCRReg.mnWord, port->membase + gnLCR_OFFSET);
	/* Set EFR Reg data */
	writeb(luEFRReg.mnWord, port->membase + gnEFR_OFFSET);

	/* Get the baud rate */
	fpgaport = container_of(port, struct fpga_uart_port, port);
	fpgaport->baudrate = tty_termios_baud_rate(termios);	
	fpga_uart_set_divisor(fpgaport);

	/* use the UART core to compute the divisor */
/*
	baud = uart_get_baud_rate(port, termios, old,
				  port->uartclk / 0xffffff,
				  port->uartclk);

	quot = uart_get_divisor(port, baud);
*/
	/* set it here*/

	spin_unlock_irqrestore(&port->lock, flags);
}

static const char *fpga_uart_type(struct uart_port *port)
{
	return "fpga_uart";
}

/**
 * Used to return memory and other hardware resources for this serial port.
 */
static void fpga_uart_release_port(struct uart_port *port)
{
	/* Normally, you would release the iomapped memory here, but the 
	   base fpga_ctrl module already has the mapping made... */
	port->membase = NULL;
}

/**
 * Used to reserve memory and other hardware resources for this serial port.
 */
static int fpga_uart_request_port(struct uart_port *port)
{
	/* Normally, you would map the io memory here, but the 
	   base fpga_ctrl module already has the mapping made... */
	port->membase = (void*)port->mapbase;

	return 0;
}

/**
 * Same as fpga_uart_request_port() except it is called when hardware
 * can autoprobe for any serial ports connected to it.
 */
static void fpga_uart_config_port(struct uart_port *port, int flags)
{
	/* Make sure the port is requested (membase set appropriately) */	
	fpga_uart_request_port(port);
	/* Give the port a "valid" type */
	port->type = PORT_FPGAUART;
}

static int fpga_uart_verify_port(struct uart_port *port, struct serial_struct *ser)
{
	/* we don't want the core code to modify any port params */
	return -EINVAL;
}

static struct uart_ops fpga_uart_ops = {
	.tx_empty	= fpga_uart_tx_empty,
	.set_mctrl	= fpga_uart_set_mctrl,
	.get_mctrl	= fpga_uart_get_mctrl,
	.stop_tx	= fpga_uart_stop_tx,
	.start_tx	= fpga_uart_start_tx,
	.stop_rx	= fpga_uart_stop_rx,
	.enable_ms	= fpga_uart_enable_ms,
	.break_ctl	= fpga_uart_break_ctl,
	.startup	= fpga_uart_startup,
	.shutdown	= fpga_uart_shutdown,
	.set_termios	= fpga_uart_set_termios,
	.type		= fpga_uart_type,
	.release_port	= fpga_uart_release_port,
	.request_port	= fpga_uart_request_port,
	.config_port	= fpga_uart_config_port,
	.verify_port	= fpga_uart_verify_port
};

static struct uart_driver fpga_uart_uart_driver = {
	.owner		= THIS_MODULE,
	.driver_name	= "fpga_uart",
	.dev_name	= FPGA_UART_NAME,
	.major		= FPGA_UART_MAJOR,
	.minor		= FPGA_UART_MINOR,
	.nr		= FPGA_UART_NR_UARTS,
};

#ifdef CONFIG_CPU_FREQ
/* Cpufreq transition notifier callback */
static int fpga_uart_cpufreq(struct notifier_block *nb, unsigned long val,
				void *data)
{
	unsigned long flags;
	struct fpga_uart_port *fpgaport;
	fpgaport = container_of(nb, struct fpga_uart_port, freq_transition);

	if (val == CPUFREQ_POSTCHANGE && fpgaport->baudrate != 0) {
		spin_lock_irqsave(&fpgaport->port.lock, flags);
		fpga_uart_set_divisor(fpgaport);
		spin_unlock_irqrestore(&fpgaport->port.lock, flags);
	}

	return 0;
}
#endif

/** fpga_uart_assign: register a fpga_uart device with the driver
 *
 * @dev: pointer to device structure
 * @id: requested id number.  Pass -1 for automatic port assignment
 *
 * Returns: 0 on success, <0 otherwise
 */
static int __devinit fpga_uart_assign(struct device *dev, int id)
{
	struct fpga_device* fpgadev = to_fpga_device(dev);
	struct uart_port *port;
	int rc;

	/* if id = -1; then scan for a free id and use that */
	if (id < 0) {
		for (id = 0; id < FPGA_UART_NR_UARTS; id++)
			if (fpga_uart_ports[id].port.mapbase == 0)
				break;
	}
	if (id < 0 || id >= FPGA_UART_NR_UARTS) {
		dev_err(dev, "%s%i too large\n", FPGA_UART_NAME, id);
		return -EINVAL;
	}

	if ((fpga_uart_ports[id].port.mapbase) && 
		(fpga_uart_ports[id].port.mapbase != (int)fpgadev->baseaddr)) {
		dev_err(dev, "cannot assign to %s%i; it is already in use\n",
			FPGA_UART_NAME, id);
		return -EBUSY;
	}

	port = &fpga_uart_ports[id].port;

	spin_lock_init(&port->lock);
	port->fifosize = 64;
	port->regshift = 2;
	port->iotype = UPIO_MEM; // UPIO_PORT //serial_in, serial_out
	port->iobase = 1; /* mark port in use */
	port->mapbase = (unsigned int)fpgadev->baseaddr;
	port->ops = &fpga_uart_ops;
//	port->irq = 
	port->flags = UPF_BOOT_AUTOCONF;
	port->dev = dev;
	port->type = PORT_UNKNOWN;
	port->line = id;
//	port->uartclk = 
	// Set timeout (for checking succesive tx_empty, and maybe other 
	// things?) to 1/10 sec(in jiffies)
	port->timeout = HZ/10;

	dev_set_drvdata(dev, port);

	/* Register the port */
	rc = uart_add_one_port(&fpga_uart_uart_driver, port);
	if (rc) {
		dev_err(dev, "uart_add_one_port() failed; err=%i\n", rc);
		port->mapbase = 0;
		dev_set_drvdata(dev, NULL);
		return rc;
	}

#ifdef CONFIG_CPU_FREQ
	/* Register cpufreq transition notifier */
	fpga_uart_ports[id].freq_transition.notifier_call = fpga_uart_cpufreq;
	if (cpufreq_register_notifier(&fpga_uart_ports[id].freq_transition,
					CPUFREQ_TRANSITION_NOTIFIER))
		dev_warn(dev, "failed to register cpufreq notifier.\n");
#endif

	return 0;
}

/**
 * IRQ handler called when an GPIO core is asserting an interrupt
 * condition.  This method is called within the context of an ISR, and
 * must be atomic.
 *
 * \param[in] dev the device issuing the interrupt.
 * \return 0
 */
static int fpga_uart_IrqHandler(struct fpga_device* dev)
{
	struct uart_port* port = dev_get_drvdata(&dev->dev);
	unsigned short   *lpBaseReg;
	unsigned long    flags;
	int busy, n = 0;

	spin_lock_irqsave(&port->lock, flags);

	lpBaseReg = (unsigned short*)dev->baseaddr;

	do {
		busy = fpga_uart_receive(port);
		busy |= fpga_uart_transmit(port);
		/* TODO MSR */
		n++;
	} while (busy);

	if (n > 1) 
		tty_flip_buffer_push(port->state->port.tty);

	spin_unlock_irqrestore(&port->lock, flags);

	return 0;
}

static int fpga_uart_probe(struct device*);
static int fpga_uart_remove(struct device*);

static struct fpga_driver fpga_uart_driver = {
	.id 		= CORE_ID_UART, /** must match value in core_ids.h */
	.version	= "UART Driver 1.0",
	.IrqHandler 	= fpga_uart_IrqHandler,
	.probe 		= fpga_uart_probe,
	.remove 	= fpga_uart_remove,
	.driver 	= {
		.name 	= "fpga_uart",
		.owner 	= THIS_MODULE,
	},
};

static int fpga_uart_remove(struct device* dev)
{
	int rv = 0;
	struct fpga_uart_port *port;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	port = dev_get_drvdata(dev);

	if (port) {
#ifdef CONFIG_CPU_FREQ
		cpufreq_unregister_notifier(&port->freq_transition,
						CPUFREQ_TRANSITION_NOTIFIER);
#endif
		rv = uart_remove_one_port(&fpga_uart_uart_driver, &port->port);
		dev_set_drvdata(dev, NULL);
		port->port.mapbase = 0;
	}

	/* kill off IRQ mask */
	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
			fpgadev->coreversion.ver0.bits.vector,
			0);
	return rv;
}

/**
 * The fpga_uart_probe routine is called after the gpio driver is successfully
 * matched to an FPGA core with the same core ID.
 *
 * \param[in] dev device within an fpga_device structure.
 * return 0 on successful probe / initialization.
 */
static int fpga_uart_probe(struct device *dev)
{
	int rv = 0;
	struct fpga_device* fpgadev = to_fpga_device(dev);

	dev_dbg(dev, "%s entered\n", __func__);

	rv = fpga_uart_assign(dev, -1);

	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
			       fpgadev->coreversion.ver0.bits.vector,
			       1);

	return rv;
}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init fpga_uart_init(void)
{
	int ret;

	ret = uart_register_driver(&fpga_uart_uart_driver);
	if (ret)
		goto err_uart;

	ret = register_fpga_driver(&fpga_uart_driver);
	if (ret)
		goto err_fpga;

	return 0;

err_fpga:
	uart_unregister_driver(&fpga_uart_uart_driver);

err_uart:
	return -1;
}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void fpga_uart_exit(void)
{
	driver_unregister(&fpga_uart_driver.driver);
	uart_unregister_driver(&fpga_uart_uart_driver);
}

module_init(fpga_uart_init);
module_exit(fpga_uart_exit);

MODULE_AUTHOR("Michael Williamson <michael.williamson@criticallink.com>");
MODULE_DESCRIPTION("Critical Link FPGA based UART driver");
MODULE_LICENSE("GPL");
 
