
/* #define DEBUG */

#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/mm.h>
#include <linux/ptrace.h>
#include <linux/module.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include "fpga.h"
#define FPGA_CTRL_C
#include "common/fpga/core_ids.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Williamson <michael.williamson@criticallink.com");
MODULE_DESCRIPTION("Bus level and Base Module driver for MityDSP-L138 FPGA interface");

int num_fpga_cores = 16;
module_param (num_fpga_cores, int, S_IRUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC (num_fpga_cores, "The maximum number of FPGA cores in the "
	"Chip Select space (i.e. the number of cores to iterate over when the "
	"FPGA is loaded. This should be set based on DECODE_BITS generic in "
	"FPGA (default 16)");

#define FPGA_PROGRAM	GPIO_TO_PIN(6, 15)
// MityDSP L138F:
#define FPGA_INIT   	GPIO_TO_PIN(1, 15)
// KLANG:technologies external FPGA:
//#define FPGA_INIT   	GPIO_TO_PIN(0, 8)

#define FPGA_RDWR   	GPIO_TO_PIN(3, 9)
#define FPGA_INT0       GPIO_TO_PIN(6, 12)
#define FPGA_INT1       GPIO_TO_PIN(6, 13)

/** These are word offsets (2 byte offsets) for the base module */
const int gnINT0_VECTOR_OFFSET = 1;
const int gnINT0_MASK_OFFSET   = 2;
const int gnINT1_VECTOR_OFFSET = 3;
const int gnINT1_MASK_OFFSET   = 4;
const int gnBM_VERSION_OFFSET  = 6;
const int gnDEVDNA_OFFSET      = 8;

/**
 *  Reads the core version information out of a spot in the 
 *  FPGA.  
 *
 *  \param[in] baseaddr location of the core version register
 *  \param[in] pdata location to store the core version data
 *
 *  \return non-zero if the core data is invalid
 */
int read_core_version(void* baseaddr, struct coreversion* pdata)
{
    int      i;
    corever0 ver;
    int      found = 0;
    int      rv = -1;
    
    for (i = 0; i < 4; i++)
    {
        ver.word = ioread16(baseaddr);
        switch(ver.bits.FIFO_no)
        {
        case 0:
            pdata->ver0.word = ver.word;
            break;
        case 1:
            pdata->ver1.word = ver.word;
            break;
        case 2:
            pdata->ver2.word = ver.word;
            break;
        case 3:
            pdata->ver3.word = ver.word;
            break;
        }
        found |= (1<<ver.bits.FIFO_no);
    }
    if (found == 0x0F)
        rv = 0;
    return rv;
}
EXPORT_SYMBOL(read_core_version);

/**
 *   This routine is called by the drive framework whenever a new 
 *   device (or a new driver) is registered on the fpga_bus.  The
 *   return value should be non-zero if the provided driver can 
 *   control the provided device.
 *
 *   For the MityDSP-L138, our matching criteria should be based
 *   on the core base ID.
 *
 *   \param devices pointer to the device that needs a driver...
 *   \param driver pointer to a registered driver that should be tested
 *   \return non-zero when a driver is capable of handling the device
 */
static int fpga_bus_match(struct device *device, struct device_driver *driver)
{
	struct fpga_device* fpgadev;
	struct fpga_driver* fpgadriver;

	fpgadev = to_fpga_device(device);
	fpgadriver = to_fpga_driver(driver);
	dev_dbg(device, "Trying to Match ID %d with %d\n",
		fpgadev->coreversion.ver0.bits.core_id,
		fpgadriver->id);
	return (fpgadev->coreversion.ver0.bits.core_id == fpgadriver->id);
}

/**
 *   The device framework uses this callback to let the bus driver handle device
 *   probes, set up device struct fields, etc. If called, the driver probe
 *   callback will not be called, so the bus driver must also dispatch that call.
 */
static int fpga_bus_probe(struct device *dev)
{
	struct fpga_device *fpgadev = to_fpga_device(dev);
	fpgadev->drv = to_fpga_driver(dev->driver);
	if(fpgadev->drv->probe)
		return fpgadev->drv->probe(dev);
	return 0;
}

static int fpga_bus_remove(struct device *dev)
{
	struct fpga_device *fpgadev = to_fpga_device(dev);
	if(fpgadev->drv->remove)
		return fpgadev->drv->remove(dev);
	return 0;
}

/**
 *  This structure describes the fpga "bus" class type.
 */
struct bus_type fpga_bus_type = {
	.name	= "fpga",
	.match	= fpga_bus_match,
	.probe	= fpga_bus_probe,
	.remove	= fpga_bus_remove,
};
EXPORT_SYMBOL(fpga_bus_type);

static void fpga_release(struct device *dev)
{
}

/**
 *  This structure is the FPGA bus controller.  It includes the top level
 *  fpga device information as well as the driver used to program the FPGA.
 *  This is a bit of a special class.
 */
struct fpga_ctrl {
	unsigned int         state;          /* The FPGA state, see fpga.h */
	unsigned char        handle_ints[2]; /* INT0 and INT1 assignments to ARM */
	struct device        fpga;           /* The FPGA device proper */
	struct device_driver fpga_drvr;      /* The FPGA controller driver */
	struct resource*     mem_region;     /* The FPGA base address region */
	void*                baseaddr;       /* the FPGA base address memory map */
	struct coreversion   bm_version;     /* base module core version data */
	struct coreversion   app_version;    /* FPGA application core version data */
	long long            fpga_dna;       /* FPGA unique DNA code */
	int                  major;          /* FPGA devnode major number */
};

struct fpga_ctrl fpga_ctrl = {
	.state	= 0,
	.fpga	= {
		.bus 		= &fpga_bus_type,
		.init_name	= "fpga_ctrl",
		.release 	= fpga_release
	},
	.fpga_drvr = {
		.name = "fpga_ctrl"
	}
};

/**
 *  Retrieve human readable core description.
 *
 *  \param[in] ID core number
 *  \return string description of core
 */
const char* CoreName(unsigned char ID)
{
	int i = 0;
	for (i = 0; i < ARRAY_SIZE(KnownCores); i++)
	{
		if (ID == KnownCores[i].ID)
		{
			return KnownCores[i].Name;
		}
	}
	return "Unknown";
}

static void fpga_dev_release(struct device *dev)
{
	dev_dbg(dev, "Releasing\n");
	/* Note: This only works if the struct device member is first in the
	 *	 declaration of struct fpga_device. dev *must* be the first
	 *	 field in struct fpga_device. */
	kfree(dev);
}

/**
 *   This method should be called by the fpga_ctrl (the "bus" controller)
 *   once for each core located in the FPGA.
 */
int register_fpga_device(struct fpga_device *fpgadev)
{
	fpgadev->dev.bus = &fpga_bus_type;
	fpgadev->dev.parent = &fpga_ctrl.fpga;
	fpgadev->dev.release = fpga_dev_release;
	fpgadev->dev.devt = MKDEV(fpga_ctrl.major, fpgadev->coreindex);
	return device_register(&fpgadev->dev);
}


/**
 *  User-space mmap/char device hooks
 */
static loff_t fpgachr_llseek(struct file *filp, loff_t offset, int orig)
{
	loff_t ret;

	mutex_lock(&filp->f_path.dentry->d_inode->i_mutex);
	switch (orig) {
		case 0:
			filp->f_pos = offset;
			ret = filp->f_pos;
			force_successful_syscall_return();
			break;
		case 1:
			filp->f_pos += offset;
			ret = filp->f_pos;
			force_successful_syscall_return();
			break;
		default:
			ret = -EINVAL;
	}
	mutex_unlock(&filp->f_path.dentry->d_inode->i_mutex);
	return ret;
}

static ssize_t fpgachr_read(struct file *filp, char __user *buf, size_t count,
				loff_t *ppos)
{
	size_t pos = *ppos;
	int core = iminor(filp->f_path.dentry->d_inode);
	char *p = (char *) fpga_ctrl.baseaddr + FPGA_CORE_SIZE * core + pos;
	size_t cnt;
	size_t remaining = FPGA_CORE_SIZE - pos;

	if (pos > FPGA_CORE_SIZE)
		return -EFAULT;

	cnt = min(count, remaining);

	if (copy_to_user(buf, p, cnt))
		return -EFAULT;

	*ppos += cnt;
	return cnt;
}

static ssize_t fpgachr_write(struct file *filp, const char __user *buf,
				size_t count, loff_t *ppos)
{
	size_t pos = *ppos;
	int core = iminor(filp->f_path.dentry->d_inode);
	char *p = (char *) fpga_ctrl.baseaddr + FPGA_CORE_SIZE * core + pos;
	size_t cnt;
	size_t remaining = FPGA_CORE_SIZE - pos;

	if (pos > FPGA_CORE_SIZE)
		return -EFAULT;

	cnt = min(count, remaining);

	if (copy_from_user(p, buf, cnt))
		return -EFAULT;

	*ppos += cnt;
	return cnt;
}

// KLANG [fabian] VM_RESERVED deprecated since 3.7
#ifndef VM_RESERVED
# define VM_RESERVED (VM_DONTEXPAND | VM_DONTDUMP)
#endif

/* We can't really mmap a single core since it's smaller than the page size.
   An attempt to mmap any of the dev files will map the entire FPGA address
   space. */
static int fpgachr_mmap(struct file *filp, struct vm_area_struct *vma)
{
	size_t size = vma->vm_end - vma->vm_start;
	unsigned long pfn = FPGA_BASE_ADDR >> PAGE_SHIFT;

	vma->vm_flags |= VM_IO | VM_RESERVED;
	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);

	if (io_remap_pfn_range(vma, vma->vm_start, pfn, size,
				vma->vm_page_prot))
		return -EAGAIN;

	return 0;
}

static int fpgachr_open(struct inode *inode, struct file *filp)
{
	int minor = iminor(inode);
	if(minor >= num_fpga_cores)
		return -ENXIO;

	return 0;
}

static const struct file_operations fpgachr_fops = {
	.llseek			= fpgachr_llseek,
	.read			= fpgachr_read,
	.write			= fpgachr_write,
	.mmap			= fpgachr_mmap,
	.open			= fpgachr_open,
};


/**
 * This method is called after the FPGA has been programmed.  It will
 * identify all of the cores on CS5 and register the devices for them.
 * Drivers with appropriate core ID controllers (which match) will be
 * automatically assigned to them as that are registered.
 *
 * \param[in] fpgactrl pointer to our base module controller.
 * \return 0 on success.
 */
int fpga_ctrl_enum_devices(struct fpga_ctrl* fpgactrl)
{
	int i;
	struct coreversion cv;

	dev_dbg(&fpgactrl->fpga, "Entering fpga_ctrl_enum_devices\n");

	for (i = 0; i < num_fpga_cores; i++)
	{
		void* baseaddr = (void*)((char*)(fpgactrl->baseaddr)+FPGA_CORE_SIZE*i);
		if (0 == read_core_version(baseaddr,&cv))
		{
			struct fpga_device* fpgadev;
			int ret;

			dev_info(&fpgactrl->fpga, "Found Device ID %02d-%s (%02d.%02d) at %08X\n",
					cv.ver0.bits.core_id,
					CoreName(cv.ver0.bits.core_id),
					cv.ver1.bits.major, cv.ver1.bits.minor,
					                                (unsigned int)baseaddr);
					                                
			/* If not assigned to an interrupt line we are managing, then
			* don't enumerate */
			if (!fpgactrl->handle_ints[cv.ver0.bits.level]) 
			{
				dev_dbg(&fpgactrl->fpga, "Device ID %02d-%s (%02d.%02d) at %08X -- managed by DSP\n",
					cv.ver0.bits.core_id,
					CoreName(cv.ver0.bits.core_id),
					cv.ver1.bits.major, cv.ver1.bits.minor,
					                                (unsigned int)baseaddr);
				continue;
			}
			
			fpgadev = (struct fpga_device*)kmalloc(sizeof(struct fpga_device), GFP_KERNEL);

			memset(fpgadev, 0, sizeof(struct fpga_device));

			fpgadev->baseaddr = baseaddr;
			fpgadev->coreversion = cv;
			fpgadev->coreindex = i;
			fpgadev->dev.init_name = kmalloc(32, GFP_KERNEL);
			snprintf((char*)&fpgadev->dev.init_name[0], 32, "%d", i);
			ret = register_fpga_device(fpgadev);
			if (ret)
				dev_warn(&fpgactrl->fpga, "Could not register device %d\n", ret);
		}
	}
	return 0;
}

ssize_t fpga_ctrl_show_version(struct device *dev, struct device_attribute *attr,
		                       char* buf)
{
	ssize_t rv = 0;
	struct fpga_ctrl* fpgactrl = container_of(dev, struct fpga_ctrl, fpga);
	switch(fpgactrl->state)
	{
    case FPGA_STATE_PROGRAMMED:
    	rv += snprintf(&buf[rv], PAGE_SIZE-rv, "PROGRAMMED\n");
    	rv += snprintf(&buf[rv], PAGE_SIZE-rv, "FPGA Version        : %02d.%02d\n", fpgactrl->app_version.ver1.bits.major,
    			fpgactrl->app_version.ver1.bits.minor);
    	rv += snprintf(&buf[rv], PAGE_SIZE-rv, "FPGA Date           : %04d-%02d-%02d\n",
			FPGA_BASEYEAR+fpgactrl->app_version.ver1.bits.year,
    			fpgactrl->app_version.ver2.bits.month,
    			fpgactrl->app_version.ver2.bits.day);

    	rv += snprintf(&buf[rv], PAGE_SIZE-rv, "Base Module Version : %02d.%02d\n", fpgactrl->bm_version.ver1.bits.major,
    			fpgactrl->bm_version.ver1.bits.minor);
    	rv += snprintf(&buf[rv], PAGE_SIZE-rv, "Base Module Date    : %04d-%02d-%02d\n",
			FPGA_BASEYEAR+fpgactrl->bm_version.ver1.bits.year,
    			fpgactrl->bm_version.ver2.bits.month,
    			fpgactrl->bm_version.ver2.bits.day);
    	break;
    case FPGA_STATE_RESET:
    case FPGA_STATE_PROGRAMMING:
    case FPGA_STATE_PROGRAM_FAIL:
    case FPGA_STATE_UNKNOWN:
    default:
    	rv += snprintf(&buf[rv], PAGE_SIZE-rv, "NOT PROGRAMMED\n");
    	break;
	}
	return rv;
}
DEVICE_ATTR(version, S_IRUGO, fpga_ctrl_show_version, NULL);

/**
 *  Display the FPGA device state in sysfs
 */
ssize_t fpga_ctrl_show_state(struct device *dev, struct device_attribute *attr,
			                 char *buf)
{
    char* desc;
    struct fpga_ctrl* fpgactrl = container_of(dev, struct fpga_ctrl, fpga);
    switch(fpgactrl->state)
    {
    case FPGA_STATE_RESET:        
        desc = "RESET";
        break;
    case FPGA_STATE_PROGRAMMING:
        desc = "PROGRAMMING";
        break;
    case FPGA_STATE_PROGRAM_FAIL:
        desc = "PROGRAM_FAIL";
        break;
    case FPGA_STATE_PROGRAMMED:
        desc = "PROGRAMMED";
        break;
    case FPGA_STATE_UNKNOWN:
    default:
        desc = "UNKNOWN";
        break;
    }
    return snprintf(buf, PAGE_SIZE, "%d : %s\n",fpgactrl->state,desc);
}
DEVICE_ATTR(state, S_IRUGO, fpga_ctrl_show_state, NULL); 


/**
 *  Display the FPGA device state in sysfs
 */
ssize_t fpga_ctrl_show_int_status(struct device *dev, struct device_attribute *attr,
			                 char *buf)
{
	ssize_t rv = 0;
	unsigned short* lpBaseReg;
	unsigned short vector, mask, i;
	intass int_assign; 
    struct fpga_ctrl* fpgactrl = container_of(dev, struct fpga_ctrl, fpga);

    lpBaseReg = (unsigned short*)fpgactrl->baseaddr;

    if (fpgactrl->state == FPGA_STATE_PROGRAMMED)
    {
		int_assign.word = lpBaseReg[BM_INT_ASSIGNMENTS_OFFSET];
    	mask = lpBaseReg[gnINT0_MASK_OFFSET];
    	vector = lpBaseReg[gnINT0_VECTOR_OFFSET];
    	
    	if (fpgactrl->handle_ints[0])
    	{	 
			rv += snprintf(&buf[rv], PAGE_SIZE-rv, "INT0 - %d\n", gpio_get_value(FPGA_INT0));
			rv += snprintf(&buf[rv], PAGE_SIZE-rv, "   Vector(%02X)   Mask(%02X)   Pending\n", vector, mask);
			for (i = 0; i < 16; i++)
			{
				rv += snprintf(&buf[rv], PAGE_SIZE-rv, "   %8d   %5d             %d\n",
						i, (mask&(1<<i))? 1 : 0, (vector&(1<<i)) ? 1 : 0);
			}
		}
		else
		{
			rv += snprintf(&buf[rv], PAGE_SIZE-rv, "INT0 - managed by DSP\n");
		}
		
    	if (fpgactrl->handle_ints[1])
    	{	 
			mask = lpBaseReg[gnINT1_MASK_OFFSET];
			vector = lpBaseReg[gnINT1_VECTOR_OFFSET];
			rv += snprintf(&buf[rv], PAGE_SIZE-rv, "INT1 - %d\n", gpio_get_value(FPGA_INT1));
			rv += snprintf(&buf[rv], PAGE_SIZE-rv, "   Vector(%02X)   Mask(%02X)   Pending\n", vector, mask);
			for (i = 0; i < 16; i++)
			{
				rv += snprintf(&buf[rv], PAGE_SIZE-rv, "   %8d   %5d             %d\n",
						i, (mask&(1<<i))? 1 : 0, (vector&(1<<i)) ? 1 : 0);
			}
		}
		else
		{
			rv += snprintf(&buf[rv], PAGE_SIZE-rv, "INT1 - managed by DSP\n");
		}
    }
    else
    {
    	rv = snprintf(buf, PAGE_SIZE, "NOT PROGRAMMED\n");
    }
    return rv;
}
DEVICE_ATTR(int_status, S_IRUGO, fpga_ctrl_show_int_status, NULL);

/**
 *  Display the FPGA device DNA code in sysfs
 */
ssize_t fpga_ctrl_show_device_dna(struct device *dev, struct device_attribute *attr,
					char *buf)
{
	ssize_t rv = 0;
	unsigned short *base_reg;
	struct fpga_ctrl *fpgactrl = container_of(dev, struct fpga_ctrl, fpga);
	int i;

	base_reg = (unsigned short *) fpgactrl->baseaddr;

	for(i = 3; i >= 0; i--) {
		unsigned short dna = base_reg[gnDEVDNA_OFFSET + i];
		rv += snprintf(&buf[rv], PAGE_SIZE-rv, "%.4hx", dna);
	}

	rv += snprintf(&buf[rv], PAGE_SIZE-rv, "\n");
	return rv;
}
DEVICE_ATTR(device_dna, S_IRUGO, fpga_ctrl_show_device_dna, NULL);


static int fptr_ctrl_match_device(struct device *dev, void* id)
{
    int level  = *((int*)id);
    int vector = *((int*)id+1);
    struct fpga_device* fpgadevice = to_fpga_device(dev);
    if (fpgadevice->coreversion.ver0.bits.core_id == CORE_ID_BASEMODULE)
    {
    	return 0;
    }
    return ( (fpgadevice->coreversion.ver0.bits.vector == vector) &&
             (fpgadevice->coreversion.ver0.bits.level  == level) );
}

/**
 *   Top level Interrupt Handler for FPGA generated interrupts meant for
 *   the ARM / linux processor.
 */
static irqreturn_t fpga_ctrl_irq_router(int irq, void *dev)
{
    uint16_t isr_mask;
    short    isr_count[16];
    int      isr_data[2];
    uint16_t* mask_addr = (uint16_t*)fpga_ctrl.baseaddr;
    
	if (irq == gpio_to_irq(FPGA_INT0))
	{
		mask_addr = &mask_addr[gnINT0_VECTOR_OFFSET];
		isr_data[0] = 0;
	}
	else if (irq == gpio_to_irq(FPGA_INT1))
	{
		mask_addr = &mask_addr[gnINT1_VECTOR_OFFSET];
		isr_data[0] = 1;
	}
	else
	{
		return IRQ_HANDLED;
	}


    memset(isr_count, 0, sizeof(short)*16);

    /* while interrupts are pending */
    isr_mask = ioread16(mask_addr);

    // dev_dbg(&fpga_ctrl.fpga, "fpga_ctrl_irq_router: isr_mask is %X\n", isr_mask);

    while(isr_mask)
    {
        int vec, vecmask;
        for (vec = 0, vecmask = 1; vec < 16; vec++, vecmask <<= 1)
        {
            if (isr_mask & vecmask)
            {
                struct device* matchdev;
            	isr_count[vec]++;
                /* check if there is a core registered at that position
                   and call it's ISR handler
                   TODO - We can make this more efficient by noting the
                   device / driver positions when interrupts are enabled...
                 */
                isr_data[1] = vec;
                matchdev = bus_find_device(&fpga_bus_type, NULL, isr_data,
                                           fptr_ctrl_match_device);
                /* if we found one... */
                if (matchdev)
                {
                    struct fpga_device* fpgadevice = to_fpga_device(matchdev);
                    if (fpgadevice && fpgadevice->drv && fpgadevice->drv->IrqHandler)
                    {
	                    fpgadevice->drv->IrqHandler(fpgadevice);
                    }
                    else
                    {
//                    	dev_err(&fpga_ctrl.fpga, "Could not dereference IrqHandler! "
//				"%X:%X:%X -- %p %p\n",
//				fpgadevice->coreversion.ver0.bits.core_id,
//				fpgadevice->coreversion.ver0.bits.vector,
//				fpgadevice->coreversion.ver0.bits.level,
//				fpgadevice->drv,
//				fpgadevice->drv->IrqHandler);
			dev_err(&fpga_ctrl.fpga, "Could not dereference IrqHandler!");
                    }
                }
                else
                {
                    /* unhandled interrupt, shut it down... */
                	enable_irq_vec(isr_data[0],vec,0);
                	dev_err(&fpga_ctrl.fpga, "Shutting down unhandled %d,%d!\n", 0, vec);
                }
		/* if the interrupt count is too high (we're stuck!) then
		   shut down that interrupt source */
		if (isr_count[vec] > 256)
		{
		   /* Spurious interrupt, shut it down... */
		   enable_irq_vec(isr_data[0],vec,0);
		   dev_err(&fpga_ctrl.fpga, "Shutting down spurious %d,%d!\n", 0, vec);
		}
            }
        }
        // go get the state of the mask again
        isr_mask = ioread16(mask_addr);
    }
    return IRQ_HANDLED;
}

/**
 * Utility routine for cleaning up all devices registered on the FPGA bus
 */
int device_get(struct device* dev, void* rv)
{
	struct device **lpDev = (struct device **)rv;
	*lpDev = dev;
	return 1;
}

int device_get_no_ctrl(struct device* dev, void* rv)
{
	struct device **lpDev = (struct device **)rv;
	if (dev != &fpga_ctrl.fpga) {
		*lpDev = dev;
		return 1;
	} else 
		return 0;
}

/**
 *  Handle a device command from user space.  Primarily to coordinate
 *  programming.
 */
ssize_t fpga_ctrl_store_cmd(struct device *dev, struct device_attribute *attr,
			                const char *buf, size_t count)
{
    int     tmp;
    ssize_t rv;
    unsigned int cmd;
    intass  int_assign;
    unsigned short *ps;
    
    struct fpga_ctrl* fpgactrl = container_of(dev, struct fpga_ctrl, fpga);
    unsigned short* lpBaseReg = (unsigned short*)fpgactrl->baseaddr;
    rv = sscanf(buf, "%d", &cmd);
    switch(cmd)
    {
    case FPGA_CMD_RESET:
		/* first release the IRQ as the FPGA is about to get whacked */
		if (fpgactrl->state == FPGA_STATE_PROGRAMMED)
		{
			if (fpgactrl->handle_ints[0])
			{
				tmp = gpio_to_irq(FPGA_INT0);
				if (tmp < 0)
				{
					dev_err( &fpga_ctrl.fpga, "Unable to convert GPIO INT0 setting to IRQ setting\n");
				}
				free_irq(tmp, &fpga_ctrl);
			}
			if (fpgactrl->handle_ints[1])
			{
				tmp = gpio_to_irq(FPGA_INT1);
				if (tmp < 0)
				{
					dev_err( &fpga_ctrl.fpga, "Unable to convert GPIO INT1 setting to IRQ setting\n");
				}
				free_irq(tmp, &fpga_ctrl);
			}
			fpgactrl->handle_ints[0] = fpgactrl->handle_ints[1] = 0;
			/* remove all bus devices */
			do
			{
				dev = NULL;
				bus_for_each_dev(&fpga_bus_type, NULL, &dev, device_get_no_ctrl);
				if (NULL != dev)
				{
					device_unregister(dev);
				}
			} while(dev != NULL);
    		}
		/* Pull the Program Pin Low */
		gpio_direction_output(FPGA_PROGRAM, 0);
		fpgactrl->state = FPGA_STATE_RESET;
		break;
    case FPGA_CMD_PROGRAM:
        /* Release the Program Pin */
        gpio_direction_output(FPGA_PROGRAM, 1);
        /* Set the RDWR pin to write */
        gpio_direction_output(FPGA_RDWR, 0);
    	fpgactrl->state = FPGA_STATE_PROGRAMMING;
        break;
    case FPGA_CMD_FINISHPROGRAM:
        /* Release RDWR */
        gpio_direction_output(FPGA_RDWR, 1);
        /* Read the base module version register */
        tmp = read_core_version(lpBaseReg + BM_FPGA_VERSION_OFFSET, &fpgactrl->app_version);
        if (!tmp) /* success */
        {
        	tmp = read_core_version(lpBaseReg, &fpgactrl->bm_version);

        	fpgactrl->state = FPGA_STATE_PROGRAMMED;
        	
        	/* before enumerating devices, we need to determine which 
        	 * interrupt vector the ARM (linux) will be handling (INT0 and/or INT1)
        	 * based on one of the registers in the base module area (offset 5 of 16-bit words)
        	 * We should only enumerate devices that are attached to the interrupt 
        	 * line managed by this core.  Cores assigned to the DSP should not be 
        	 * assigned / probed.
        	 */
        	ps = (unsigned short*)fpgactrl->baseaddr;
        	int_assign.word = ps[BM_INT_ASSIGNMENTS_OFFSET];
        	fpgactrl->handle_ints[0] = (int_assign.bits.irq0_cpu == BM_CPU_ARM) ? 1 : 0;
        	fpgactrl->handle_ints[1] = (int_assign.bits.irq1_cpu == BM_CPU_ARM) ? 1 : 0;

        	// enumerate all of the devices on the PART.
        	fpga_ctrl_enum_devices(fpgactrl);

        	/* Turn on interrupt handler */
            /* allocate the interrupts associated with the fpga and map our
               dispatch handler in for it */
            if (fpgactrl->handle_ints[0])
            {
				tmp = gpio_to_irq(FPGA_INT0);
				if (tmp < 0)
				{
					dev_err( &fpga_ctrl.fpga, "Unable to convert GPIO INT0 setting to IRQ setting\n");
				}
				tmp = request_irq(tmp, fpga_ctrl_irq_router, IRQF_DISABLED | IRQF_TRIGGER_RISING, fpga_ctrl.fpga_drvr.name, &fpga_ctrl);
				if (tmp)
				{
					dev_err( &fpga_ctrl.fpga, "Unable to map FPGA INT0 irq (%d)\n", tmp);
				}
			}
			if (fpgactrl->handle_ints[1])
			{
				tmp = gpio_to_irq(FPGA_INT1);
				if (tmp < 0)
				{
					dev_err( &fpga_ctrl.fpga, "Unable to convert GPIO INT1 setting to IRQ setting\n");
				}
				tmp = request_irq(tmp, fpga_ctrl_irq_router, IRQF_DISABLED | IRQF_TRIGGER_RISING, fpga_ctrl.fpga_drvr.name, &fpga_ctrl);
				if (tmp)
				{
					dev_err( &fpga_ctrl.fpga, "Unable to map FPGA INT1 irq (%d)\n", tmp);
				}
			}
        }
        else
        {
        	fpgactrl->state = FPGA_STATE_PROGRAM_FAIL;
        }
        break;
    default:
        break;
    }
    return count;
}
DEVICE_ATTR(cmd, S_IRUGO | S_IWUSR, NULL, fpga_ctrl_store_cmd);

/**
 *  Handle image data sent from user space.  Only send it to FPGA if we are in the
 *  PROGRAMMING state.
 */
ssize_t fpga_ctrl_store_image(struct device *dev, struct device_attribute *attr,
			                const char *buf, size_t count)
{
    struct fpga_ctrl* fpgactrl = container_of(dev, struct fpga_ctrl, fpga);

    // blast all data at the FPGA if we are in the PROGRAMMING STATE.
    if (fpgactrl->state == FPGA_STATE_PROGRAMMING)
    {
    	int i;
    	for (i = 0; i < count; i++, buf++)
    	{
    		iowrite8(*buf, fpgactrl->baseaddr);
    	}
    }
    return count;
}
DEVICE_ATTR(image,  S_IRUGO | S_IWUSR, NULL, fpga_ctrl_store_image);

/**
 *  This method is used by the sysfs to present the version property
 *  for an FPGA core driver.
 */
static ssize_t show_version(struct device_driver *driver, char *buf)
{
    struct fpga_driver *fpgadriver = to_fpga_driver(driver);
    sprintf(buf, "%s\n", fpgadriver->version);
    return strlen(buf);
}

/**
 *  This method should be called by all FPGA "core" drivers.  This 
 *  will result in devices with matching core IDs to be auto-assigned
 *  to the appropriate drivers.
 */
int register_fpga_driver(struct fpga_driver *driver)
{
    int rv;
    
    driver->driver.bus = &fpga_bus_type;
    rv = driver_register(&driver->driver);
    if (rv)
    {
    	dev_warn(&fpga_ctrl.fpga, "Unable to register driver\n");
    	return rv;
    }
        
    driver->version_attr.attr.name = "version";
#ifdef OLD_KERNEL
    driver->version_attr.attr.owner = driver->driver.owner;
#endif
    driver->version_attr.attr.mode = S_IRUGO;
    driver->version_attr.show = show_version;
    driver->version_attr.store = NULL;
    return driver_create_file(&driver->driver, &driver->version_attr);
    
}
EXPORT_SYMBOL(register_fpga_driver);


struct pin_io {
    int id;
    char* name;
};

const struct pin_io fpga_ios[] = {
        { FPGA_PROGRAM, "fpga_program" },
        { FPGA_INIT, "fpga_init" },
        { FPGA_RDWR, "fpga_rdwr" },
        { FPGA_INT0, "fpga_int0" },
        { FPGA_INT1, "fpga_int1" },
        { -1, NULL }
};

/**
 * Enables the global interrupt mask for a specified interrupt vector.
 *
 * \param[in] level 0 or 1 (for INT0 or INT1)
 * \param[in] vector 0 to 15 (for vector position for given INT level)
 * \param[in] enable 1 if interrupt should be enabled, 0 if disabled
 * \return non-zero if there was an invalid parameter
 */
int enable_irq_vec(int level, int vector, int enable)
{
	unsigned short* lpBaseReg = (unsigned short*)fpga_ctrl.baseaddr;
	unsigned short mask, temp;
	int offset;

	dev_info(&fpga_ctrl.fpga, "enable_irq_vector (%d,%d,%d)\n", level, vector, enable);

	if ((0>vector) || (15<vector))
	{
		dev_err(&fpga_ctrl.fpga, "enable_irq_vector: Invalid Vector Requested %d\n", vector);
		return -2;
	}

	mask = 1<<vector;

	switch(level)
	{
	case 0:
		offset = gnINT0_MASK_OFFSET;
		break;
	case 1:
		offset = gnINT1_MASK_OFFSET;
		break;
	default:
		dev_err(&fpga_ctrl.fpga, "enable_irq_vector: Invalid Level Requested %d\n", level);
		return -1;
	}

	/** spinlock??? */
	temp = lpBaseReg[offset];
	if (enable)
	{
		temp |= mask;
	}
	else
	{
		temp &= ~mask;
	}
	lpBaseReg[offset] = temp;
	return 0;
}
EXPORT_SYMBOL(enable_irq_vec);

/**
 *  This routine frees the various resources in the kernel prior to 
 *  exiting.  It is normally called if there is an initialization error 
 *  or if the module is being removed.
 */
static void fpga_ctrl_cleanup(void)
{
    int i = 0;
	struct device* dev = NULL;
    dev_info( &fpga_ctrl.fpga, "Cleaning up fpga_ctrl\n");

	if (fpga_ctrl.state == FPGA_STATE_PROGRAMMED)
	{
		free_irq(gpio_to_irq(FPGA_INT0), &fpga_ctrl);
	}

    do
    {
    	dev = NULL;
    	bus_for_each_dev(&fpga_bus_type, NULL, &dev, device_get);
    	if (NULL != dev)
    	{
    		device_unregister(dev);
    	}
    } while(dev != NULL);

    bus_unregister(&fpga_bus_type); /* unregister the fpga_bus "type" */

    while(fpga_ios[i].name != NULL)
    {
        gpio_free(fpga_ios[i].id);
        i++;
    }
}

/**
 *  This routine maps in the GPIO lines needed for programming
 *  the FPGA.
 */
static int setup_ios(void)
{
    int rv = 0;
    int i  = 0;
    while(fpga_ios[i].name != NULL)
    {
    	/* u-boot should already have configured FPGA programming pins in pin-mux settings */
    	rv = gpio_request(fpga_ios[i].id, fpga_ios[i].name);
	    if (rv) 
	    {
	    	dev_err( &fpga_ctrl.fpga, "Cannot request GPIO[%d] %s\n",	fpga_ios[i].id, fpga_ios[i].name);
	        return rv;
	    }
    	i++;
    }
    return rv;
}

static int __init fpga_ctrl_init(void)
{

    dev_info( &fpga_ctrl.fpga, "loading the fpga_ctrl module.\n");

    /* map in the module base address space here and save it off */
    fpga_ctrl.mem_region = request_mem_region(FPGA_BASE_ADDR, FPGA_CORE_SIZE, 
                                              fpga_ctrl.fpga_drvr.name);
                                              
    fpga_ctrl.baseaddr = ioremap_nocache(FPGA_BASE_ADDR, FPGA_CORE_SIZE*num_fpga_cores);

    if(setup_ios())
    {
        goto exit_bail;
    }
      
    /* make sure programming I/O pins are configured properly */
    /* Need:
     *  - init_b (axr7 / epwmitz[0] / gpI [15] ) as an input
     *  - program_b (resetout#) as an output
     *  - rdwr_b (EMA_RW# / GP3[9] ) as an output
     */
	gpio_direction_output(FPGA_PROGRAM, 1);
	gpio_direction_input(FPGA_INIT);
	gpio_direction_output(FPGA_RDWR, 1);
	gpio_direction_input(FPGA_INT0);
	gpio_direction_input(FPGA_INT1);

    /* register the "fpga" bus class / type */
    if (bus_register(&fpga_bus_type))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to register fpga_bus_type\n");
        goto exit_bail;
    }
        
    /* register the specific instance of the fpga bus, i.e. fpga0 */
    if (device_register(&fpga_ctrl.fpga))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to register fpga_ctrl device\n");
        goto exit_bail;
    }
    
    /* set our device state attribute for user access */
    if (device_create_file(&fpga_ctrl.fpga, &dev_attr_state))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to create state device property\n");
        goto exit_bail;
    }
    
    /* set our binary FPGA image attribute for programming */
    if (device_create_file(&fpga_ctrl.fpga, &dev_attr_cmd))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to create cmd device property\n");
        goto exit_bail;
    }

    /* set our binary FPGA image attribute for programming */
    if (device_create_file(&fpga_ctrl.fpga, &dev_attr_image))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to create image device property\n");
        goto exit_bail;
    }

    /* set our version attribute for reporting the FPGA version information */
    if (device_create_file(&fpga_ctrl.fpga, &dev_attr_version))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to create version device property\n");
        goto exit_bail;
    }

    if (device_create_file(&fpga_ctrl.fpga, &dev_attr_int_status))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to create interrupt status property\n");
        goto exit_bail;
    }

    if (device_create_file(&fpga_ctrl.fpga, &dev_attr_device_dna))
    {
    	dev_err( &fpga_ctrl.fpga, "Unable to create device DNA property\n");
    	goto exit_bail;
    }

    if (!(fpga_ctrl.major = register_chrdev(0, "fpga", &fpgachr_fops)))
    {
        dev_err( &fpga_ctrl.fpga, "Unable to get major for FPGA cores\n");
        goto exit_bail;
    }

    return 0;

exit_bail: /* Uh-Oh */
    fpga_ctrl_cleanup();
    return -1;
}




#ifdef MODULE
static void fpga_ctrl_exit(void)
{
	fpga_ctrl_cleanup();

	if(fpga_ctrl.major)
		unregister_chrdev(fpga_ctrl.major, "fpga");

	dev_info(&fpga_ctrl.fpga, "FPGA Module Exit\n");
}


module_init(fpga_ctrl_init);
module_exit(fpga_ctrl_exit);
#else
subsys_initcall(fpga_ctrl_init);
#endif
