/*
 * fpga_gpio.c
 *
 *  Created on: Aug 7, 2010
 *      Author: Mike Williamson
 */

#include "common/fpga/fpga_gpio.h"
#include "common/fpga/core_ids.h"
#include "fpga.h"

#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/version.h>
#include <asm-generic/gpio.h>
#include <asm/mach/irq.h>
#include <mach/irqs.h>
#include <asm/io.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Williamson <michael.williamson@criticallink.com");
MODULE_DESCRIPTION("Driver for MityDSP-L138 FPGA Based GPIO Interface");

#define IRQ_SUPPORT

struct fpga_gpio {
	struct gpio_chip chip;
	unsigned short numbanks;
	unsigned short ioperbank;
	int irq_base;
};

#define chip2controller(chip)	\
	container_of(chip, struct fpga_gpio, chip)

/**
 * IRQ handler called when an GPIO core is asserting an interrupt
 * condition.  This method is called within the context of an ISR, and
 * must be atomic.
 *
 * \param[in] dev the device issuing the interrupt.
 * \return 0
 */
static int fpga_gpio_IrqHandler(struct fpga_device* dev)
{
	struct fpga_gpio *driver_data;
	unsigned short *lpBaseReg;
	int i, bank, offset, mask;
	unsigned short tmp;

	driver_data = dev_get_drvdata(&dev->dev);
	lpBaseReg = (unsigned short*)dev->baseaddr;

	/* figure out which gpios have pending interrupts */
	for (bank = 0, i = 0; bank < driver_data->numbanks; bank++) {
		tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIP_OFFSET];
		for (mask = 1, offset = 0; offset < driver_data->ioperbank;
			offset++, i++, mask <<= 1) {
			if (tmp & mask)
				generic_handle_irq_desc(driver_data->irq_base+i, 
					irq_desc+driver_data->irq_base+i);
		}
		lpBaseReg[gnBANK_OFFSET[bank]+gnIP_OFFSET] = tmp;
	}

	return 0;
}

ssize_t fpga_gpio_show_regs(struct device *dev, 
	struct device_attribute *attr, char *buf)
{
	int i;
	ssize_t rv = 0;
	unsigned short *lpBaseReg = NULL;
	struct fpga_device *fpgadev = to_fpga_device(dev);
	struct fpga_gpio *d = (struct fpga_gpio*)dev_get_drvdata(dev);
	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	for (i = 0; i < d->numbanks; i++)
	{
	    rv += sprintf(&buf[rv],"BANK[%d] DIR : 0x%04X\n", i, 
	    	ioread16(&lpBaseReg[gnBANK_OFFSET[i]+gnIODIR_OFFSET]));
	    rv += sprintf(&buf[rv],"BANK[%d] VAL : 0x%04X\n", i, 
	    	ioread16(&lpBaseReg[gnBANK_OFFSET[i]+gnIOVAL_OFFSET]));
	    rv += sprintf(&buf[rv],"BANK[%d] IRE : 0x%04X\n", i, 
	    	ioread16(&lpBaseReg[gnBANK_OFFSET[i]+gnIRE_OFFSET]));
	    rv += sprintf(&buf[rv],"BANK[%d] IFE : 0x%04X\n", i, 
	    	ioread16(&lpBaseReg[gnBANK_OFFSET[i]+gnIFE_OFFSET]));
	    rv += sprintf(&buf[rv],"BANK[%d] IP : 0x%04X\n", i, 
	    	ioread16(&lpBaseReg[gnBANK_OFFSET[i]+gnIP_OFFSET]));
	}
	return rv;
}
static DEVICE_ATTR(show_regs, S_IRUGO, fpga_gpio_show_regs, NULL);

static struct attribute *fpga_gpio_attributes[] = {
	&dev_attr_show_regs.attr,
	NULL,
};

static struct attribute_group fpga_gpio_attr_group = {
	.attrs = fpga_gpio_attributes,
};


/**
 * This routine is called when a device is removed from the FPGA bus.
 *
 * \param[in] dev pointer to the device being removed.  */
static int fpga_gpio_remove(struct device *dev)
{
	int rv = 0;
	struct fpga_gpio *driver_data;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	driver_data = dev_get_drvdata(dev);

	dev_dbg(dev,  "%s entered\n", __func__);

	/* make sure IRQ is disabled */
	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
				   fpgadev->coreversion.ver0.bits.vector,
				   0);

	rv = gpiochip_remove(&driver_data->chip);
	if (rv)
		dev_warn(dev, "%s trouble removing gpiochip, %d\n", __func__,
			 rv);

	kfree(driver_data);

	sysfs_remove_group(&dev->kobj, &fpga_gpio_attr_group);

	dev_dbg(dev,  "%s completed\n", __func__);
	return rv;
}

static int fpga_gpio_probe(struct device *dev);

/**
 * The driver object.  The information here must be common for
 * all of the potential drivers in the system.
 */
static struct fpga_driver fpga_gpio_driver = {
	.id		= CORE_ID_GPIO, /** must match value in core_ids.h */
	.version	= "GPIO Driver 1.0",
	.IrqHandler	= fpga_gpio_IrqHandler,
	.probe		= fpga_gpio_probe,
	.remove		= fpga_gpio_remove,
	.driver		= {
		.name = "fpga_gpio",
		.owner = THIS_MODULE,
	},
};

static int fpga_gpio_direction_in(struct gpio_chip *chip, unsigned offset)
{
	unsigned short      bank, mask, tmp;
	struct fpga_gpio   *d = chip2controller(chip);
	struct fpga_device *fpgadev = to_fpga_device(chip->dev);
	unsigned short     *lpBaseReg = NULL;

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	bank = offset / d->ioperbank;
	mask = 1<<(offset-(bank*d->ioperbank));

	tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIODIR_OFFSET];

	tmp &= ~mask;

	lpBaseReg[gnBANK_OFFSET[bank]+gnIODIR_OFFSET] = tmp;

	return 0;
}

static int
fpga_gpio_direction_out(struct gpio_chip *chip, unsigned offset, int value)
{
	unsigned short      bank, mask, tmp;
	struct fpga_gpio   *d = chip2controller(chip);
	struct fpga_device *fpgadev = to_fpga_device(chip->dev);
	unsigned short     *lpBaseReg = NULL;

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	bank = offset / d->ioperbank;
	mask = 1<<(offset-(bank*d->ioperbank));

	tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET];

	if (value)
		tmp |= mask;
	else
		tmp &= ~mask;

	lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET] = tmp;

	tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIODIR_OFFSET];

	tmp |= mask;

	lpBaseReg[gnBANK_OFFSET[bank]+gnIODIR_OFFSET] = tmp;

	return 0;
}

/*
 * Read the pin's value (works even if it's set up as output);
 * returns zero/nonzero.
 */
static int fpga_gpio_get(struct gpio_chip *chip, unsigned offset)
{
	unsigned short      bank, mask;
	struct fpga_gpio   *d = chip2controller(chip);
	struct fpga_device *fpgadev = to_fpga_device(chip->dev);
	unsigned short     *lpBaseReg = NULL;
	int		   rv = 0;

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	bank = offset / d->ioperbank;
	mask = 1<<(offset-(bank*d->ioperbank));

	rv = ioread16(&lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET]) & mask;

	return rv;
}
/*
 */
static void
fpga_gpio_set(struct gpio_chip *chip, unsigned offset, int value)
{
	unsigned short      bank, mask, val;
	struct fpga_gpio   *d = chip2controller(chip);
	struct fpga_device *fpgadev = to_fpga_device(chip->dev);
	unsigned short     *lpBaseReg = NULL;

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	bank = offset / d->ioperbank;
	mask = 1<<(offset-(bank*d->ioperbank));

	val = lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET];

	if (value)
		val |= mask;
	else
		val &= ~mask;

	lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET] = val;
}

int gpio_base = DAVINCI_N_GPIO;
module_param (gpio_base, int, S_IRUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC (gpio_base, "The base adaptor number for "
		                     "FPGA Based IO pins");

#ifdef IRQ_SUPPORT
int irq_base = DA850_N_CP_INTC_IRQ + DAVINCI_N_GPIO;
module_param (irq_base, int, S_IRUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC (irq_base, "The base irq number for "
		                     "FPGA Based GPIO pins");

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,1,0)
static void gpio_irq_disable(unsigned irq)
{
	struct fpga_gpio* chip = irq_desc[irq].handler_data;
#else
static void gpio_irq_disable(struct irq_data* data)
{
	struct fpga_gpio* chip = irq_data_get_irq_handler_data(data);
	int irq = data->irq;
#endif
	struct fpga_device *fpgadev = to_fpga_device(chip->chip.dev);
	unsigned short     *lpBaseReg = (unsigned short*)fpgadev->baseaddr;
	unsigned short tmp;

	int offset = irq-chip->irq_base;
	int bank = offset / chip->ioperbank;
	int off_in_bank = offset - bank * chip->ioperbank;

	tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIRE_OFFSET];
	tmp &= ~(1 << off_in_bank);
	lpBaseReg[gnBANK_OFFSET[bank]+gnIRE_OFFSET] = tmp;

	tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIFE_OFFSET];
	tmp &= ~(1 << off_in_bank);
	lpBaseReg[gnBANK_OFFSET[bank]+gnIFE_OFFSET] = tmp;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,1,0)
static void gpio_irq_enable(unsigned irq)
{
	struct fpga_gpio* chip = irq_desc[irq].handler_data;
	unsigned status = irq_desc[irq].status;
#else
static void gpio_irq_enable(struct irq_data* data)
{
	int irq = data->irq;
	struct fpga_gpio* chip = irq_data_get_irq_handler_data(data);
	unsigned status = irqd_get_trigger_type(data);
#endif
	struct fpga_device *fpgadev = to_fpga_device(chip->chip.dev);
	unsigned short     *lpBaseReg = (unsigned short*)fpgadev->baseaddr;
	unsigned short tmp;
	int offset = irq-chip->irq_base;
	int bank = offset / chip->ioperbank;
	int off_in_bank = offset - bank * chip->ioperbank;

	status &= IRQ_TYPE_EDGE_FALLING | IRQ_TYPE_EDGE_RISING;
	if (!status)
		status = IRQ_TYPE_EDGE_FALLING | IRQ_TYPE_EDGE_RISING;

	tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIRE_OFFSET];
	if (status & IRQ_TYPE_EDGE_RISING)
		tmp |= (1 << off_in_bank);
	else
		tmp &= ~(1 << off_in_bank);
	lpBaseReg[gnBANK_OFFSET[bank]+gnIRE_OFFSET] = tmp;

	tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIFE_OFFSET];
	if (status & IRQ_TYPE_EDGE_FALLING)
		tmp |= (1 << off_in_bank);
	else
		tmp &= ~(1 << off_in_bank);
	lpBaseReg[gnBANK_OFFSET[bank]+gnIFE_OFFSET] = tmp;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,1,0)
static int gpio_irq_type(unsigned irq, unsigned trigger)
{
	struct fpga_gpio* chip = irq_desc[irq].handler_data;
#else
static int gpio_irq_type(struct irq_data* data, unsigned trigger)
{
	int irq = data->irq;
	struct fpga_gpio* chip = irq_get_handler_data(irq);
#endif
	struct fpga_device *fpgadev = to_fpga_device(chip->chip.dev);
	unsigned short     *lpBaseReg = (unsigned short*)fpgadev->baseaddr;
	unsigned short tmp;
	int offset = irq-chip->irq_base;
	int bank = offset / chip->ioperbank;
	int off_in_bank = offset - bank * chip->ioperbank;

	if (trigger & ~(IRQ_TYPE_EDGE_FALLING | IRQ_TYPE_EDGE_RISING))
		return -EINVAL;

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,1,0)
	irq_desc[irq].status &= ~IRQ_TYPE_SENSE_MASK;
	irq_desc[irq].status |= trigger;
#else
	irq_clear_status_flags(irq, IRQ_TYPE_SENSE_MASK);
	irq_set_status_flags(irq, trigger);
#endif
	if (irq_desc[irq].depth == 0) {
		tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIRE_OFFSET];
		if (trigger & IRQ_TYPE_EDGE_RISING) 
			tmp |= (1 << off_in_bank);
		else
			tmp &= ~(1 << off_in_bank);
		lpBaseReg[gnBANK_OFFSET[bank]+gnIRE_OFFSET] = tmp;

		tmp = lpBaseReg[gnBANK_OFFSET[bank]+gnIFE_OFFSET];
		if (trigger & IRQ_TYPE_EDGE_FALLING)
			tmp |= (1 << off_in_bank);
		else
			tmp &= ~(1 << off_in_bank);
		lpBaseReg[gnBANK_OFFSET[bank]+gnIFE_OFFSET] = tmp;
	}

	return 0;
}

static struct irq_chip gpio_irqchip = {
	.name		= "GPIO",
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,1,0)
	.enable		= gpio_irq_enable,
	.disable	= gpio_irq_disable,
	.set_type	= gpio_irq_type,
#else
	.irq_enable	= gpio_irq_enable,
	.irq_disable	= gpio_irq_disable,
	.irq_set_type	= gpio_irq_type,
#endif
};

static int fpga_gpio_to_irq(struct gpio_chip *chip, unsigned offset)
{
	struct fpga_gpio *d = container_of(chip, struct fpga_gpio, chip);

	if (d->irq_base >= 0)
		return d->irq_base + offset;
	else
		return -ENODEV;
}

static int fpga_gpio_irq_setup(struct fpga_gpio *chip)
{
	int lvl;

	chip->chip.to_irq = fpga_gpio_to_irq;
	for (lvl = 0; lvl < chip->chip.ngpio; lvl++) {
		int irq = lvl + chip->irq_base;
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,1,0)
		set_irq_chip(irq, &gpio_irqchip);
		set_irq_data(irq, chip);
		set_irq_handler(irq, handle_simple_irq);
#else
		irq_set_chip_and_handler(irq, &gpio_irqchip, handle_simple_irq);
		irq_set_handler_data(irq, chip);
#endif
		set_irq_flags(irq, IRQF_VALID);
	}
	return 0;
}

#else
static int fpga_gpio_irq_setup(struct fpga_gpio *chip) {
	return 0;
}
#endif

/**
 * The fpga_gpio_probe routine is called after the gpio driver is successfully
 * matched to an FPGA core with the same core ID.
 *
 * \param[in] dev device within an fpga_device structure.
 * return 0 on successful probe / initialization.
 */
static int fpga_gpio_probe(struct device *dev)
{
	int rv = 0;
	struct fpga_gpio  *driver_data = NULL;
	unsigned short     *lpBaseReg = NULL;
	tuConfigReg         config;

	struct fpga_device* fpgadev = to_fpga_device(dev);

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;
	config.mnWord = lpBaseReg[gnCFG_OFFSET];

	dev_dbg(dev, "%s entered\n", __func__);

	driver_data = kzalloc(sizeof(struct fpga_gpio), GFP_KERNEL);
	if (!driver_data)
	{
		rv = -ENOMEM;
		goto probe_bail;
	}

	driver_data->numbanks = config.msBits.mnNumBanks;
	driver_data->ioperbank = config.msBits.mnNumIOPerBank;

	driver_data->chip.label = "FPGA GPIO";

	driver_data->chip.direction_input = fpga_gpio_direction_in;
	driver_data->chip.get = fpga_gpio_get;
	driver_data->chip.direction_output = fpga_gpio_direction_out;
	driver_data->chip.set = fpga_gpio_set;

	driver_data->chip.base = gpio_base;
	driver_data->chip.ngpio = driver_data->numbanks*driver_data->ioperbank;
	driver_data->chip.dev = dev;
	gpio_base += driver_data->chip.ngpio;

#ifdef IRQ_SUPPORT
	driver_data->irq_base = irq_base;
	irq_base += driver_data->chip.ngpio;
#endif

	gpiochip_add(&driver_data->chip);

	dev_set_drvdata(dev, driver_data);

	fpga_gpio_irq_setup(driver_data);

	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
			       fpgadev->coreversion.ver0.bits.vector,
			       1);

	if(sysfs_create_group(&dev->kobj, &fpga_gpio_attr_group)) {
		dev_warn(dev, "Unable to create sysfs group\n");
	}

	return rv;

	kfree(driver_data);

probe_bail:
	return rv;
}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init fpga_gpio_init(void)
{
	int ret;

	ret = register_fpga_driver(&fpga_gpio_driver);
	if (ret)
	{
		printk(KERN_ALERT "Unable to register fpga_gpio driver\n");
		goto exit_bail;
	}

	return 0;

exit_bail: /* Uh-Oh */
	return -1;
}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void fpga_gpio_exit(void)
{
	driver_unregister(&fpga_gpio_driver.driver);
}

module_init(fpga_gpio_init);
module_exit(fpga_gpio_exit);


