/*
 * Critical Link MityOMAP-L138 SoM Baseboard initialization file
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/usb/musb.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/setup.h>
#include <mach/mux.h>
#include <mach/da8xx.h>

#define BASEBOARD_NAME "Critical Link Test Fixture"

/*
 * Custom Pin Muxing for this baseboard
 */
static short baseboard_pins[] __initdata = {
	DA850_GPIO6_10,
	DA850_GPIO2_0,
	DA850_GPIO6_6,
	DA850_GPIO6_9,
	DA850_GPIO6_8,
	DA850_GPIO6_5,
	DA850_GPIO6_11,
	DA850_GPIO8_13,
	DA850_GPIO6_3,
	DA850_GPIO6_4,
	DA850_GPIO8_12,
	DA850_GPIO8_14,
	DA850_GPIO8_15,
	DA850_GPIO6_7,
	DA850_GPIO7_7,
	DA850_GPIO7_6,
	DA850_GPIO7_5,
	DA850_GPIO7_4,
	DA850_GPIO7_3,
	DA850_GPIO7_2,
	DA850_GPIO7_1,
	DA850_GPIO7_0,
	DA850_GPIO7_15,
	DA850_GPIO2_6,
	DA850_GPIO7_14,
	DA850_GPIO8_8,
	DA850_GPIO8_9,
	DA850_GPIO2_4,
	DA850_GPIO7_13,
	DA850_GPIO2_5,
	DA850_GPIO8_11,
	DA850_GPIO7_12,
	DA850_GPIO7_11,
	DA850_GPIO7_10,
	DA850_GPIO7_9,
	DA850_GPIO7_8,
	DA850_GPIO6_0,
	DA850_GPIO6_15,
	DA850_GPIO6_2,
	DA850_GPIO6_1,
	DA850_GPIO8_10,
	DA850_GPIO4_4,
	DA850_GPIO4_5,
	DA850_GPIO4_6,
	DA850_GPIO4_7,
	DA850_GPIO2_11,
	DA850_GPIO0_2,
	DA850_GPIO2_10,
	DA850_GPIO0_0,
	DA850_GPIO2_12,
	DA850_GPIO0_8,
	DA850_GPIO2_13,
	DA850_GPIO0_9,
	DA850_GPIO2_15,
	DA850_GPIO4_0,
	DA850_GPIO4_1,
	DA850_GPIO1_2,
	DA850_GPIO4_2,
	DA850_GPIO1_3,
	DA850_GPIO4_3,
	DA850_GPIO0_7,
	DA850_GPIO0_10,
	DA850_GPIO0_11,
	DA850_GPIO0_15,
	DA850_GPIO0_6,
	DA850_GPIO0_14,
	DA850_GPIO0_12,
	DA850_GPIO0_5,
	DA850_GPIO0_13,
	DA850_GPIO0_1,
	DA850_GPIO0_4,
	DA850_GPIO0_3,
	DA850_GPIO6_14,

	/* LEDs */
	DA850_GPIO6_12,
	DA850_GPIO6_13,

	/* Don't interfere with pins pre-configured by bootloader */

	-1,
};

static short baseboard_unflopped_pins[] = {
	DA850_I2C0_SDA,
	DA850_I2C0_SCL,

	DA850_EMA_D_0,
	DA850_EMA_D_1,
	DA850_EMA_D_2,
	DA850_EMA_D_3,
	DA850_EMA_D_4,
	DA850_EMA_D_5,
	DA850_EMA_D_6,
	DA850_EMA_D_7,
	DA850_EMA_D_8,
	DA850_EMA_D_9,
	DA850_EMA_D_10,
	DA850_EMA_D_11,
	DA850_EMA_D_12,
	DA850_EMA_D_13,
	DA850_EMA_D_14,
	DA850_EMA_D_15,

	DA850_EMA_WAIT_0,
	DA850_NEMA_OE,
	DA850_NEMA_CS_3,
	DA850_EMA_A_2,
	DA850_EMA_A_1,
	DA850_NEMA_WE,

	/* NEED TO ADD PINS FOR FPGA INTERFACING */
	DA850_EMA_WE_DQM_0,
	DA850_EMA_WE_DQM_1,
	DA850_NEMA_CS_5,
	DA850_EMA_A_0,
	DA850_EMA_A_3,
	DA850_EMA_A_4,
	DA850_EMA_A_5,
	DA850_EMA_A_6,
	DA850_EMA_A_7,
	DA850_EMA_A_8,
	DA850_EMA_A_9,
	DA850_EMA_A_10,
	DA850_EMA_CLK,

	-1,
};

static short baseboard_flopped_pins[] = {
	DA850_GPIO1_4,
	DA850_GPIO1_5,

	DA850_GPIO4_8,
	DA850_GPIO4_9,
	DA850_GPIO4_10,
	DA850_GPIO4_11,
	DA850_GPIO4_12,
	DA850_GPIO4_13,
	DA850_GPIO4_14,
	DA850_GPIO4_15,
	DA850_GPIO3_0,
	DA850_GPIO3_1,
	DA850_GPIO3_2,
	DA850_GPIO3_3,
	DA850_GPIO3_4,
	DA850_GPIO3_5,
	DA850_GPIO3_6,
	DA850_GPIO3_7,

	DA850_GPIO3_8,
	DA850_GPIO3_10,
	DA850_GPIO3_14,
	DA850_GPIO5_2,
	DA850_GPIO5_1,
	DA850_GPIO3_11,

	/* NEED TO ADD PINS NORMALLY USED FOR FPGA INTERFACING */
	DA850_GPIO2_2,
	DA850_GPIO2_3,
	DA850_GPIO3_12,
	DA850_GPIO5_0,
	DA850_GPIO5_3,
	DA850_GPIO5_4,
	DA850_GPIO5_5,
	DA850_GPIO5_6,
	DA850_GPIO5_7,
	DA850_GPIO2_7,
	DA850_GPIO3_13,
	DA850_GPIO3_15,
	DA850_GPIO5_8,
	DA850_GPIO5_9,
	DA850_GPIO5_10,
	DA850_GPIO5_14,
	DA850_GPIO5_13,

	-1,
};

static void baseboard_config_pullups(void)
{
	void __iomem *pupdctl1 =
		IO_ADDRESS(DAVINCI_SYSTEM_MODULE_BASE + 0x7c);

	/* Configure pull down control */
	__raw_writel((__raw_readl(pupdctl1) & ~0xE),
			pupdctl1);
}

static int __init baseboard_pre_init(void)
{
	pr_info("%s: Entered\n", __func__);
	davinci_soc_info.emac_pdata->phy_id = "0:03";
	return 0;
}
postcore_initcall_sync(baseboard_pre_init);

static int __init baseboard_init(void)
{
	int ret;
	pr_info("%s [%s]...\n", __func__, BASEBOARD_NAME);

	ret = davinci_cfg_reg_list(baseboard_pins);
	if (ret)
		pr_warning("%s unable to configure baseboard pinmux (%d)\n",
			__func__, ret);

	baseboard_config_pullups();

	mityomapl138_usb_init(MUSB_HOST);

	return 0;
}

/*
 * Stores the state of the multiplexer configuration.
 * true = GPIO functionality,
 * false = other devices
 */
static int is_flopped;

/*
 * Reads back the is_flopped variable to a user-mode caller.
 */
static ssize_t flopped_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	return sprintf(buf, "%d\n", is_flopped);
}

/*
 * Sets the is_flopped variable and updates the multiplexer configuration.
 */
static ssize_t flopped_store(struct kobject *kobj, struct kobj_attribute *attr,
			 const char *buf, size_t count)
{
	sscanf(buf, "%du", &is_flopped);

	if (is_flopped)
		davinci_cfg_reg_list(baseboard_flopped_pins);
	else
		davinci_cfg_reg_list(baseboard_unflopped_pins);

	return count;
}

static struct kobj_attribute flopped_attribute =
	__ATTR(enable_device_gpio, 0666, flopped_show, flopped_store);

static struct attribute *attrs[] = {
	&flopped_attribute.attr,
	NULL
};

static struct attribute_group attr_group = {
	.attrs = attrs,
};

static struct kobject *flopped_kobj;

/*
 * Initializes the kobject used to provide a user-mode file
 * API for changing GPIO configuration.
 */
static int __init baseboard_sysfs_init(void)
{
	int retval;

	flopped_kobj = kobject_create_and_add("gpio_ext", kernel_kobj);
	if (!flopped_kobj)
		return -ENOMEM;

	retval = sysfs_create_group(flopped_kobj, &attr_group);
	if (retval)
		kobject_put(flopped_kobj);

	return retval;
}

static void __exit baseboard_sysfs_exit(void)
{
	kobject_put(flopped_kobj);
}

arch_initcall_sync(baseboard_init);
module_init(baseboard_sysfs_init);
module_exit(baseboard_sysfs_exit);
