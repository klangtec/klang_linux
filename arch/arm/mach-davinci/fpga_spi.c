/*
 * fpga_spi.c
 *
 *  Created on: Sep 9, 2010
 *      Author: Mike Williamson
 */

#include "common/fpga/fpga_spi.h"
#include "common/fpga/core_ids.h"
#include "fpga.h"

#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi_bitbang.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <asm/io.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Williamson <michael.williamson@criticallink.com");
MODULE_DESCRIPTION("Driver for MityDSP-L138 FPGA Based SPI Controller Interface");

int loopback = 0;
module_param (loopback, int, S_IRUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC (loopback, "When 1 forces installed spidev user device"
			    " into loopback mode (default 0)");

int bus_num = 3;
module_param (bus_num, int, S_IRUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC (bus_num, "default bus ID for spi core (default 3)");

struct fpga_spi_slave {
	int bytes_per_word;
};

/**
 *  this is the device driver specific parameters tied to each spi controller
 *  device in the system (the object data)
 */
struct fpga_spi {
	struct spi_bitbang	bitbang;
	struct completion	done;
	struct fpga_spi_slave	slave[MAX_FPGA_CHIP_SELECTS];
	int			remaining_bytes;
	int			fifo_depth;
	int			bytes_per_word;
	int			rx_remaining_bytes;
	int			delay;
	const uint8_t*		tx_buf;
	uint8_t*		rx_buf;
	struct fpga_device*	fpgadev;
};

static ssize_t fpga_spi_csr_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_spi* fpga_spi = (struct fpga_spi*)spi_master_get_devdata(dev_get_drvdata(dev));
	unsigned short* lpBaseReg = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	tuSPICSR CSR;

	CSR.mnWord = ioread16(&lpBaseReg[gnCSR_OFFSET]);

	return sprintf(buf, "0x%X\n"
			            "RE = %d\n"
			            "SCLK = %d\n"
			            "CPHA = %d\n"
			            "CPOL = %d\n"
			            "Width = %d\n"
			            "LB = %d\n",
			            CSR.mnWord,
			            CSR.msBits.rcv_en,
			            CSR.msBits.sclk_ctrl,
			            CSR.msBits.cpha,
			            CSR.msBits.cpol,
			            1<<(CSR.msBits.dwidth+2),
			            CSR.msBits.loopback);
}
static DEVICE_ATTR(csr, S_IRUGO, fpga_spi_csr_show, NULL);

static ssize_t fpga_spi_state_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	ssize_t rv = 0;
	struct fpga_spi* fpga_spi = (struct fpga_spi*)spi_master_get_devdata(dev_get_drvdata(dev));
	unsigned short* lpBaseReg = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	tuSPICSR CSR;
	tuSPIFWR FWR;
	tuSPIFRR FRR;
	tuSPIIER IER;
	tuSPIIPR IPR;
	tuSPIDIV DIV;
	tuSPIDEL DEL;

	CSR.mnWord = ioread16(&lpBaseReg[gnCSR_OFFSET]);
	FWR.mnWord = ioread16(&lpBaseReg[gnFWR_OFFSET]);
	FRR.mnWord = ioread16(&lpBaseReg[gnFRR_OFFSET]);
	IER.mnWord = ioread16(&lpBaseReg[gnIER_OFFSET]);
	IPR.mnWord = ioread16(&lpBaseReg[gnIPR_OFFSET]);
	DIV.mnWord = ioread16(&lpBaseReg[gnDIV_OFFSET]);
	DEL.mnWord = ioread16(&lpBaseReg[gnDEL_OFFSET]);
	rmb();

	rv += sprintf(buf, "CSR = 0x%X\n"
			"	RE = %d\n"
			"	SCLK = %d\n"
			"	CPHA = %d\n"
			"	CPOL = %d\n"
			"	Width = %d\n"
			"	LB = %d\n",
			CSR.mnWord,
			CSR.msBits.rcv_en,
			CSR.msBits.sclk_ctrl,
			CSR.msBits.cpha,
			CSR.msBits.cpol,
			1<<(CSR.msBits.dwidth+2),
			CSR.msBits.loopback);

	rv += sprintf(&buf[rv], "FWR = 0x%X\n"
				"	write_cnt = %d\n"
				"	depth = %d\n",
				FWR.mnWord,
				FWR.msBits.write_cnt,
				1<<(FWR.msBits.depth+4));

	rv += sprintf(&buf[rv], "FRR = 0x%X\n"
				"	write_cnt = %d\n"
				"	depth = %d\n"
				"	empty = %d\n",
				FRR.mnWord,
				FRR.msBits.read_cnt,
				1<<(FRR.msBits.depth+4),
				FRR.msBits.empty);

	rv += sprintf(&buf[rv], "IPR = 0x%X\n"
				"	mosi_tc = %d\n"
				"	mosi_hf = %d\n"
				"	miso_da = %d\n"
				"	miso_hf = %d\n",
				IPR.mnWord,
				IPR.msBits.mosi_tc,
				IPR.msBits.mosi_hf,
				IPR.msBits.miso_nempty,
				IPR.msBits.miso_hf);

	rv += sprintf(&buf[rv], "IER = 0x%X\n"
				"	mosi_tc = %d\n"
				"	mosi_hf = %d\n"
				"	miso_da = %d\n"
				"	miso_hf = %d\n",
				IER.mnWord,
				IER.msBits.mosi_tc,
				IER.msBits.mosi_hf,
				IER.msBits.miso_nempty,
				IER.msBits.miso_hf);

	rv += sprintf(&buf[rv], "DIV = 0x%X\n"
				"	divisor  = %d\n"
				"	chip_sel = %d\n",
				DIV.mnWord,
				DIV.msBits.divisor,
				DIV.msBits.cs_en);
				
	rv += sprintf(&buf[rv], "DEL = 0x%X\n"
				"	delay = %d\n"
				"	enable = %d\n",
				DEL.mnWord,
				DEL.msBits.delay,
				DEL.msBits.delay_en);
	return rv;
}
static DEVICE_ATTR(state, S_IRUGO, fpga_spi_state_show, NULL);

static ssize_t fpga_spi_remaining_bytes_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_spi* fpga_spi = (struct fpga_spi*)spi_master_get_devdata(dev_get_drvdata(dev));
	return sprintf(buf, "%d\n", fpga_spi->remaining_bytes);
}
static DEVICE_ATTR(remaining_bytes, S_IRUGO, fpga_spi_remaining_bytes_show, NULL);

static ssize_t fpga_spi_fifo_depth_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_spi* fpga_spi = (struct fpga_spi*)spi_master_get_devdata(dev_get_drvdata(dev));
	return sprintf(buf, "%d\n", fpga_spi->fifo_depth);
}
static DEVICE_ATTR(fifo_depth, S_IRUGO, fpga_spi_fifo_depth_show, NULL);

static ssize_t fpga_spi_delay_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_spi* fpga_spi = (struct fpga_spi*)spi_master_get_devdata(dev_get_drvdata(dev));
	
	return sprintf(buf, "%d\n",fpga_spi->delay);
}

static ssize_t fpga_spi_delay_set(struct device *dev, 
		struct device_attribute *attr, const char *buf, size_t count)
{
	int delay;
	tuSPIDEL DEL;
	struct fpga_spi* fpga_spi = (struct fpga_spi*)spi_master_get_devdata(dev_get_drvdata(dev));
	unsigned short* lpBaseAddr = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	DEL.mnWord = 0;
	
	sscanf(buf,"%d", &delay);
	
	if (delay >= 0)
	{
		DEL.msBits.delay = delay;
		DEL.msBits.delay_en = 1;
	}
	fpga_spi->delay = delay;
	
	iowrite16(DEL.mnWord, &lpBaseAddr[gnDEL_OFFSET]);

	return count;
}
static DEVICE_ATTR(delay, S_IRUGO | S_IWUSR, fpga_spi_delay_show, fpga_spi_delay_set);

static struct attribute *fpga_spi_attributes[] = {
		&dev_attr_fifo_depth.attr,
		&dev_attr_remaining_bytes.attr,
		&dev_attr_csr.attr,
		&dev_attr_state.attr,
		&dev_attr_delay.attr,
		NULL,
};

static struct attribute_group fpga_spi_attr_group = {
		.attrs = fpga_spi_attributes,
};

static void tx_word(struct fpga_spi *fpga_spi)
{
	unsigned short *lpBaseReg = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	tuSPIFDR FDR;

	FDR.mnWord = 0;

	if (fpga_spi->tx_buf) {
		int i;
		for (i = 0; i < fpga_spi->bytes_per_word; i++)
			FDR.mnWord |= (unsigned) *fpga_spi->tx_buf++ << (i * 8);
	}
	iowrite32(FDR.mnWord, (uint32_t *) &lpBaseReg[gnFDR_OFFSET]);
	fpga_spi->remaining_bytes -= fpga_spi->bytes_per_word;
}

static void rx_word(struct fpga_spi *fpga_spi)
{
	unsigned short *lpBaseReg = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	tuSPIFDR FDR;

	FDR.mnWord = ioread32((uint32_t *) &lpBaseReg[gnFDR_OFFSET]);
	if (fpga_spi->rx_buf) {
		int i;
		for (i = 0; i < fpga_spi->bytes_per_word; i++) {
			*fpga_spi->rx_buf++ = FDR.mnWord & 0xFF;
			FDR.mnWord >>= 8;
		}
		fpga_spi->rx_remaining_bytes -= fpga_spi->bytes_per_word;
	}
}

static void fill_tx_fifo(struct fpga_spi *fpga_spi)
{
	tuSPIFWR frw;
	unsigned short  *lpBaseReg = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	int words_to_write;
	
	frw.mnWord = ioread16(&lpBaseReg[gnFWR_OFFSET]);
	/* while there is space in the FIFO... */
	words_to_write = fpga_spi->fifo_depth-2-frw.msBits.write_cnt;
	while(words_to_write-- > 0)
	{
		/* if we are done, then quit */
		if (fpga_spi->remaining_bytes <= 0)
		{
			break;
		}
		/* transfer a word (this will update remaining_bytes) */
		tx_word(fpga_spi);
	}
}

/**
 * IRQ handler called when an SPI core is asserting an interrupt
 * condition.  This method is called within the context of an ISR, and
 * must be atomic.
 *
 * \param[in] dev the device issuing the interrupt.
 * \return 0
 */
static int fpga_spi_IrqHandler(struct fpga_device* dev)
{
	struct fpga_spi *fpga_spi;
	unsigned short  *lpBaseReg;
	tuSPIIPR IPR;
	tuSPIIER IER;

	fpga_spi = spi_master_get_devdata(dev_get_drvdata(&dev->dev));
	lpBaseReg = (unsigned short*)dev->baseaddr;

	if (fpga_spi->remaining_bytes > 0)
		fill_tx_fifo(fpga_spi);

	IPR.mnWord = ioread16(&lpBaseReg[gnIPR_OFFSET]);
	IER.mnWord = ioread16(&lpBaseReg[gnIER_OFFSET]);
	rmb();
	while(IPR.msBits.miso_nempty)
	{
		rx_word(fpga_spi);
		IPR.mnWord = ioread16(&lpBaseReg[gnIPR_OFFSET]);
		rmb();
	}

	/* no more data to transmit, disable FIFO level flags */ 
	if (fpga_spi->remaining_bytes <= 0)
	{
		/* disable helf empty threshold */
		IER.msBits.mosi_hf = 0;
	}

	if (fpga_spi->rx_remaining_bytes <= fpga_spi->fifo_depth/2)
	{
		IER.msBits.miso_hf = 0;
	}

	if (fpga_spi->rx_remaining_bytes <= 0)
	{
		IER.msBits.miso_nempty = 0;
	}

	iowrite16(IER.mnWord, &lpBaseReg[gnIER_OFFSET]);

	IPR.mnWord = ioread16(&lpBaseReg[gnIPR_OFFSET]);

	/* if we are complete, then signal completion to waiting task */
	if (IPR.msBits.mosi_tc && !IPR.msBits.miso_nempty)
	{
		/* disable interrupts */
		IER.mnWord = 0;

		/* clear trasnmit completion status bit */
		iowrite16(IPR.mnWord, &lpBaseReg[gnIPR_OFFSET]);

		/* signal to waiting task that we're done */
		complete(&fpga_spi->done);
	}

	return 0;
}

/**
 * This routine is called when a device is removed from the FPGA bus.
 *
 * \param[in] dev pointer to the device being removed.
 */
static int fpga_spi_remove(struct device *dev)
{
	int rv = 0;
	struct fpga_spi *fpga_spi;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	struct spi_master* master  = dev_get_drvdata(dev);
	fpga_spi = spi_master_get_devdata(master);

	rv = spi_bitbang_stop(&fpga_spi->bitbang);

	sysfs_remove_group(&dev->kobj, &fpga_spi_attr_group);

	/* make sure IRQ is disabled */
	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
				   fpgadev->coreversion.ver0.bits.vector,
				   0);

	spi_master_put(master);

	return rv;
}

static int fpga_spi_probe(struct device *dev);

/**
 * The driver object.  The information here must be common for
 * all of the potential drivers in the system.
 */
static struct fpga_driver fpga_spi_driver = {
	.id		= CORE_ID_SPI, /** must match value in core_ids.h */
	.version	= "SPI Driver 2.0",
	.IrqHandler	= fpga_spi_IrqHandler,
	.probe		= fpga_spi_probe,
	.remove		= fpga_spi_remove,
	.driver 	= {
		.name 	= "fpga_spi",
		.owner 	= THIS_MODULE,
	},
};

static int fpga_spi_setup_transfer(struct spi_device *spi, struct spi_transfer *t)
{
	struct fpga_spi *fpga_spi;
	unsigned short* lpBaseAddr;
	int divisor;
	tuSPICSR CSR;
	tuSPIDIV DIV;
	u8 bits_per_word = 0;
	u32 hz = 0;
	int emifa_rate;
	struct clk *emif_clk;

	emif_clk = clk_get(NULL, "aemif");
	if (IS_ERR(emif_clk))
		emifa_rate = 100000000;
	else
		emifa_rate = clk_get_rate(emif_clk);

	fpga_spi = spi_master_get_devdata(spi->master);
	lpBaseAddr = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	
	if (t) {
		bits_per_word = t->bits_per_word;
		hz = t->speed_hz;		
	}

	/* if bits_per_word is not set then set it default */
	if (!bits_per_word)
		bits_per_word = spi->bits_per_word;

	CSR.mnWord = ioread16(&lpBaseAddr[gnCSR_OFFSET]);

	/* Our core only supports multiples of 4 between 4-32. */
	if(spi->bits_per_word > 32 || (spi->bits_per_word & 3) != 0)
		return -EINVAL;
	fpga_spi->bytes_per_word = (spi->bits_per_word + 7) / 8;
	CSR.msBits.dwidth = spi->bits_per_word / 4 - 1;

	if (!hz)
		hz = spi->max_speed_hz;
	DIV.mnWord = ioread16(&lpBaseAddr[gnDIV_OFFSET]);
	divisor = emifa_rate / (2 * hz);
	if (divisor == 0)
		divisor = 1;
	if ((emifa_rate / (2 * divisor)) > hz)
		divisor += 1;
	if (divisor > 0x0FFF)
		divisor = 0x0FFF;
	DIV.msBits.divisor = divisor;

	/* we do not support LSB FIRST shifting */
	if (spi->mode & SPI_LSB_FIRST)
		return -EINVAL;

	CSR.msBits.cpol = (spi->mode & SPI_CPOL) ? 1 : 0;
	CSR.msBits.cpha = (spi->mode & SPI_CPHA) ? 1 : 0;
	CSR.msBits.loopback = (spi->mode & SPI_LOOP) ? 1 : 0;

	/* There's no SPI framework mode for this -- default to SYNC-gated,
	 * because that's what most SPI devices do */
	CSR.msBits.sclk_ctrl = 1;

        /* Set rcv_en if there is a receive buffer requested */
        CSR.msBits.rcv_en = 0;

	/* chip select */
	if (!(spi->mode & SPI_NO_CS)) {
		DIV.msBits.cs_en = spi->chip_select;
	}
	
	/* loopback */
	CSR.msBits.loopback = (spi->mode & SPI_LOOP) ? 1 : 0;

	iowrite16(DIV.mnWord, &lpBaseAddr[gnDIV_OFFSET]);
	iowrite16(CSR.mnWord, &lpBaseAddr[gnCSR_OFFSET]);

	return 0;
}

/*
 * This method is called when the SPI framework is cleaning up
 */
static void fpga_spi_cleanup(struct spi_device *spi)
{
	/* struct fpga_spi *fpga_spi = spi_master_get_devdata(spi->master); */

	/* nothing to do yet... */
}

/*
 * This method is called when the SPI framework is configured for a given
 * chip device.
 */
static int fpga_spi_setup(struct spi_device *spi)
{
	struct fpga_spi *fpga_spi;
	unsigned short* lpBaseAddr;

	fpga_spi = spi_master_get_devdata(spi->master);
	lpBaseAddr = (unsigned short*)fpga_spi->fpgadev->baseaddr;

	/* if bits per word length is zero then set it default 8 */
	if (!spi->bits_per_word)
		spi->bits_per_word = 8;
		
	if (!spi->max_speed_hz)
		spi->max_speed_hz = MAX_FPGA_SPI_SPEED_HZ;

	return 0;
}

static void fpga_spi_chipselect(struct spi_device *spi , int value)
{
	/* struct fpga_spi *fpga_spi = spi_master_get_devdata(spi->master); */

	/* chip select controlled by transfers.. */
}

static int fpga_spi_txrx_bufs(struct spi_device *spi, struct spi_transfer *t)
{
	struct fpga_spi *fpga_spi = spi_master_get_devdata(spi->master);
	unsigned short* lpBaseAddr = (unsigned short*)fpga_spi->fpgadev->baseaddr;
	tuSPIIER IER;
	tuSPIIPR IPR;
	tuSPICSR CSR;
	tuSPIFRR FRR;
	uint32_t temp;

	fpga_spi = spi_master_get_devdata(spi->master);

	fpga_spi->remaining_bytes = t->len;
	fpga_spi->rx_buf = t->rx_buf;
	if (t->rx_buf)
		fpga_spi->rx_remaining_bytes = t->len;
	else
		fpga_spi->rx_remaining_bytes = 0;
		
	fpga_spi->tx_buf = t->tx_buf;

	do {
		temp = ioread32((uint32_t *) &lpBaseAddr[gnFDR_OFFSET]);
		FRR.mnWord = ioread16(&lpBaseAddr[gnFRR_OFFSET]);
		rmb();
	} while(!FRR.msBits.empty);

	/* clear any interrupt pending conditions */
	IPR.mnWord = ioread16(&lpBaseAddr[gnIPR_OFFSET]);
	IPR.msBits.mosi_tc = 1;
	iowrite16(IPR.mnWord, &lpBaseAddr[gnIPR_OFFSET]);

	IER.mnWord = 0;
	iowrite16(IER.mnWord,&lpBaseAddr[gnIER_OFFSET]);

	IPR.mnWord = ioread16(&lpBaseAddr[gnIPR_OFFSET]);
	if (IPR.msBits.mosi_tc)
		dev_err(&fpga_spi->fpgadev->dev, "%s : TC bit not cleared in IPR at start of xfer",
			__func__);

	INIT_COMPLETION(fpga_spi->done);

	/* reset fifos */
        CSR.mnWord = ioread16(&lpBaseAddr[gnCSR_OFFSET]);
	CSR.msBits.fifo_rst = 1;
        iowrite16(CSR.mnWord, &lpBaseAddr[gnCSR_OFFSET]);
	wmb();

        /* fill the output fifo */
	fill_tx_fifo(fpga_spi);

	/* start the outbound transfer by setting go bit */
        CSR.mnWord = ioread16(&lpBaseAddr[gnCSR_OFFSET]);
        CSR.msBits.go = 1;
        CSR.msBits.rcv_en = t->rx_buf ? 1 : 0;
        iowrite16(CSR.mnWord, &lpBaseAddr[gnCSR_OFFSET]);

	/* enable interrupts to catch transfer completion, or
	 * FIFO emptying */
	IER.mnWord = 0;
	IER.msBits.mosi_tc = 1;
	if (fpga_spi->remaining_bytes)
		IER.msBits.mosi_hf = 1;
	if (fpga_spi->rx_remaining_bytes >= fpga_spi->fifo_depth/2)
		IER.msBits.miso_hf = 1;
	else if (fpga_spi->rx_remaining_bytes > 0)
		IER.msBits.miso_nempty = 1;
	iowrite16(IER.mnWord,&lpBaseAddr[gnIER_OFFSET]);

	/* wait for completion */
	wait_for_completion(&fpga_spi->done);

	if (fpga_spi->rx_remaining_bytes || fpga_spi->remaining_bytes)
		dev_err(&fpga_spi->fpgadev->dev, "%s : rx_rem_bytes was %d, tx_rem_bytes %d\n",
			__func__, fpga_spi->rx_remaining_bytes, fpga_spi->remaining_bytes);

	return t->len - fpga_spi->remaining_bytes;
}

/**
 * The spi_probe routine is called after the spi driver is successfully
 * matched to an FPGA core with the same core ID.
 *
 * \param[in] dev device within an fpga_device structure.
 * return 0 on successful probe / initialization.
 */
static int fpga_spi_probe(struct device *dev)
{
	int rv = 0;
	struct spi_master *master;
	struct fpga_spi   *fpga_spi = NULL;

	unsigned short    *lpBaseReg = NULL;
	tuSPIFWR FWR;
	tuSPIDEL DEL;

	struct fpga_device* fpgadev = to_fpga_device(dev);

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	dev_info(dev, "spi_probe() entered\n");

	rv = sysfs_create_group(&dev->kobj, &fpga_spi_attr_group);
	if (rv)
	{
		dev_err(dev, "spi_probe() failed to add attributes group - %d\n", rv);
		goto probe_bail;
	}

	master = spi_alloc_master(&fpgadev->dev, sizeof(struct fpga_spi));
	if (master == NULL)
	{
		rv = -ENOMEM;
		goto probe_bail;
	}

	dev_set_drvdata(dev, master);

	fpga_spi = spi_master_get_devdata(master);
	if (fpga_spi == NULL)
	{
		rv = -ENOENT;
		goto probe_bail_free_master;
	}

	fpga_spi->fpgadev = fpgadev;

	fpga_spi->bitbang.master = spi_master_get(master);
	if (fpga_spi->bitbang.master == NULL)
	{
		rv = -ENODEV;
		goto probe_bail_free_master;
	}

	fpga_spi->bitbang.flags = SPI_CPOL | SPI_CPHA | SPI_LOOP;
	master->bus_num = bus_num; /* auto assign it... */
	master->num_chipselect = MAX_FPGA_CHIP_SELECTS;
	master->setup = fpga_spi_setup;
	master->cleanup = fpga_spi_cleanup;
	init_completion(&fpga_spi->done);

	fpga_spi->bitbang.chipselect = fpga_spi_chipselect;
	fpga_spi->bitbang.setup_transfer = fpga_spi_setup_transfer;
	fpga_spi->bitbang.txrx_bufs = fpga_spi_txrx_bufs;

	FWR.mnWord = ioread16(&lpBaseReg[gnFWR_OFFSET]);
	fpga_spi->fifo_depth = 1<<(FWR.msBits.depth+4);
	dev_info(&fpgadev->dev, "FIFO DEPTH = %d\n", fpga_spi->fifo_depth);

	DEL.mnWord = ioread16(&lpBaseReg[gnDEL_OFFSET]);
	fpga_spi->delay = DEL.msBits.delay;
	
	iowrite16(0, &lpBaseReg[gnIER_OFFSET]);

	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
			       fpgadev->coreversion.ver0.bits.vector,
			       1);

	rv = spi_bitbang_start(&fpga_spi->bitbang);
	if (rv)
		goto probe_bail_free_master;

	dev_info(&fpgadev->dev, "FPGA SPI Loaded\n");

	/* Increment bus_num for next SPI core */
	bus_num++;

	return rv;

probe_bail_free_master:
	spi_master_put(master);

probe_bail:
	return rv;
}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init fpga_spi_init(void)
{
	int ret;

	ret = register_fpga_driver(&fpga_spi_driver);
	if (ret)
	{
		printk(KERN_ALERT "Unable to register fpga_spi_driver\n");
		goto exit_bail;
	}

	return 0;

exit_bail: /* Uh-Oh */
	return -1;
}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void fpga_spi_exit(void)
{
	driver_unregister(&fpga_spi_driver.driver);
}

module_init(fpga_spi_init);
module_exit(fpga_spi_exit);
