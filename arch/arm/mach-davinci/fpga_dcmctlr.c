/*
 * fpga_dcmctlr.c
 *
 *  Created on: Nov 7, 2011
 *      Author: Mike Williamson
 */

#include "fpga.h"
#include "common/fpga/fpga_dcmctlr.h"
#include "common/fpga/core_ids.h"

#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/clk.h>
#include <linux/cpufreq.h>
#include <linux/err.h>
#include <asm/io.h>
#include <linux/device.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Williamson <michael.williamson@criticallink.com");
MODULE_DESCRIPTION("Driver for MityDSP-L138 FPGA Based DCM Controller Core");

struct fpga_dcmctlr {
	struct fpga_device*	fpgadev;
	int			phase_counts;
};

static int phase_shift(struct fpga_dcmctlr* fpga_dcmctlr, int inc)
{
	unsigned short *lpBaseReg = (unsigned short*)fpga_dcmctlr->fpgadev->baseaddr;
	tuPSC PSC;
	int   overflow;
	
	PSC.word = ioread16(lpBaseReg+gnPSC_OFFSET);
	PSC.bits.done = 1;
	PSC.bits.psincdec = inc ? 1 : 0;
	PSC.bits.go = 1;
	overflow = PSC.bits.overflow;
	iowrite16(PSC.word, lpBaseReg+gnPSC_OFFSET);
	
	do {
		PSC.word = ioread16(lpBaseReg+gnPSC_OFFSET);
	} while(!PSC.bits.done);

	// if we aren't stuck overflowing...
	if (!(overflow & PSC.bits.overflow))
	{	
		if (inc)
			fpga_dcmctlr->phase_counts++;
		else
			fpga_dcmctlr->phase_counts--;
	}
	
	return PSC.bits.overflow;
}

static ssize_t fpga_dcmctlr_ps_show(struct device *dev,
                        struct device_attribute *attr, char *buf)
{
	struct fpga_dcmctlr* fpga_dcmctlr = (struct fpga_dcmctlr*)dev_get_drvdata(dev);
	return sprintf(buf, "%d\n",fpga_dcmctlr->phase_counts);
}

static ssize_t fpga_lcdctlr_ps_set(struct device *dev, 
		struct device_attribute *attr, const char *buf, size_t count)
{
	int phase_deg, inc;
	struct fpga_dcmctlr* fpga_dcmctlr = (struct fpga_dcmctlr*)dev_get_drvdata(dev);

	sscanf(buf,"%d", &phase_deg);
	
	inc = (phase_deg < fpga_dcmctlr->phase_counts) ? 0 : 1;
	
	while (phase_deg != fpga_dcmctlr->phase_counts)
	{
		if (phase_shift(fpga_dcmctlr, inc))
			break;
	}

	return count;
}
static DEVICE_ATTR(phase, S_IRUGO | S_IWUSR, fpga_dcmctlr_ps_show, fpga_lcdctlr_ps_set);

static struct attribute *fpga_dcmctlr_attributes[] = {
		&dev_attr_phase.attr,
		NULL,
};

static struct attribute_group fpga_dcmctlr_attr_group = {
		.attrs = fpga_dcmctlr_attributes,
};

/**
 *  Does nothing yet...
 */
static int fpga_dcmctlr_IrqHandler(struct fpga_device* dev)
{
	struct fpga_lcdctlr *fpga_lcdctlr;
	unsigned short  *lpBaseReg;

	fpga_lcdctlr = dev_get_drvdata(&dev->dev);
	lpBaseReg = (unsigned short*)dev->baseaddr;

	return 0;
}

/**
 * This routine is called when a device is removed from the FPGA bus.
 *
 * \param[in] dev pointer to the device being removed.
 */
static int fpga_dcmctlr_remove(struct device *dev)
{
	int rv = 0;
	struct fpga_device* fpgadev = to_fpga_device(dev);
	struct fpga_dcmctlr *fpga_dcmctlr = dev_get_drvdata(dev);

	sysfs_remove_group(&dev->kobj, &fpga_dcmctlr_attr_group);

	/* make sure IRQ is disabled */
	enable_irq_vec(fpgadev->coreversion.ver0.bits.level,
			   fpgadev->coreversion.ver0.bits.vector,
			   0);

	kfree(fpga_dcmctlr);

	return rv;
}

static int fpga_dcmctlr_probe(struct device *dev);

/**
 * The driver object.  The information here must be common for
 * all of the potential drivers in the system.
 */
static struct fpga_driver fpga_dcmctlr_driver = {
	.id		= CORE_ID_DCMCTLR, /** must match value in core_ids.h */
	.version	= "DCM Controller Driver 1.0",
	.IrqHandler	= fpga_dcmctlr_IrqHandler,
	.probe		= fpga_dcmctlr_probe,
	.remove		= fpga_dcmctlr_remove,
	.driver 	= {
		.name 	= "fpga_dcmctlr",
		.owner 	= THIS_MODULE,
	},
};

/**
 */
static int fpga_dcmctlr_probe(struct device *dev)
{
	int rv = 0;
	struct fpga_dcmctlr   *fpga_dcmctlr = NULL;
	unsigned short    *lpBaseReg = NULL;
	struct fpga_device *fpgadev = to_fpga_device(dev);

	lpBaseReg = (unsigned short*)fpgadev->baseaddr;

	rv = sysfs_create_group(&dev->kobj, &fpga_dcmctlr_attr_group);
	if (rv)
	{
		dev_err(dev, "%s failed to add attributes group - %d\n", __func__, rv);
		goto probe_bail;
	}

	fpga_dcmctlr = (struct fpga_dcmctlr *)kmalloc(sizeof(struct fpga_dcmctlr),
							GFP_KERNEL);
	if (fpga_dcmctlr == NULL)
	{
		rv = -ENOMEM;
		goto probe_bail;
	}
	fpga_dcmctlr->fpgadev = fpgadev;
	fpga_dcmctlr->phase_counts = 0;

	dev_set_drvdata(dev, fpga_dcmctlr);

	dev_info(&fpgadev->dev, "FPGA DCMCTLR Loaded\n");

	return rv;

probe_bail:
	return rv;
}

/**
 * Called when the module containing this driver is loaded.
 */
static int __init fpga_dcmctlr_init(void)
{
	int ret;

	ret = register_fpga_driver(&fpga_dcmctlr_driver);
	if (ret)
	{
		printk(KERN_ALERT "Unable to register fpga_dcmctlr_driver\n");
		goto exit_bail;
	}

	return 0;

exit_bail: /* Uh-Oh */
	return -1;
}

/**
 * Called when the module containing this driver is unloaded from kernel.
 */
static void fpga_dcmctlr_exit(void)
{
	driver_unregister(&fpga_dcmctlr_driver.driver);
}

module_init(fpga_dcmctlr_init);
module_exit(fpga_dcmctlr_exit);
