/*
 * Critical Link MityOMAP-L138 SoM Baseboard initializtaion file
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/mtd/mtd.h>
#include <linux/usb/musb.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/setup.h>
#include <mach/mux.h>
#include <mach/da8xx.h>
#include <linux/can/platform/mcp251x.h>

#define BASEBOARD_NAME "MicroWIU"
#define MSTPRI2_LCD_MASK  0x70000000
#define MSTPRI2_LCD_SHIFT 28
#define LCD_PRIORITY	  0 /* make video output highest priority */

#define MMCSD_CD_PIN		GPIO_TO_PIN(4, 2)
#define SYSTEM_RESET		GPIO_TO_PIN(5, 14)
#define CAN_CS_N		GPIO_TO_PIN(2, 15)
#define CAN_INT_PIN		GPIO_TO_PIN(2, 12)
#define DSD1791_CS_N		GPIO_TO_PIN(0, 8)

static int baseboard_mmc_get_cd(int index)
{
	/* TJI 4/5/12 - WIU SD card is always present
	return !gpio_get_value(MMCSD_CD_PIN);
	*/
	return (0 == index);
}

static struct davinci_mmc_config da850_mmc_config = {
	.get_cd		= baseboard_mmc_get_cd,
	.wires		= 4,
	.max_freq	= 50000000,
	.caps		= MMC_CAP_MMC_HIGHSPEED | MMC_CAP_SD_HIGHSPEED,
	.version	= MMC_CTLR_VERSION_2,
};

static const short da850_mmcsd0_pins[] __initconst = {
	DA850_MMCSD0_DAT_0, DA850_MMCSD0_DAT_1, DA850_MMCSD0_DAT_2,
	DA850_MMCSD0_DAT_3, DA850_MMCSD0_CLK, DA850_MMCSD0_CMD,
	DA850_GPIO4_0, DA850_GPIO4_1,
	-1
};

/**
 * UART2 Pins. CTS ignored as it's floating on hw.
 */
static short baseboard_uart2_pins[] __initdata = {
	DA850_UART2_RXD,
	DA850_UART2_TXD,
	-1,
};

static __init void baseboard_setup_mmc(void)
{
	int ret;

	ret = davinci_cfg_reg_list(da850_mmcsd0_pins);
	if (ret)
		pr_warning("%s: mmcsd0 mux setup failed: %d\n", __func__, ret);

	ret = da8xx_register_mmcsd0(&da850_mmc_config);
	if (ret)
		pr_warning("%s: mmcsd0 registration failed: %d\n", __func__,
				ret);
}

/*
 * GPIO pins, this is an exhaustive list which may be overridden by
 * other devices
 */
static short baseboard_gpio_pins[] __initdata = {
	DA850_GPIO0_0, DA850_GPIO0_1, DA850_GPIO0_2, DA850_GPIO0_3,
	DA850_GPIO0_4, DA850_GPIO0_5, DA850_GPIO0_6, DA850_GPIO0_7,
	DA850_GPIO0_8, DA850_GPIO0_9, DA850_GPIO0_10, DA850_GPIO0_11,
	DA850_GPIO0_12, DA850_GPIO0_13, DA850_GPIO0_14, DA850_GPIO0_15,
	DA850_GPIO2_12, DA850_GPIO2_15, -1,
};

static const struct display_panel disp_panel = {
	QVGA,
	16,
	16,
	COLOR_ACTIVE,
};

static struct lcd_ctrl_config lcd_cfg = {
	&disp_panel,
	.ac_bias		= 255,
	.ac_bias_intrpt		= 0,
	.dma_burst_sz		= 16,
	.bpp			= 16,
	.fdd			= 255,
	.tft_alt_mode		= 0,
	.stn_565_mode		= 0,
	.mono_8bit_mode		= 0,
	.invert_line_clock	= 0,
	.invert_frm_clock	= 0,
	.sync_edge		= 0,
	.sync_ctrl		= 1,
	.raster_order		= 0,
};

static struct da8xx_lcdc_platform_data sharp_lq035q7dh06_pdata = {
	.manu_name		= "sharp",
	.controller_data	= &lcd_cfg,
	.type			= "Sharp_LQ035Q7DH06",
};

static struct da8xx_lcdc_platform_data chimei_p0430wqlb_pdata = {
	.manu_name		= "ChiMei",
	.controller_data	= &lcd_cfg,
	.type			= "ChiMei_P0430WQLB",
};

static struct da8xx_lcdc_platform_data vga_640x480_pdata = {
	.manu_name		= "VGA",
	.controller_data	= &lcd_cfg,
	.type			= "vga_640x480",
};

static struct da8xx_lcdc_platform_data wvga_800x480_pdata = {
	.manu_name		= "WVGA",
	.controller_data	= &lcd_cfg,
	.type			= "wvga_800x480",
};

static struct da8xx_lcdc_platform_data svga_800x600_pdata = {
	.manu_name		= "SVGA",
	.controller_data	= &lcd_cfg,
	.type			= "svga_800x600",
};

static struct da8xx_lcdc_platform_data nec_nl4827hc19_pdata = {
	.manu_name		= "NEC",
	.controller_data	= &lcd_cfg,
	.type			= "NEC_NL4827HC19-05B",
};

static __init void baseboard_setup_lcd(const char *panel)
{
	int ret;
	struct da8xx_lcdc_platform_data *pdata;

	u32 prio;

	/* set peripheral master priority up to 1 */
	prio = __raw_readl(DA8XX_SYSCFG0_VIRT(DA8XX_MSTPRI2_REG));
	prio &= ~MSTPRI2_LCD_MASK;
	prio |= LCD_PRIORITY<<MSTPRI2_LCD_SHIFT;
	__raw_writel(prio, DA8XX_SYSCFG0_VIRT(DA8XX_MSTPRI2_REG));

	if (!strcmp("Sharp_LQ035Q7DH06", panel))
		pdata = &sharp_lq035q7dh06_pdata;
	else if (!strcmp("ChiMei_P0430WQLB", panel))
		pdata = &chimei_p0430wqlb_pdata;
	else if (!strcmp("vga_640x480", panel))
		pdata = &vga_640x480_pdata;
	else if (!strcmp("wvga_800x480", panel))
		pdata = &wvga_800x480_pdata;
	else if (!strcmp("svga_800x600", panel))
		pdata = &svga_800x600_pdata;
	else if (!strcmp("NEC_NL4827HC19-05B", panel)) {
		pdata = &nec_nl4827hc19_pdata;
		lcd_cfg.invert_line_clock	= 1;
		lcd_cfg.invert_frm_clock	= 1;
	} else {
		pr_warning("%s: unknown LCD type : %s\n", __func__,
				panel);
		return;
	}

	ret = davinci_cfg_reg_list(da850_lcdcntl_pins);
	if (ret) {
		pr_warning("%s: lcd pinmux failed : %d\n", __func__,
				ret);
		return;
	}

	ret = da8xx_register_lcdc(pdata);
}

#ifdef CONFIG_MTD
#define OFFSET_LCDCONFIG 0260
#define LCDCONFIG_LEN 33
static void baseboard_mtd_notify(struct mtd_info *mtd)
{
	int retlen;
	char buf[LCDCONFIG_LEN]; /* enable, manufacturer name */
	if (!strcmp(mtd->name, "periph-config")) {
		mtd_read(mtd, OFFSET_LCDCONFIG, LCDCONFIG_LEN, &retlen,
			  buf);
		if (retlen == LCDCONFIG_LEN) {
			if (buf[0]) {
				buf[LCDCONFIG_LEN-1] = 0;
				pr_info("Using LCD panel: %s\n", &buf[1]);
				baseboard_setup_lcd(&buf[1]);
			} else
				pr_info("No LCD configured\n");
		}
	}
}

static struct mtd_notifier baseboard_spi_notifier = {
	.add	= baseboard_mtd_notify,
};

static void baseboard_mtd_notify_add(void)
{
	register_mtd_user(&baseboard_spi_notifier);
}
#else
static void baseboard_mtd_notify_add(void) { }
#endif


static struct davinci_spi_config spi_ksz8995_config = {
	.io_type	= SPI_IO_TYPE_DMA,
	.c2tdelay	= 0,
	.t2cdelay	= 0,
};

static struct davinci_spi_config spi_vital_a_config = {
	.io_type	= SPI_IO_TYPE_DMA,
	.c2tdelay	= 0,
	.t2cdelay	= 0,
};

static struct davinci_spi_config spi_vital_b_config = {
	.io_type	= SPI_IO_TYPE_DMA,
	.c2tdelay	= 0,
	.t2cdelay	= 0,
};

static struct spi_board_info baseboard_fpga_spi_info[] = {
	[0] = {
		.modalias		= "spidev",
		.controller_data	= &spi_ksz8995_config,
		.max_speed_hz		= 10000000,
		.bus_num		= 3,
		.chip_select		= 0,
	},
	[1] = {
		.modalias		= "spidev",
		.controller_data	= &spi_vital_a_config,
		.max_speed_hz		= 1000000,
		.bus_num		= 4,
		.chip_select		= 0,
	},
	[2] = {
		.modalias		= "spidev",
		.controller_data	= &spi_vital_b_config,
		.max_speed_hz		= 1000000,
		.bus_num		= 5,
		.chip_select		= 0,
	},
};

static void __init baseboard_setup_spi(void)
{
	int ret;

	ret = spi_register_board_info(baseboard_fpga_spi_info,
					ARRAY_SIZE(baseboard_fpga_spi_info));
	if (ret)
		pr_warning("%s: Unable to register SPI1 Info: %d\n", __func__,
				ret);
}

static int __init baseboard_pre_init(void)
{
	pr_info("%s: Entered\n", __func__);
	davinci_soc_info.emac_pdata->phy_id = "0:05";
	return 0;
}
postcore_initcall_sync(baseboard_pre_init);

static int __init baseboard_init(void)
{
	pr_info("%s [%s]...\n", __func__, BASEBOARD_NAME);

	davinci_cfg_reg_list(baseboard_gpio_pins);

	baseboard_setup_mmc();

	baseboard_setup_spi();

	baseboard_mtd_notify_add();

	mityomapl138_usb_init(MUSB_HOST);

	davinci_cfg_reg_list(baseboard_uart2_pins);

	return 0;
}
arch_initcall_sync(baseboard_init);

